list(APPEND _f_srcs
   ${CMAKE_CURRENT_LIST_DIR}/LSopt-input.F90
   ${CMAKE_CURRENT_LIST_DIR}/ls_opt.F90
   ${CMAKE_CURRENT_LIST_DIR}/ls_opt2.F90
   ${CMAKE_CURRENT_LIST_DIR}/ls_redint.F90
   ${CMAKE_CURRENT_LIST_DIR}/q_to_x.F90
  )

set_property(
  SOURCE
    ${_f_srcs}
  APPEND_STRING
  PROPERTY
    COMPILE_FLAGS
      "${_LSDALTON_Fortran_FLAGS} ${_LSDALTON_Fortran_FLAGS_${CMAKE_BUILD_TYPE}}"
  )

set_property(
  SOURCE
    ${CMAKE_CURRENT_LIST_DIR}/dqdx.cpp
  APPEND_STRING
  PROPERTY
    COMPILE_FLAGS
      "${_LSDALTON_CXX_FLAGS} ${_LSDALTON_CXX_FLAGS_${CMAKE_BUILD_TYPE}}"
  )

add_library(geomopt
  OBJECT
    ${CMAKE_CURRENT_LIST_DIR}/dqdx.cpp
    "${_f_srcs}"
  )

target_link_libraries(geomopt
  PUBLIC
    lsutil
    LSint
  )

target_link_libraries(lsdalton
  PUBLIC
    geomopt
  )
