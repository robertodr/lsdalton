#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > dmabn_pariK.info <<'%EOF%'
   dmabn_pariK
   -------------
   Molecule:         C60/6-31G
   Wave Function:    HF
   Profile:          Exchange Matrix
   CPU Time:         9 min 30 seconds
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > dmabn_pariK.mol <<'%EOF%'
BASIS
cc-pVTZ Aux=cc-pVTZdenfit
DMABN
Excitation Calculation
Atomtypes=3 Angstrom Nosymmetry
Charge=6.0 Atoms=9
C    -0.000189    0.141320    0.035917
C     1.207912    0.106068   -0.696336
C    -1.207497    0.100843   -0.697398
C    -1.206745    0.030840   -2.084823
C     1.208703    0.036083   -2.083770
C     0.001344    0.002555   -2.794796
C     0.002264   -0.074879   -4.225295
C     1.232813   -0.051811    2.129284
C    -1.234031   -0.057318    2.128285
Charge=7.0 Atoms=2
N    -0.001028    0.259210    1.421598
N     0.003679   -0.138284   -5.407584           
Charge=1.0 Atoms=10
H     2.152877    0.006978   -2.622283
H     2.164061    0.121371   -0.186492
H    -2.164167    0.111962   -0.188388
H    -2.150327   -0.002318   -2.624141
H     2.025241    0.641152    1.837018
H     1.580627   -1.080443    1.952237
H     1.061212    0.080917    3.198204
H    -2.029085    0.632690    1.836141
H    -1.577613   -1.087195    1.950196
H    -1.063598    0.075177    3.197423
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > dmabn_pariK.dal <<'%EOF%'
**PROFILE
.EXCHANGE
**INTEGRALS
.PARI-K
**WAVE FUNCTIONS
.HF
*DENSOPT
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >dmabn_pariK.check
cat >> dmabn_pariK.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Exchange energy, mat_dotproduct\(D,K\)\= * \-XXX\.XXXXXX" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
