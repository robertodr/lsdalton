#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > beta-dipeptide_pariK.info <<'%EOF%'
   beta-dipeptide_pariK
   -------------
   Molecule:         C60/6-31G
   Wave Function:    HF
   Profile:          Exchange Matrix
   CPU Time:         9 min 30 seconds
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > beta-dipeptide_pariK.mol <<'%EOF%'
BASIS
cc-pVTZ Aux=cc-pVTZdenfit
Betapeptide
EXCITATION DIAGNOSTIC
Atomtypes=4 Nosymmetry
Charge=6.0 Atoms=6
C       6.5891803751           -4.5682626310            0.0000000000
C       5.1736235994           -2.0794842991            0.0000000000
C       1.0470572733            0.0194232569            0.0000000000
C      -1.7281903783           -0.7177645998            0.0000000000
C      -3.4304130946            1.6013583816            0.0000000000
C      -7.8658037626            3.0333602581            0.0000000000
Charge=8.0 Atoms=2
O       6.2732437020           -0.0249589732            0.0000000000
O      -2.5794235527            3.7704168942            0.0000000000
Charge=7.0 Atoms=2
N      -5.9555494906            1.0684664638            0.0000000000
N       2.6015824040           -2.2478703142            0.0000000000
Charge=1.0 Atoms=12
H       5.3535976550           -6.2232010839            0.0000000000
H       7.8065723374           -4.6350069919            1.6651893485
H       7.8065723374           -4.6350069919           -1.6651893485
H       1.7636947207           -3.9631671423            0.0000000000
H       1.4869313246            1.1765924867            1.6541037149
H       1.4869313246            1.1765924867           -1.6541037149
H      -2.1635815150           -1.8741450595            1.6671609704
H      -2.1635815150           -1.8741450595           -1.6671609704
H      -6.5084500049           -0.7602197931            0.0000000000
H      -6.8736529748            4.8368583642            0.0000000000
H      -9.0573706472            2.9127907958            1.6842359338
H      -9.0573706472            2.9127907958           -1.6842359338
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > beta-dipeptide_pariK.dal <<'%EOF%'
**PROFILE
.EXCHANGE
**INTEGRALS
.PARI-K
**WAVE FUNCTIONS
.HF
*DENSOPT
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >beta-dipeptide_pariK.check
cat >> beta-dipeptide_pariK.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Exchange energy, mat_dotproduct\(D,K\)\= * \-XXX\.XXXXXX" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
