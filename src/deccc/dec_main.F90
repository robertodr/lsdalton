!> @file
!> Main driver for DEC program. 

!> \author Marcin Ziolkowski (modified for Dalton by Kasper Kristensen)
!> \date 2010-09

module dec_main_mod

  use precision
  use lstiming!,only: lstimer
  use lsparameters
  use typedeftype!,only: lsitem
  use matrix_module!,only:matrix
  use matrix_operations !,only: mat_read_from_disk,mat_set_from_full
  use memory_handling!,only: mem_alloc, mem_dealloc
  use dec_typedef_module
  use files !,only:lsopen,lsclose
  !use reorder_frontend_module 
  !use tensor_tester_module
  use Matrix_util!, only: get_AO_gradient
  use configurationType
  use background_buffer_module
  use tensor_interface_module
#ifdef VAR_MPI
  use lsmpi_param,only: MPI_COMM_LSDALTON
  use lsmpi_op
#endif
  use IntegralInterfaceMOD

  !> F12routines
  !use f12_routines_module
  
  ! DEC DEPENDENCIES (within deccc directory) 
  ! *****************************************
  use snoop_main_module
  use dec_fragment_utils
  use array4_memory_manager!,only: print_memory_currents4
  use full_molecule!,only: molecule_init, molecule_finalize,molecule_init_from_inputs
  use orbital_operations!,only: check_lcm_against_canonical
  use full_molecule
  use mp2_gradient_module
  use f12_routines_module
  use dec_driver_module,only: dec_wrapper
  use full,only: full_driver
  use fullrimp2f12,only: NaturalLinearScalingF12Terms
  use lofex_tools
  use ricc2_module

  public :: dec_main_prog_input, dec_main_prog_file, &
       & get_mp2gradient_and_energy_from_inputs, get_total_CCenergy_from_inputs, &
       & lofex_driver
private

contains

  !> Wrapper for main DEC program to use when Fock,density, and MO coefficient
  !> matrices are available from HF calculation.
  !> \author Kasper Kristensen
  !> \date April 2013
  subroutine dec_main_prog_input(mylsitem,config,F,D,C,E)
    implicit none

    !> Integral info
    type(lsitem), intent(inout) :: mylsitem
    !> Config info
    type(configItem),intent(inout)  :: config
    !> Fock matrix 
    type(matrix),intent(inout) :: F
    !> HF density matrix 
    type(matrix),intent(in) :: D
    !> MO coefficients 
    type(matrix),intent(inout) :: C
    !> CC Energy (intent out) 
    real(realk),intent(inout) :: E
    !local variables
    type(fullmolecule) :: Molecule
    !SCF gradient norm
    real(realk) :: gradnorm
    integer :: i,j,k

    print *, 'Hartree-Fock info comes directly from HF calculation...'

    ! Get informations about full molecule
    ! ************************************
    call molecule_init_from_inputs(Molecule,mylsitem,config,F,C,D)

    ! Fock, overlap, and MO coefficient matrices are now stored
    ! in Molecule, and there is no reason to store them twice.
    ! So we delete them now and reset them at the end.
    call mat_free(F)
    call mat_free(C)

    call dec_main_prog(MyLsitem,config,molecule,D,E)

    ! Restore input matrices
    call molecule_copyback_FC_matrices(mylsitem,Molecule,F,C)
    
    ! Delete molecule structure
    call molecule_finalize(molecule,.true.)

  end subroutine dec_main_prog_input

  subroutine SCFgradError(gradnorm)
    implicit none
    real(realk) :: gradnorm
    WRITE(DECinfo%output,'(4X,A,ES16.8)')'WARNING: The SCF gradient norm = ',gradnorm
    WRITE(DECinfo%output,'(4X,A,ES16.8)')'WARNING: Is greater then the FOT=',DECinfo%FOT
    WRITE(DECinfo%output,'(4X,A,ES16.8)')'WARNING: The Error of the DEC calculation would be determined by the SCF error.'
    WRITE(DECinfo%output,'(4X,A,ES16.8)')'WARNING: Tighten the SCF threshold!'
!    call lsquit('DEC ERROR: SCF gradient too large',-1)
  end subroutine SCFgradError

  !> Wrapper for main DEC program to use when Fock,density,overlap, and MO coefficient
  !> matrices are not directly available from current HF calculation, but rather needs to
  !> be read in from previous HF calculation.
  !> \author Kasper Kristensen
  !> \date April 2013
  subroutine dec_main_prog_file(mylsitem,config)

    implicit none
    !> Integral info
    type(lsitem), intent(inout) :: mylsitem
    !> Config info
    type(configItem),intent(inout)  :: config
    type(matrix) :: D
    type(fullmolecule) :: Molecule
    integer :: nbasis
    real(realk) :: E
    E = 0.0E0_realk
    
    print *, 'Hartree-Fock info is read from file...'

    ! Get density matrix
    Molecule%nbasis = get_num_basis_functions(mylsitem)
    call dec_get_density_matrix_from_file(Molecule%nbasis,D)
    
    ! Get informations about full molecule by reading from file
    call molecule_init_from_files(molecule,mylsitem,config,D)
 
    ! Main DEC program
    call dec_main_prog(MyLsitem,config,molecule,D,E)
     
    ! Delete molecule structure and density
    call molecule_finalize(molecule,.true.)
    IF(.NOT.DECinfo%NoDensity)THEN
       call mat_free(D)
    ENDIF

  end subroutine dec_main_prog_file



  !> \brief Main DEC program.
  !> \author Marcin Ziolkowski (modified for Dalton by Kasper Kristensen)
  !> \date September 2010
  subroutine dec_main_prog(MyLsitem,config,molecule,D,E,lofex_data)

    implicit none
    !> Integral info
    type(lsitem), intent(inout) :: mylsitem
    !> Config info
    type(configItem),intent(inout)  :: config
    !> Molecule info
    type(fullmolecule),intent(inout) :: molecule
    !> HF density matrix
    type(matrix),intent(in) :: D
    !> Energy (maybe HF energy as input, CC energy as output) 
    real(realk),intent(inout) :: E
    !> lofex information
    type(lofex_item), intent(inout), optional :: lofex_data

    character(len=50) :: MyHostname
    integer, dimension(8) :: values
    real(realk) :: tcpu1, twall1, tcpu2, twall2, EHF,Ecorr,Eerr,ES2
    real(realk) :: molgrad(3,Molecule%natoms)  
    integer :: funit 
    integer(kind=8) :: bytes_to_alloc
    logical :: save_densfit

    EHF = 0.0E0_realk 
    Ecorr = 0.0E0_realk 

    call LSTIMER('START',tcpu1,twall1,DECinfo%output)    

    ! Set DEC memory
    call get_memory_for_dec_calculation()

    ! set Density fitting information for correlated part
    save_densfit = mylsitem%setting%scheme%densfit
    mylsitem%setting%scheme%densfit = DECinfo%DoRI

#ifdef VAR_MPI
    IF(DECinfo%use_bg_buffer)THEN
       IF(.NOT.mem_is_background_buf_init())THEN
          !Allocate the Background buffer. We allocate one huge chunk of memory
          !that we use in a standard fortran77 workarrat fashion to avoid 
          !defragmentation
          bytes_to_alloc = int(DECinfo%bg_memory*(1024.0**3), kind=8)
          call mem_init_background_alloc_all_nodes(MPI_COMM_LSDALTON,bytes_to_alloc)
       ENDIF
    ENDIF
#endif

    ! Perform SNOOP calculation and skip DEC calculation
    ! (at some point SNOOP and DEC might be merged)
    if(DECinfo%SNOOP) then
       write(DECinfo%output,'(4X,A)') '***********************************************************'
       write(DECinfo%output,'(4X,A)') '      Performing SNOOP interaction energy calculation...'
       write(DECinfo%output,'(4X,A)') '***********************************************************'
       call snoop_driver(mylsitem,config,Molecule,D)
       return
    end if

       ! =============================================================
       ! Main program 
       ! =============================================================
       write(DECinfo%output,*) 
       write(DECinfo%output,*)
       write(DECinfo%output,'(4X,a)') '============================================================================='
       if(DECinfo%full_molecular_cc) then
          write(DECinfo%output,'(4X,a)') '     -- Full molecular Coupled-Cluster calculation -- '
       else
          write(DECinfo%output,'(4X,a)') '     -- Divide, Expand & Consolidate Coupled-Cluster calculation -- '
       end if
       write(DECinfo%output,'(4X,a)') '============================================================================='
       write(DECinfo%output,*)

    if(DECinfo%use_canonical) then
       write(DECinfo%output,'(4X,A)') 'Using canonical orbitals as requested in input!'
       if(.not. DECinfo%full_molecular_cc) then
          write(DECinfo%output,'(4X,A)') 'Warning: This may cause meaningless results for the DEC scheme'
       end if
       write(DECinfo%output,*)
       write(DECinfo%output,*)
    end if
    
    
    if(DECinfo%F12 .and. DECinfo%F12singles ) then
       call F12singles_driver(Molecule,MyLsitem,D)
    endif

    if(DECinfo%F12 .and. DECinfo%NaturalLinearScalingF12Terms ) then
       call NaturalLinearScalingF12Terms(Molecule,MyLsitem,D)
    endif

   ! Sanity check: LCM orbitals span the same space as canonical orbitals 
    if(DECinfo%check_lcm_orbitals) then
       call check_lcm_against_canonical(molecule,MyLsitem)
       return
    end if

    if(DECinfo%force_Occ_SubSystemLocality)then
       call force_Occupied_SubSystemLocality(molecule,MyLsitem)
    endif

    if(DECinfo%check_Occ_SubSystemLocality)then
       call check_Occupied_SubSystemLocality(molecule,MyLsitem)
    endif

    if(DECinfo%full_molecular_cc) then
       ! -- Call full molecular CC
       call full_driver(molecule,mylsitem,config,D,EHF,Ecorr)
       ! --
    else
       ! -- Initialize DEC driver for energy calculation
       call DEC_wrapper(molecule,mylsitem,D,EHF,Ecorr,molgrad,Eerr,lofex_data)
       ! --
    end if
    E = EHF + Ecorr 

    ! Update number of DEC calculations for given FOT level
    DECinfo%ncalc(DECinfo%FOTlevel) = DECinfo%ncalc(DECinfo%FOTlevel) +1
    call print_calculation_bookkeeping()

    call LSTIMER('START',tcpu2,twall2,DECinfo%output)

    ! Print memory summary
    write(DECinfo%output,*)
    write(DECinfo%output,'(4X,A)') 'CC Memory summary'
    write(DECinfo%output,'(4X,A)') '-----------------'
    call print_memory_currents4(DECinfo%output)
    write(DECinfo%output,'(4X,A)') '------------------'
    write(DECinfo%output,*)
    write(DECinfo%output,'(/,4X,a)') '------------------------------------------------------'
    if(DECinfo%full_molecular_cc) then
       write(DECinfo%output,'(4X,a,g20.6,a)') 'Total CPU  time used in CC           :',tcpu2-tcpu1,' s'
       write(DECinfo%output,'(4X,a,g20.6,a)') 'Total Wall time used in CC           :',twall2-twall1,' s'
    else
       write(DECinfo%output,'(4X,a,g20.6,a)') 'Total CPU  time used in DEC          :',tcpu2-tcpu1,' s'
       write(DECinfo%output,'(4X,a,g20.6,a)') 'Total Wall time used in DEC          :',twall2-twall1,' s'
    end if
    write(DECinfo%output,'(4X,a,/)') '------------------------------------------------------'
    write(DECinfo%output,*)

#ifdef __GNUC__  
    call hostnm(MyHostname)
    write(DECinfo%output,'(4X,a,a)')   'Hostname       : ',MyHostname
#endif
    call date_and_time(VALUES=values)
    write(DECinfo%output,'(4X,2(a,i2.2),a,i4.4,3(a,i2.2))') &
         'Job finished   : Date: ',values(3),'/',values(2),'/',values(1), &
         '   Time: ',values(5),':',values(6),':',values(7)

#ifdef VAR_MPI
    IF(DECinfo%use_bg_buffer)THEN
       !Deallocate the Background buffer
       call mem_free_background_alloc_all_nodes(MPI_COMM_LSDALTON)
    ENDIF
#endif

    write(DECinfo%output,*)
    write(DECinfo%output,*)
    write(DECinfo%output,'(/,4X,a)') '============================================================================='
    if(DECinfo%full_molecular_cc) then
       write(DECinfo%output,'(4X,a)')   '                          -- end of CC program --'
    else
       write(DECinfo%output,'(4X,a)')   '                          -- end of DEC program --'
    end if
    write(DECinfo%output,'(4X,a,/)') '============================================================================='
    write(DECinfo%output,*)
    write(DECinfo%output,*)    

    ! set Density fitting information back
    mylsitem%setting%scheme%densfit = save_densfit

  end subroutine dec_main_prog


  !> \brief Calculate MP2 energy and molecular gradient using DEC scheme
  !> using the Fock, density, and MO matrices and input - rather than reading them from file
  !> as is done in a "conventional" DEC calculation which uses the dec_main_prog subroutine.
  !> Intended to be used for MP2 geometry optimizations.
  !> \author Kasper Kristensen
  !> \date November 2011
  subroutine get_mp2gradient_and_energy_from_inputs(MyLsitem,config,F,D,C,natoms,MP2gradient,EMP2,Eerr)

    implicit none
    !> LSitem structure
    type(lsitem), intent(inout) :: mylsitem
    !> Config info
    type(configItem),intent(inout)  :: config
    !> Fock matrix 
    type(matrix),intent(inout) :: F
    !> HF density matrix 
    type(matrix),intent(in) :: D
    !> MO coefficients 
    type(matrix),intent(inout) :: C
    !> Number of atoms in molecule
    integer,intent(in) :: natoms
    !> MP2 molecular gradient
    real(realk), intent(inout) :: mp2gradient(3,natoms)
    !> Total MP2 energy (Hartree-Fock + correlation contribution)
    real(realk),intent(inout) :: EMP2
    !> Difference between intrinsic energy error at this and the previous geometry
    !> (zero for single point calculation or first geometry step)
    real(realk),intent(inout) :: Eerr
    real(realk) :: Ecorr,EHF
    type(fullmolecule) :: Molecule

    write(DECinfo%output,'(4X,A,ES16.8)') 'Calculating DEC-MP2 gradient, FOT = ', DECinfo%FOT

    ! Sanity check
    ! ************
    ! account for multilayer DEC scheme
    if(.not. DECinfo%MultiLayer) then
      if( (.not. DECinfo%gradient) .or. (.not. DECinfo%first_order) &
         &  .or. (DECinfo%ccmodel/=MODEL_MP2 .and. DECinfo%ccmodel/=MODEL_RIMP2 ) ) then
         call lsquit("ERROR(get_mp2gradient_and_energy_from_inputs): Inconsitent input!!",DECinfo%output)
      end if
    else
      if( (.not. DECinfo%gradient) .or. (.not. DECinfo%first_order) &
         &  .or. ((DECinfo%ccmodel_low/=MODEL_MP2 .and. DECinfo%ccmodel_low/=MODEL_RIMP2) .or. &
         &(DECinfo%ccmodel_high/=MODEL_MP2 .and. DECinfo%ccmodel_high/=MODEL_RIMP2) )) then
         call lsquit("ERROR(get_mp2gradient_and_energy_from_inputs): Inconsitent input!!",DECinfo%output)
      end if
    end if
    write(DECinfo%output,*) 'Calculating MP2 energy and gradient from Fock, density, overlap, and MO inputs...'

    ! Set DEC memory
    call get_memory_for_dec_calculation()

    ! Get informations about full molecule
    ! ************************************
    call molecule_init_from_inputs(Molecule,mylsitem,config,F,C,D)

    ! No reason to save F and C twice. Delete the ones in matrix format and reset at the end
    call mat_free(F)
    call mat_free(C)

    ! -- Calculate molecular MP2 gradient and correlation energy
    call DEC_wrapper(Molecule,mylsitem,D,EHF,Ecorr,mp2gradient,Eerr)

    ! Total MP2 energy: EHF + Ecorr
    EMP2 = EHF + Ecorr

    ! Restore input matrices
    call molecule_copyback_FC_matrices(mylsitem,Molecule,F,C)

    ! Free molecule structure and other stuff
    call molecule_finalize(Molecule,.true.)
    

    ! Set Eerr equal to the difference between the intrinsic error at this geometry
    ! (the current value of Eerr) and the intrinsic error at the previous geometry.
    call dec_get_error_for_geoopt(Eerr)

    ! Update number of DEC calculations for given FOT level
    DECinfo%ncalc(DECinfo%FOTlevel) = DECinfo%ncalc(DECinfo%FOTlevel) +1
    call print_calculation_bookkeeping()

  end subroutine get_mp2gradient_and_energy_from_inputs




  !> \brief Calculate MP2 energy using DEC scheme
  !> using the Fock, density, and MO matrices and input - rather than reading them from file
  !> as is done in a "conventional" DEC calculation which uses the dec_main_prog subroutine.
  !> \author Kasper Kristensen
  !> \date November 2011
  subroutine get_total_CCenergy_from_inputs(MyLsitem,config,F,D,C,ECC,Eerr)

    implicit none
    !> LSitem structure
    type(lsitem), intent(inout) :: mylsitem
    !> Config info
    type(configItem),intent(inout)  :: config
    !> Fock matrix 
    type(matrix),intent(inout) :: F
    !> HF density matrix 
    type(matrix),intent(in) :: D
    !> MO coefficients 
    type(matrix),intent(inout) :: C
    !> Total CC energy (Hartree-Fock + correlation contribution)
    real(realk),intent(inout) :: ECC
    !> Difference between intrinsic energy error at this and the previous geometry
    !> (zero for single point calculation or first geometry step)
    real(realk),intent(inout) :: Eerr
    real(realk) :: Ecorr,EHF
    type(fullmolecule) :: Molecule
    real(realk), pointer :: dummy(:,:)
    integer :: i
    logical :: save_first_order, save_grad, save_dens
    ! Quick solution to ensure that the MP2 gradient contributions are not set
    save_first_order = DECinfo%first_order
    save_grad = DECinfo%gradient
    save_dens = DECinfo%density
    DECinfo%first_order = .false.
    DECinfo%gradient = .false.
    DECinfo%density=.false.
    
    write(DECinfo%output,'(4X,A,ES16.8)') 'Calculating DEC correlation energy, FOT = ', DECinfo%FOT

    ! Set DEC memory
    call get_memory_for_dec_calculation()

    ! Get informations about full molecule
    ! ************************************
    call molecule_init_from_inputs(Molecule,mylsitem,config,F,C,D)

    ! No reason to save F and C twice. Delete the ones in matrix format and reset at the end
    call mat_free(F)
    call mat_free(C)

    ! -- Calculate correlation energy
    if(DECinfo%full_molecular_cc) then
       ! -- Call full molecular CC
       write(DECinfo%output,'(/,4X,a,/)') 'Full molecular calculation is carried out ...'
       call full_driver(molecule,mylsitem,config,D,EHF,Ecorr)
       ! --
    else
       ! -- Initialize DEC driver for energy calculation
       write(DECinfo%output,'(/,4X,a,/)') 'DEC calculation is carried out...'
       call mem_alloc(dummy,3,Molecule%natoms)
       call DEC_wrapper(Molecule,mylsitem,D,EHF,Ecorr,dummy,Eerr)
       call mem_dealloc(dummy)
       ! --
    end if

    ! Total CC energy: EHF + Ecorr
    ECC = EHF + Ecorr
    ! Restore input matrices
    call molecule_copyback_FC_matrices(mylsitem,Molecule,F,C)

    ! Free molecule structure and other stuff
    call molecule_finalize(Molecule,.true.)

    ! Reset DEC parameters to the same as they were at input
    DECinfo%first_order = save_first_order
    DECinfo%gradient = save_grad
    DECinfo%density = save_dens

    ! Set Eerr equal to the difference between the intrinsic error at this geometry
    ! (the current value of Eerr) and the intrinsic error at the previous geometry.
    call dec_get_error_for_geoopt(Eerr)

    ! Update number of DEC calculations for given FOT level
    DECinfo%ncalc(DECinfo%FOTlevel) = DECinfo%ncalc(DECinfo%FOTlevel) +1
    call print_calculation_bookkeeping()

  end subroutine get_total_CCenergy_from_inputs

  !> \brief Print number of DEC calculations for each FOT level (only interesting for geometry opt)
  !> \author Kasper Kristensen
  !> \date December 2012
  subroutine print_calculation_bookkeeping()
    implicit none
    integer :: i

    ! This only be done for geometry optimization and DEC scheme
    if( (.not. DECinfo%full_molecular_cc) .and. DECinfo%gradient) then
       write(DECinfo%output,*) 
       write(DECinfo%output,'(4X,A)') 'DEC CALCULATION BOOK KEEPING'
       write(DECinfo%output,'(4X,A)') '============================'
       write(DECinfo%output,*) 
       do i=1,nFOTs
          if(DECinfo%ncalc(i)/=0) then
             write(DECinfo%output,'(5X,a,i6,a,i6)') '# calculations done for FOTlevel ',i, ' is ', DECinfo%ncalc(i)
          end if
       end do
       write(DECinfo%output,*) 
    end if

  end subroutine print_calculation_bookkeeping


  !> \brief Driver for the calculation of cc excitation energies using LoFEx
  !> \author Pablo Baudin
  !> \date Oct. 2015
  !
  ! The driver uses NTOs from CCS or TDHF to define excitation orbital
  ! spaces required to describe a given excitation energy.
  ! We then solve the amplitude and eigenvalue problem in the reduced
  ! space and repeat with increased orbital spaces until convergence.
  !
  ! .HFRESTART option doesn't really work with LoFEx it can be used
  ! but it will try to read other files as well. Also if it is used
  ! without .NTO_CCS then it will assume that the both response vectors
  ! and NTOs are present on disk.
  subroutine lofex_driver(lsit,config,F,D)

     implicit none

     !> Integral settings
     type(lsitem), intent(inout) :: lsit
     !> General configuration settings
     type(configItem), intent(inout) :: config
     !> density and AO-fock matrix
     type(Matrix), intent(inout) :: D, F

     ! Local variables
     type(fullmolecule) :: molecule
     real(realk), pointer :: NTLMO(:,:), S(:,:)
     real(realk) :: dummy_ener, global_thres, ttot
     integer :: iexc, ista, nc, no, nv, nm, nb, funit, imacro
     logical :: nto_ccs, save_info, read_hf_files, restart, exists
     character(30) :: filename
     type(lofex_item) :: lofex_data
     logical,pointer :: which_exci(:)

     ! Send relevant information to slaves for MPI
#ifdef VAR_MPI
     call lofex_driver_mpi()
#endif 

     nb = get_num_basis_functions(lsit)
     nto_ccs = DECinfo%lofex%nto_ccs

     ! Should we read HF (ground states) files?
     read_hf_files = DECinfo%HFrestart .or. DECinfo%DECrestart
     ! Should we try to read everything?
     restart = DECinfo%DECrestart

     ! Which excitation energies to calculate (default all)
     ! ----------------------------------------------------
     call mem_alloc(which_exci,DECinfo%ccresp%nexcitCCS)
     call get_exci_list(which_exci)

     ! read or set default values to main lofex information
     call setup_lofex_data(lofex_data)
     if (restart) then
        call read_lofex_data(lofex_data)
     end if

     ! Get CCS excitation energies and vectors (put on disk)
     ! -----------------------------------------------------
     if (nto_ccs) then
        call lofex_ccs_starting_guess(nb,restart,read_hf_files,F,D,lsit,config)

        if (DECinfo%ccresp%spectrum.and.DECinfo%lofex%add_ccs_contrib) then
           ! setup oscillator strength from full CCS in lofex_data from file
           call lofex_setup_ccs_full_strength(lofex_data)
        end if
     end if

     ! Initialization
     ! --------------
     call lofex_init_molecule(molecule,config,lsit,read_hf_files,D,F)

     ! make sure the correct overlap matrix is on file
     ! -----------------------------------------------
     call mem_alloc(S,nb,nb)
     call II_get_mixed_overlap_full(DECinfo%output,DECinfo%output,lsit%setting,&
        & S,nb,nb,AORdefault,AORdefault)
     funit=-1
     ! Write overlap matrix elements to file 
     call lsopen(funit,'overlap.restart','replace','unformatted')
     write(funit) nb, nb
     write(funit) S
     call lsclose(funit,'KEEP')
     call mem_dealloc(S)


     nc = molecule%ncore
     no = molecule%nocc
     nv = molecule%nvirt
     nm = molecule%nMO
     call mem_alloc(NTLMO,nb,nm)


     ! Loop over required excitation energies
     ! --------------------------------------
     ista = lofex_data%iexc
     exci: do iexc=ista,lofex_data%nexc

        ! Skip this excitation?
        if(.not. which_exci(iexc)) then
           write(DECinfo%output,'(1X,a,i7,a)') 'LOFEX: Skipping excitation ', iexc, &
                & ' as requested in input'
           cycle exci
        end if

        ! Loop over lofex macro iterations
        ! --------------------------------
        !
        ! In each iterations NTOs are calculated from the last
        ! converged singles excitation vector. In the first 
        ! iteration, that corresponds to the CCS/TDHF vector.
        !
        lofex_data%iexc = iexc
        lofex_macro: do imacro=1,DECinfo%lofex%nmacroit

           write(DECinfo%output,*) "=================================================================="
           write(DECinfo%output,'(a,i3,a,i3)') "   LOFEX: Starting MACRO-ITERATION ",imacro," for state ", iexc
           write(DECinfo%output,*) "=================================================================="
           lofex_data%imacro = imacro

           if (imacro==1 .and. DECinfo%lofex%nto_cisdp) then
              call riCISDp_nto_from_file(lsit,iexc,config)
           end if

           ! Generate mixed orbital space from NTOs and LMOs
           ! -----------------------------------------------
           call lofex_generate_mixed_orbital_space(iexc,nc,no,nv,nm,nb,NTLMO, &
              & restart,nto_ccs,lsit,config,imacro,lofex_data)

           ! MO Fock matrix and MO coef in molecule structure
           call molecule_generate_basis(molecule,config,NTLMO)
           call molecule_mo_fock(molecule,config)

           ! Get absolute overlap matrix for space selection
           call molecule_init_abs_overlap(molecule,lsit)

           ! Get carthesian moments
           call molecule_get_carmom(molecule,lsit)

           ! Interatomic distances in atomic units
           call mem_alloc(molecule%DistanceTable,molecule%nfrags,molecule%nfrags)
           call GetDistances(molecule,lsit,DECinfo%output) 

           !==============================================================================!
           !                         MAIN LOFEX CALL: XOS OPTIMIZATION                    !
           !==============================================================================!
           !
           !> Select excitation orbital space (XOS)
           !> Calculate CC amplitudes 
           !> Diagonalize Jacobian => get excitation energy
           !> Increase size of XOS
           !> Repeat until self-consistent excitation energy
           call dec_main_prog(lsit,config,molecule,D,dummy_ener,lofex_data)

           ! Deallocate state specific information
           call molecule_finalize(molecule,.true.,free_lofex=.false.)

           ! if state converged then quit macro iterations
           if (lofex_data%conv(iexc)) exit lofex_macro

        end do lofex_macro

        if (.not. lofex_data%conv(iexc)) then
           write(DECinfo%output,'(x,a)') "LOFEX: Max number of Macro-iteration reached."
           write(DECinfo%output,'(x,a)') "LOFEX: WARNING!! current state did NOT converge!!"
           call lsquit("LOFEX: current state did not converge!",DECInfo%output)
        end if

     end do exci


     ! print final lofex information
     ! -----------------------------
     call lofex_final_print(lofex_data,nto_ccs,DECinfo%output)
     call lofex_final_print(lofex_data,nto_ccs,6)
     call write_lofex_data(lofex_data)
     ttot = lofex_data%tot_time/60.0_realk
     write(DECinfo%output,'(4x,a,f10.2,a)') "Total wall-time in LoFEx (includes restarts): ",ttot," min."
     write(DECinfo%output,*) 
     write(DECinfo%output,*) 
     call free_lofex_data(lofex_data)

     ! Restore input matrices
     if(read_hf_files) then
        call mat_free(D)
     else
        if(DECinfo%noaofock) then
           ! We need to read Fock file first because it 
           ! is not stored in full molecule structure.
           call molecule_get_fock(molecule,lsit)
        end if

        call mat_init(F,nb,nb)
        call mat_set_from_full(Molecule%fock%p%p,1.0_realk,F)
     end if

     ! Delete molecule structure
     call molecule_finalize(molecule,.true.,free_lofex=.true.)
     call mem_dealloc(NTLMO)
     call mem_dealloc(which_exci)

  end subroutine lofex_driver


  !> \brief:  Perform CCS calculation if needed by lofex
  !           This routine cannot be moved to lofex_tools.F90
  !           for dependency reasons (it calls dec_main_prog_xxxx)
  !
  !> \author: Pablo Baudin
  !> \date:   Mar. 2016
  subroutine lofex_ccs_starting_guess(nb,restart,read_hf_files,F,D,lsit,config)

     implicit none

     !> number of basis functions
     integer, intent(in) :: nb
     !> try to read files for restart
     logical, intent(in) :: restart, read_hf_files
     !> Fock and density matrices
     type(matrix), intent(inout) :: F,D
     !> Integral settings
     type(lsitem), intent(inout) :: lsit
     !> General configuration settings
     type(configItem), intent(inout) :: config

     integer :: funit, iexc
     character(40) :: filename
     real(realk) :: dummy
     logical :: do_ccs, exists
     type(DECsettings) :: DECinfo_copy
     type(matrix) :: CMO

     do_ccs = .true.

     ! Check wether a CCS calculation is required
     ! ------------------------------------------
     if (restart) then
        do_ccs = .false.
        ! CCS vectors need to be on disk
        do iexc=1,DECinfo%ccresp%nexcitCCS
           if (DECinfo%lofex%nto_cc2) then
              filename = get_cc_excit_vector_filename(iexc,MODEL_RICC2)
           else
              filename = get_cc_excit_vector_filename(iexc,MODEL_CCS)
           end if
           inquire(file=filename,exist=exists)
           if (.not.exists) do_ccs = .true.
           exit
        end do
     end if

     if (do_ccs) then
        ! save DECinfo
        DECinfo_copy = DECinfo

        DECinfo%full_molecular_cc = .true.
        DECinfo%do_lofex = .false.
        DECinfo%use_singles=.true. 
        if (DECinfo%lofex%nto_cc2) then
           ! get CC2 NTOs instead of CCS
           DECinfo%ccmodel = MODEL_RICC2
        else
           DECinfo%ccmodel = MODEL_CCS
        end if
        DECinfo%ccmultipliers = .false.
        if (.not. DECinfo%lofex%add_ccs_contrib) DECinfo%ccresp%spectrum = .false.

        ! by setting use canonical we enforce the use of canonical orbitals
        ! which should ALWAYS corresponds to the one on file (cmo_orbitals.u)
        DECinfo%use_canonical = .true.
        DECinfo%ccresp%CCS_start_guess = .false.

        ! perform CCS calculation
        if (read_hf_files) then
           call dec_main_prog_file(lsit,config)
        else
           call mat_init(CMO,nb,nb)
           funit = -1
           call lsopen(funit,'cmo_orbitals.u','old','unformatted')
           call mat_read_from_disk(funit,CMO,.true.)
           call lsclose(funit,'KEEP')
           call dec_main_prog_input(lsit,config,F,D,CMO,dummy)
           call mat_free(CMO)
        end if

        ! reset data
        DECinfo = DECinfo_copy

     end if

  end subroutine lofex_ccs_starting_guess

end module dec_main_mod
