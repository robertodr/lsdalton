!> @file
!> Module containing wrapper functions for using QcMatrix library
!> \author Bin Gao
!> \date 2014-06-15
module ls_qcmatrix_wrapper

    use precision, only: INTK
    use qcmatrix_backend
    use qcmatrix_f

    implicit none

    ! IO unit of standard output
    integer(kind=4), parameter, private :: STDOUT = 6
!FIXME: one component, should be defined in other module of LSDALTON?
    integer(kind=INTK), parameter, public :: NUM_COMPONENTS = 1_INTK
    integer(kind=INTK), parameter, public :: IDX_BLOCKS(2) = (/ &
        1_INTK,1_INTK/)

    public :: ls_qcmatrix_create
    public :: ls_qcmatrix_zero_mat
    public :: ls_qcmatrix_get_host_mat

    interface ls_qcmatrix_get_host_mat
        module procedure ls_qcmatrix_get_host_Matrix, &
                         ls_qcmatrix_get_host_Matrixp
    end interface ls_qcmatrix_get_host_mat

    contains

    !> \brief Creates QcMatrix arrays
    !> \author Jogvan Magnus Haugaard Olsen
    !> \date 2018-03-12
    !> \return created A matrices
    subroutine ls_qcmatrix_create(A)
        type(QcMat), dimension(:), intent(inout) :: A
        integer(kind=INTK) imat
        integer(kind=4) :: ierr
        do imat = 1, size(A)
            ierr = QcMatCreate_f(A=A(imat))
            if (ierr /= QSUCCESS) then
                write(STDOUT,'(2i8)') imat, size(A)
                call lsquit('ls_qcmatrix_create>> call QcMatCreate_f()', -1_INTK)
            end if
        end do
    end subroutine ls_qcmatrix_create

    !> \brief Returns zero matrices
    !> \author Bin Gao
    !> \date 2015-04-02
    !> \param num_row Number of rows
    !> \param num_col Number of columns
    !> \return A Matrices to be zeroed
    subroutine ls_qcmatrix_zero_mat(A, num_row, num_col)
        type(QcMat), intent(inout) :: A(:)
        integer(kind=INTK), intent(in) :: num_row
        integer(kind=INTK), intent(in) :: num_col
        ! checks if the matrix is assembled or not
        logical(kind=4) assembled
        ! incremental recorder
        integer(kind=INTK) imat
        ! error information
        integer(kind=4) ierr
        do imat = 1, size(A)
            ! checks if the matrix is assembled or not
            ierr = QcMatIsAssembled_f(A=A(imat), &
                                      assembled=assembled)
            if (ierr/=QSUCCESS) then
                write(STDOUT,100) imat, size(A)
                call lsquit('ls_qcmatrix_zero_mat>> call QcMatIsAssembled_f()', &
                            -1_INTK)
            end if
            if (.not.assembled) then
                ierr = QcMatBlockCreate_f(A=A(imat), &
                                          dim_block=NUM_COMPONENTS)
                if (ierr/=QSUCCESS) then
                    write(STDOUT,100) imat, size(A)
                    call lsquit('ls_qcmatrix_zero_mat>> call QcMatBlockCreate_f()', &
                                -1_INTK)
                end if
                ierr = QcMatSetSymType_f(A=A(imat), &
                                         sym_type=QSYMMAT)
                if (ierr/=QSUCCESS) then
                    write(STDOUT,100) imat, size(A)
                    call lsquit('ls_qcmatrix_zero_mat>> call QcMatSetSymType_f()', &
                                -1_INTK)
                end if
                ierr = QcMatSetDataType_f(A=A(imat),                     &
                                          num_blocks=NUM_COMPONENTS,     &
                                          idx_block_row=IDX_BLOCKS(1:1), &
                                          idx_block_col=IDX_BLOCKS(2:2), &
                                          data_type=(/QREALMAT/))
                if (ierr/=QSUCCESS) then
                    write(STDOUT,100) imat, size(A)
                    call lsquit('ls_qcmatrix_zero_mat>> call QcMatSetDataType_f()', &
                                -1_INTK)
                end if
                ierr = QcMatSetDimMat_f(A=A(imat),       &
                                        num_row=num_row, &
                                        num_col=num_col)
                if (ierr/=QSUCCESS) then
                    write(STDOUT,100) imat, size(A)
                    call lsquit('ls_qcmatrix_zero_mat>> call QcMatSetDimMat_f()', &
                                -1_INTK)
                end if
                ierr = QcMatAssemble_f(A=A(imat))
                if (ierr/=QSUCCESS) then
                    write(STDOUT,100) imat, size(A)
                    call lsquit('ls_qcmatrix_zero_mat>> call QcMatAssemble_f()', &
                                -1_INTK)
                end if
            end if
            ierr = QcMatZeroEntries_f(A=A(imat))
            if (ierr/=QSUCCESS) then
                write(STDOUT,100) imat, size(A)
                call lsquit('ls_qcmatrix_zero_mat>> call QcMatZeroEntries_f()', &
                            -1_INTK)
            end if
        end do
        return
100     format("ls_qcmatrix_zero_mat>> ",2I8)
    end subroutine ls_qcmatrix_zero_mat

    !> \brief Gets LSDALTON Matrix type from QcMat type matrix
    !> \author Bin Gao
    !> \date 2015-04-02
    !> \param A QcMat type matrix
    !> \param data_type Data type of LSDALTON Matrix type
    !> \return A_mat LSDALTON Matrix type
    subroutine ls_qcmatrix_get_host_Matrix(A, data_type, A_mat)
        use Matrix_module, only: Matrix
        use matrixp_operations, only: Matrixp_GetMat
        type(QcMat), intent(in) :: A
        integer(kind=INTK), intent(in) :: data_type
        type(Matrix), pointer, intent(inout) :: A_mat
        type(Matrixp), pointer :: A_host
        integer(kind=4) ierr
        ierr = QcMatGetExternalMat_f(A=A,                         &
                                     idx_block_row=IDX_BLOCKS(1), &
                                     idx_block_col=IDX_BLOCKS(2), &
                                     data_type=data_type,         &
                                     A_ext=A_host)
        if (ierr/=QSUCCESS) then
            call lsquit('ls_qcmatrix_get_host_Matrix>> failed to call QcMatGetExternalMat_f()', &
                        -1_INTK)
        end if
        call Matrixp_GetMat(A_host, A_mat)
        return
    end subroutine ls_qcmatrix_get_host_Matrix

    !> \brief Gets array of LSDALTON Matrixp type from QcMat type matrices
    !> \author Bin Gao
    !> \date 2015-04-02
    !> \param A QcMat type matrices
    !> \param data_type Data types of LSDALTON Matrixp type
    !> \return A_matp array of LSDALTON Matrixp type
    subroutine ls_qcmatrix_get_host_Matrixp(A, data_type, A_matp)
        use Matrix_module, only: Matrixp
        use matrixp_operations, only: Matrixp_Associate
        type(QcMat), intent(in) :: A(:)
        integer(kind=INTK), intent(in) :: data_type(:)
        type(Matrixp), intent(inout) :: A_matp(:)
        type(Matrixp), pointer :: A_host
        integer(kind=INTK) imat
        integer(kind=4) ierr
        if (size(A)/=size(data_type) .or. size(A)/=size(A_matp)) then
            write(STDOUT,"(A,3I8)") "ls_qcmatrix_get_host_Matrixp>> ", &
                                    size(A), size(data_type), size(A_matp)
            call lsquit('ls_qcmatrix_get_host_Matrixp>> inconsistent sizes', &
                        -1_INTK)
        end if
        do imat = 1, size(A)
            ierr = QcMatGetExternalMat_f(A=A(imat),                   &
                                         idx_block_row=IDX_BLOCKS(1), &
                                         idx_block_col=IDX_BLOCKS(2), &
                                         data_type=data_type(imat),   &
                                         A_ext=A_host)
            if (ierr/=QSUCCESS) then
                call lsquit('ls_qcmatrix_get_host_Matrixp>> failed to call QcMatGetExternalMat_f()', &
                            -1_INTK)
            end if
            call Matrixp_Associate(A_matp(imat), A_host)
        end do
        return
    end subroutine ls_qcmatrix_get_host_Matrixp

end module ls_qcmatrix_wrapper
