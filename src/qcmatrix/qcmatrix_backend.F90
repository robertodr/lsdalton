!> @file
!> A backend matrix module for QcMatrix library, which will invoke the
!> type(Matrixp) module of LSDALTON
!> \author Bin Gao
!> \date 2015-02-20
module qcmatrix_backend

    use Matrix_module, only: Matrixp
    use matrixp_operations, only:                          &
        Matrix_Create => Matrixp_Create,                   &
        Matrix_Destroy => Matrixp_Destroy,                 &
        Matrix_SetSymType => Matrixp_SetSymType,           &
        Matrix_SetDimMat => Matrixp_SetDimMat,             &
        Matrix_Assemble => Matrixp_Assemble,               &
        Matrix_GetSymType => Matrixp_GetSymType,           &
        Matrix_GetDimMat => Matrixp_GetDimMat,             &
        Matrix_IsAssembled => Matrixp_IsAssembled,         &
        Matrix_SetValues => Matrixp_SetValues,             &
        Matrix_AddValues => Matrixp_AddValues,             &
        Matrix_GetValues => Matrixp_GetValues,             &
        Matrix_Duplicate => Matrixp_Duplicate,             &
        Matrix_ZeroEntries => Matrixp_ZeroEntries,         &
        Matrix_GetTrace => Matrixp_GetTrace,               &
        Matrix_GetMatProdTrace => Matrixp_GetMatProdTrace, &
        Matrix_Write => Matrixp_Write,                     &
        Matrix_Read => Matrixp_Read,                       &
        Matrix_Scale => Matrixp_Scale,                     &
        Matrix_AXPY => Matrixp_AXPY,                       &
        Matrix_Transpose => Matrixp_Transpose,             &
        Matrix_GEMM => Matrixp_GEMM

    implicit none

end module qcmatrix_backend
