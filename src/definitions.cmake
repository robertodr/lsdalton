# Set directory-global compile definitions. This hurts my eyes
# NOTE Cannot use the target for MPI (as for OpenMP) as we perform some "magic"
#  for other implementations
list(APPEND _definitions
  $<$<CONFIG:Debug>:VAR_LSDEBUGINT>
  $<$<CONFIG:Debug>:VAR_LSDEBUG>
  $<$<TARGET_EXISTS:OpenMP::OpenMP_Fortran>:VAR_OMP>
  $<$<BOOL:${MPI_FOUND}>:VAR_MPI>
  $<$<BOOL:${USE_32BIT_MPI_INTERFACE}>:VAR_MPI_32BIT_INT>
  $<$<BOOL:${MPI3_FEATURES_WORK}>:VAR_HAVE_MPI3>
  $<$<BOOL:${ENABLE_64BIT_INTEGERS}>:VAR_INT64>
  $<$<BOOL:${HAS_STD_2003}>:COMPILER_UNDERSTANDS_FORTRAN_2003>
  $<$<Fortran_COMPILER_ID:Cray>:VAR_CRAY>
  $<$<Fortran_COMPILER_ID:GNU>:VAR_GFORTRAN>
  $<$<Fortran_COMPILER_ID:Intel>:VAR_IFORT>
  $<$<Fortran_COMPILER_ID:PGI>:VAR_PGI;VAR_PGF90>
  $<$<Fortran_COMPILER_ID:XL>:VAR_XLF>
  )

if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
  list(APPEND _definitions
    SYS_LINUX
    SYS_UNIX
    )
endif()

if(CMAKE_SYSTEM_NAME STREQUAL "Darwin")
  list(APPEND _definitions
    SYS_DARWIN
    SYS_UNIX
    )
endif()

if(CMAKE_SYSTEM_NAME STREQUAL "AIX")
  list(APPEND _definitions
    SYS_AIX
    SYS_UNIX
    )
endif()

if(HAVE_MKL_LAPACK)
  list(APPEND _definitions
    VAR_MKL
    $<$<BOOL:${ENABLE_64BIT_INTEGERS}>:VAR_LAPACK_INT64>
    )
  if(ENABLE_64BIT_INTEGERS)
    list(APPEND _definitions
      VAR_LAPACK_INT64
      )
  endif()
endif()

if(ENABLE_XCFUN)
  list(APPEND _definitions
    VAR_XCFUN
    )
endif()

if(ENABLE_FRAME)
  list(APPEND _definitions
    ENABLE_FRAME
    )
endif()

if(ENABLE_PCMSOLVER)
  list(APPEND _definitions
    HAS_PCMSOLVER
    )
endif()

if(ENABLE_DEC)
  list(APPEND _definitions
    VAR_DEC
    )
endif()

if(ENABLE_BUILD_TITAN)
  list(APPEND _definitions
    VAR_HAVE_MPI3
    )
endif()

if(ENABLE_CHEMSHELL)
  list(APPEND _definitions
    VAR_CHEMSHELL
    )
endif()

if(ENABLE_CSR)
  list(APPEND _definitions
    VAR_MKL
    VAR_CSR
    )
endif()

if(ENABLE_SCALAPACK)
  list(APPEND _definitions
    VAR_MKL
    VAR_SCALAPACK
    )
endif()

if(ENABLE_TIMINGS)
  list(APPEND _definitions
    VAR_TIME
    )
endif()

if(ENABLE_RSP)
  list(APPEND _definitions
    VAR_RSP
    )
endif()

if(ENABLE_GPU)
  list(APPEND _definitions
    VAR_CUDA
    VAR_OPENACC
    )
  if(ENABLE_CUBLAS)
    list(APPEND _definitions
      VAR_CUBLAS
      )
  endif()
endif()

if(ENABLE_QCMATRIX)
  list(APPEND _definitions
    ENABLE_LS_QCMATRIX
    )
endif()

if(ENABLE_OPENRSP)
  list(APPEND _definitions
    ENABLE_LS_OPENRSP
    )
endif()

if(ENABLE_TENSORS)
  list(APPEND _definitions
    VAR_ENABLE_TENSORS
    )
endif()

if(ENABLE_PYTHON_INTERFACE)
  list(APPEND _definitions
    HAS_PYTHON_INTERFACE
    )
endif()

set_directory_properties(
  PROPERTY
    COMPILE_DEFINITIONS
      "${_definitions}"
  )
# "Fai una lista / delle cose che non vuoi"
unset(_definitions)
