#!/usr/bin/env python

from pathlib import Path
from subprocess import check_output

from cffi import FFI

ffibuilder = FFI()

definitions = ["-DPYLSDALTON_API=", "-DPYLSDALTON_NOINCLUDE"]
header = Path(__file__).resolve().parent / "pylsdalton.h"
command = ["cc", "-E"] + definitions + [str(header)]
interface = check_output(command).decode("utf-8")

# remove possible \r characters on windows which
# would confuse cdef
_interface = [l.strip("\r") for l in interface.split("\n")] + [
    'extern "Python+C" void logger(const char*, int);'
]

# cdef() expects a single string declaring the C types, functions and
# globals needed to use the shared object. It must be in valid C syntax.
ffibuilder.cdef("\n".join(_interface))

# set_source() gives the name of the python extension module to
# produce, and some C source code as a string.  This C code needs
# to make the declared functions, types and globals available,
# so it is often just the "#include".
ffibuilder.set_source(
    "_pylsdalton",
    """
     #include "pylsdalton.h"
""",
)

ffibuilder.emit_c_code("_pylsdalton.c")
