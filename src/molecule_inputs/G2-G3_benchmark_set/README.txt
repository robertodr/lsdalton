These molecules belong to the Gaussian benchmark sets:
      * G3/05 Test set (http://www.cse.anl.gov/OldCHMwebsiteContent/compmat/g3-05.htm)
      * G3/99 Test Set (http://www.cmt.anl.gov/OldCHMwebsiteContent/compmat/G3-99.htm)
      * G2/97 Test Set (http://www.cmt.anl.gov/OldCHMwebsiteContent/compmat/G2-97.htm)

you can find the original inputs in the Gaussian format in the directory: Gaussian_original_inputs/



*** G2/97 Test Set:
    G2 theory [1] was originally tested on a total of 125 reaction energies, chosen because they have well-established experimental values.
    This test set has been expanded to include larger, more diverse molecules and is referred to as the G2/97 test set.
    It includes 148 enthalpies of formation, 88 ionization potentials, 58 electron affinities, and 8 proton affinities.
    The comprehensive set contains 302 entries.[2,3]
	* MP2(full)/6-31G* geometries
	  - Neutrals
	  - Ions
	* B3LYP/6-31G* geometries
	  - Neutrals
	  - Ions

*** G2/97 Test Set:
    The G2/97 test set [4,5] for assessing quantum chemical methods was expanded to include 75 additional enthalpies of formation.
    This new set, referred to as the G3/99 test set, has 222 enthalpies of formation, 88 ionization potentials, 58 electron affinities, and 8 proton affinities.[6]
    The total number of energies in G3/99 is 376.
    The additional 75 MP2(FU)/6-31G* geometries are available here.
    Also located here are the B3LYP/6-31G* geometries for these species.
	 * MP2(full)/6-31G* geometries
	 * B3LYP/6-31G* geometries

*** G3/05 Test Set:
    The G3/99 test set [7] for assessing quantum chemical methods was expanded to include 78 additional energies.
    This new set is referred to as the G3/05 test set and it has a total of 454.[8]
    The additional 78 MP2(FU)/6-31G* geometries are available here.
    Also located here are the B3LYP/6-31G(2df,p) geometries for these species.
	 * MP2(full)/6-31G* geometries of hydrogen bonded dimers
	 * MP2(full)/6-31G* geometries of 14 additional nonhydrogens
	 * MP2(full)/6-31G* geometries of third row species
	 * B3LYP/6-31G2df,p) geometries of hydrogen bonded dimers
	 * B3LYP/6-31G2df,p) geometries of 14 additional nonhydrogens
	 * B3LYP/6-31G2df,p) geometries of third row species
 

    1. references where presented:
       [1] "Gaussian-2 theory for molecular energies of first- and second-row compound",
	    L.A. Curtiss, K. Raghavachari, G. W. Trucks, and J. A. Pople, Journal of Chemical Physics 94, 7221 (1991).
       [2] "Assessment of Gaussian-2 and Density Functional Methods for the Computation of Enthalpies of Formation",
	    L. A. Curtiss, K. Raghavachari, P. C. Redfern, and J. A. Pople, Journal of Chemical Physics 106, 1063 (1997).
       [3] "Assessment of Gaussian-2 and Density Functional Methods for the Computation of Ionization Energies and Electron Affinities",
	    L. A. Curtiss, P. C. Redfern, K. Raghavachari, and J. A. Pople, Journal of Chemical Physics, 109, 42 (1998).
       [4] "Assessment of Gaussian-2 and Density Functional Methods for the Computation of Enthalpies of Formation",
	    L. A. Curtiss, K. Raghavachari, P. C. Redfern, and J. A. Pople, Journal of Chemical Physics 106, 1063 (1997).
       [5] "Assessment of Gaussian-2 and Density Functional Methods for the Computation of Ionization Energies and Electron Affinities",
	    L. A. Curtiss, P. C. Redfern, K. Raghavachari, and J. A. Pople, Journal of Chemical Physics, 109, 42 (1998).
       [6] "Assessment of Gaussian-3 and Density Functional Theories for a Larger Test Set",
            L. A. Curtiss, K. Raghavachari, P. C. Redfern, and J. A. Pople, Journal of Chemical Physics, 112, 7374 (2000).
       [7] "Assessment of Gaussian-3 and Density Functional Theories for a Larger Test Set",
	    L. A. Curtiss, K. Raghavachari, P. C. Redfern, and J. A. Pople, Journal of Chemical Physics, 112, 7374 (2000).
       [8] "Assessment of Gaussian-3 and Density Functional Theories on the G3/05 Test Set of Experimental Geometries",
            Journal of Chemical Physics, 123, 124107 (2005).

    2. references where used:
       Future article on the "PARI approximation (Pair-Atomic Resolution-of-the-Identity)"

    3. list of atoms it contains:

    4. size of molecules involved:

    5. closed and open shell molecules are present in this benchmark sets

    6. properties it has been benchmarked for:

    7. Which basis set it has been used for:
       6-31G/df-def2,
       6-31G*/df-def2,
       cc-pVTZ/cc-pVTZdenfit,
       cc-pVTZ/cc-pVTZ-RI

