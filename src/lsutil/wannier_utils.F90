!> @brief Subroutines that manipulates the derived types in wannier types.
module wannier_utils


  use precision
  use wannier_types, only: wannier_config, bravais_lattice
  !TODO indxtocoor = indxtocoorpt
  !TODO integer parameter N, ... change names use eqs
  !TODO
  use fundamental, only: bohr_to_angstrom, pi
  use typedeftype, only: lssetting
  use matrix_operations, only: mat_free
  use molecule_typetype, only: moleculeinfo
  use memory_handling, only: mem_alloc

  
  implicit none
  private
  public :: &
       & wannier_read_lattice_vectors, wu_coortoindx, wu_indxtolatvec, wu_indxtocoor, &
       & WU_LMAX, wtranslate_molecule, wcopy_atompositions, wu_coortolatvec, w_set_kmesh
  !& wu_init_lattice_cells, wu_delete_lattice_cells, wu_isredundant1d, &
  !& wu_isredundant2d, wu_isredundant3d

  ! Largest number of layers = 644 (Largest that can be represented 
  ! with int32 using our indexinscheme) 
  integer, parameter :: N = 1289  ! N = 2L+1 
  integer, parameter :: N2 = 1661521  ! N^2 
  integer, parameter :: NMIN1OVER2 = 644  ! L
  integer, parameter :: N2MIN1OVER2 = 830760 ! (N^2 - 1)/2 
  integer, parameter :: WU_LMAX = NMIN1OVER2

contains

  !> @brief Read lattice vectors from MOL file
  !> @param luinfo Logical unit for input. 
  !> @param ll Definition of bravais lattice.
  !> @param angstrom Bohr or angstrom as length unit.
  !> @param lupri Logical print unit.
  !> @author KRL
  !> @date 2015
  subroutine wannier_read_lattice_vectors(luinfo, ll, angstrom, lupri)
    integer, intent(in) :: lupri, luinfo
    type(bravais_lattice), intent(inout) :: ll
    logical, intent(in) :: angstrom

    integer :: i, j

    call read_latvec(luinfo, ll%lvec(:, 1), ll%dim_is_periodic(1), 'a1', lupri)
    call read_latvec(luinfo, ll%lvec(:, 2), ll%dim_is_periodic(2), 'a2', lupri)
    call read_latvec(luinfo, ll%lvec(:, 3), ll%dim_is_periodic(3), 'a3', lupri)
    if (angstrom) then
       ll%lvec(:,:)=ll%lvec(:,:)/bohr_to_angstrom 
    endif

    ! count the number of periodic dimenstions
    ll%dims = 0 
    do i = 1, 3
       if (ll%dim_is_periodic(i)) then 
          ll%dims = ll%dims + 1
       endif
    enddo

    ! if 2D or iD lattice, always the last vector(s) should be nonperiodic
    loop: do
       i = 1
       do while (ll%dim_is_periodic(i))
          if (i == 2) exit loop
          i = i + 1
       enddo
       j = i + 1
       loop2: do 
          if (j==4) exit loop
          if (ll%dim_is_periodic(j)) exit loop2 
          j = j + 1
       enddo loop2
       ll%dim_is_periodic(i) = .true.
       ll%dim_is_periodic(j) = .false.
       ll%lvec(:, i) = ll%lvec(:, j)
    enddo loop

    ! set all nonperiodic lattice vectors to 0
    do i = 1, 3
       if (.not. ll%dim_is_periodic(i)) then
          ll%lvec(:, i) = 0.0_realk
       endif
    enddo

  end subroutine wannier_read_lattice_vectors

  !> @brief Read one lattice vector and info about periodicity 
  !> (one single line from .mol file)
  !> @param luinfo Logical unit for input. 
  !> @param lvec Lattice vector.
  !> @param periodic PBC along this lvec?.
  !> @param vname As written in .mol file, a1, a2 or a3.
  !> @param lupri Logical print unit.
  !> @author KRL
  !> @date 2015
  subroutine read_latvec(luinfo, lvec, periodic, vname, lupri)
    integer, intent(in) :: lupri, luinfo
    real(realk), intent(inout) :: lvec(3)
    logical, intent(inout) :: periodic
    character (len=2), intent(in) :: vname

    character(len=80) :: templine
    integer :: ipos, ipos2
    character(len=10) :: activedim
    logical :: inputerror

    inputerror = .false.

    read (luinfo, '(a80)') templine
    ipos = index(templine, vname)
    if (ipos == 0) then
       inputerror = .true.
    else if (ipos > 0) then
       ipos2 = index(templine(ipos:), '=')
       if ( (ipos2 == 0) .or. (ipos2 > 7) ) then
          inputerror = .true.
       else
          read (templine(ipos+ipos2:80),*) &
               & lvec(1), lvec(2), lvec(3), activedim
          if (activedim(1:8) == 'inactive') then 
             periodic =  .false.
          else if (activedim(1:6) == 'active') then
             periodic =  .true.
          else
             inputerror = .true.
          endif
       endif
    endif

    if (inputerror) then
       write (lupri, *) 'Error in mol file:'
       write (lupri, *) 'incorrect input for lattice vectors'
       write (lupri, *) 'format is ai = ai1 ai2 ai3 <active/inactive>.'
       write (lupri, *) 'All a1, a2, a3 must be specified in the given order'
       call lsquit('incorrect input for lattice vectors', lupri)
    endif
  end subroutine read_latvec

  !> @brief Calculate lattice coor from lattice index. 
  !> @param lat Defines bravais lattice.
  !> @param i lattice index.
  !> @date 2015
  !> @author KRL
  pure function wu_indxtocoor(i)
    integer, intent(in) :: i
    integer :: wu_indxtocoor(3)
    integer :: j, r(3)

    r(3) = (i + sign(1, i)*N2MIN1OVER2)/N2
    j = i - r(3)*N2
    r(2) = (j + sign(1, j)*NMIN1OVER2)/N
    r(1) = j - r(2)*N
    wu_indxtocoor = r
  end function wu_indxtocoor

  !> @brief Calculate lattice vector from lattice index. 
  !> @param lat Defines bravais lattice.
  !> @param i lattice index.
  !> @date 2015
  !> @author KRL
  pure function wu_indxtolatvec(blat, i)
    integer, intent(in) :: i
    type(bravais_lattice), intent(in) :: blat
    real(realk) :: wu_indxtolatvec(3)

    wu_indxtolatvec = matmul(blat%lvec, wu_indxtocoor(i))
  end function wu_indxtolatvec

  !> @brief Calculate lattice vector from coordinate point. 
  !> @param lat Defines bravais lattice.
  !> @param coor Coordinate point.
  !> @date 2015
  !> @author KRL
  pure function wu_coortolatvec(blat, coor)
    integer, intent(in) :: coor(3)
    type(bravais_lattice), intent(in) :: blat
    real(realk) :: wu_coortolatvec(3)

    wu_coortolatvec = matmul(blat%lvec, coor)
  end function wu_coortolatvec

  !> @brief Calculate lattice index from lattice coordinate. 
  !> @param r Integers r=(x,y,z) is the lattice coordinates.
  !> @date 2015
  !> @author KRL
  pure integer function wu_coortoindx(r)
    integer, intent(in) :: r(3)

    wu_coortoindx = r(1) + r(2)*N + r(3)*N2
  end function wu_coortoindx

  !!> @brief .
  !!> @author KRL
  !!> @date 2015
  !!> @param
  !!> @param
  !!> @param
  !subroutine wget_cell(blat, indx, refcell, cell)
  !	use molecule_typetype, only: moleculeinfo
  !	type(moleculeinfo), intent(in) :: refcell
  !	type(moleculeinfo), intent(inout) :: cell
  !	type(bravais_lattice), intent(in) :: blat
  !	integer, intent(in) :: indx
  !
  !	call wcopy_atompositions(mol3, refcell)
  !	call wtranslate_molecule(mol3, wu_indxtolatvec(blat, indx))
  !end subroutine
  !
  ! Below not blat dep.
  !


  ! todo move to molecule operations
  !> @brief Translate all atomcenters in a molecule by the vector transl.
  !> @author KRL
  !> @date 2015
  !> @param
  !> @param
  !> @param
  subroutine wtranslate_molecule(molecule, transl)
    
    type(moleculeinfo), intent(inout) :: molecule
    real(realk) :: transl(3)

    integer :: i
    do i = 1, molecule%natoms
       molecule%atom(i)%center(:) = &
            & molecule%atom(i)%center(:) + transl
    enddo
  end subroutine wtranslate_molecule

  !> @brief Copy all atompositions from molecule to another (must have the 
  !> same number of atoms)
  !> @author KRL
  !> @date 2015
  !> @param
  !> @param
  !> @param
  subroutine wcopy_atompositions(molecule1, molecule2)
    
    type(moleculeinfo), intent(inout) :: molecule1
    type(moleculeinfo), intent(in) :: molecule2

    integer :: i
    if (molecule1%natoms /= molecule2%natoms) then
       call lsquit('mod: wannier_utils, s.r.:wcopy_atompositions. Input molecules &
            & must have the same number of atoms.', -1)
    endif
    do i = 1, molecule1%natoms
       molecule1%atom(i)%center(:) = &
            & molecule2%atom(i)%center(:)
    enddo
  end subroutine wcopy_atompositions


  !> @brief Initiate the reciprocal vectors.
  !> @author KRL
  !> @date 2015
  !> @param
  !> @param
  !> @param
  function wget_reciprocal_vectors(blat) result(b)
    
    type(bravais_lattice), intent(in) :: blat

    real(realk), dimension(3, 3) :: a, b
    integer :: i, j, k

    do i = 1, 3
       do j = 1, 3
          a(i, j) = blat%lvec(i, j)
       enddo
    enddo
    if (blat%dims < 3) then
       a(:, 3) = [0._realk, 0._realk, 1._realk]
    endif
    if (blat%dims < 2) then
       a(:, 2) = [0._realk, 1._realk, 0._realk]
    endif

    b = 0._realk
    b(:, 1) = recvec(1, 2, 3)
    if (blat%dims == 1) return
    b(:, 2) = recvec(2, 3, 1)
    if (blat%dims == 2) return
    b(:, 3) = recvec(3, 1, 2)

  contains 

    function recvec(x, y, z)
      integer :: x, y, z
      real(realk), dimension(3) :: recvec
      recvec = 2 * pi * cross(a(:, y), a(:, z)) 
      recvec = recvec / dot_product(a(:, x), cross(a(:, y), a(:, z)))
    end function recvec

    function cross(x1, x2)
      real(realk), dimension(3) :: cross
      real(realk), dimension(3), intent(in) :: x1, x2
      cross(1) = x1(2) * x2(3) - x1(3) * x2(2)
      cross(2) = x1(3) * x2(1) - x1(1) * x2(3)
      cross(3) = x1(1) * x2(2) - x1(2) * x2(1)
    end function cross

  end function wget_reciprocal_vectors

  !> @brief Get k - mesh of possible k - points for a given bvk - cell
  !> given by n = (nx, ny, nz). The total number of kpoints ntot is calculated 
  !> and set. The gridpoint array is allocated here. NB: remember to deallocate.
  !> @author KRL
  !> @date 2015
  !> @param
  !> @param
  !> @param

  subroutine w_set_kmesh(gridpoints, ntot, blat, ninp)

    integer, intent(in) :: ninp(3)
    integer, intent(inout) :: ntot
    type(bravais_lattice), intent(in) :: blat
    real(realk), pointer, intent(inout) :: gridpoints(:, :)

    real(realk) :: tmp1(3), tmp2(3), tmp3(3), recvec(3, 3)
    integer :: i, j, k, counter, n(3)
    real(realk) :: ntmp

    ! copy ninp to n
    do i = 1, blat%dims
       n(i) = ninp(i)
    enddo
    ! make sure that n(i) = 1 for "non - active dimentions"
    do i = blat%dims + 1, 3
       n(i) = 1
    enddo

    call mem_alloc(gridpoints, 3, ntot)
    gridpoints(:, :) = 0._realk

    recvec =  wget_reciprocal_vectors(blat)
    !!	  ! debug
    !!	  write (*, *) 'The reciprocal vector is:'
    !!	  write (*, *) recvec(:, 1)
    !!	  write (*, *) recvec(:, 2)
    !!	  write (*, *) recvec(:, 3)
    !!	  write (*, *) 'ntot = ', ntot
    !!	  write (*, *) ''

    counter = 1
    do i = 1, n(1)
       ntmp = real(n(1), realk)
       tmp1 = ( (2._realk * i - ntmp - 1._realk) / &
            & ( 2._realk * ntmp) ) * recvec(:, 1) 
       ! tmp = ( (2 * n(1) - i - 1) / ( 2 * n(1)) ) * recvec(:, 1)  ?

       do j = 1, n(2)
          ntmp = real(n(2), realk)
          tmp2 = ( (2._realk * j - ntmp - 1._realk) / &
               & ( 2._realk * ntmp) ) * recvec(:, 2)

          do k = 1, n(3)
             ntmp = real(n(3), realk)
             tmp3 = ( (2._realk * k - ntmp - 1._realk) &
                  & / ( 2._realk * ntmp) ) * recvec(:, 3)

             gridpoints(:, counter) = tmp1 + tmp2 + tmp3
             counter = counter + 1

          enddo
       enddo
    enddo

  end subroutine w_set_kmesh

end module wannier_utils
