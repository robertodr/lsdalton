!@ file ls_shell.F90
!> \brief Collect and store AO shell information
!> \author Simen Reine
!> \date 2016-10-13
!>
!> NOTE: here shell mean only ONE contracted function (to agree with the format needed for XCINT)
!>
MODULE ls_shells
use precision
use molecule_typetype, only: moleculeinfo
use basis_typetype, only: basisinfo,basissetinfo
use memory_handling, only: mem_alloc, mem_dealloc

public init_shell_info,free_shell_info,shell_info_type,print_shell_info

!> The AO shell information in Structure-of-Array (SoA) format (written with Gaussians in mind, 
!> but can be used for Slater or other exponent based structures)
TYPE shell_info_type
  !> The number of centers (typically the nuclei, but can in principle be placed anywhere in space)
  Integer             :: ncents
  !> The number of shells. Shell here means all AOs from fixed angular momenta and one primitive contraction
  Integer             :: nshells
  !> The total number of primitives for all shells
  Integer             :: nprim_tot
  !> Index that for given shell point to its center 
  Integer,pointer     :: icent(:) !nshells
  !> The angular momentum quantum number of each shell
  Integer,pointer     :: ang_mom(:) !nshells
  !> The number of primitive of each shell
  Integer,pointer     :: nprim(:) !nshells
  !> The starting primitive index (of nprim_tot) for each shell
  Integer,pointer     :: start_prim(:) !nshells
  !> The Cartesian coordinates of the centers
  Real(realk),pointer :: center(:,:)   !3,ncents
  !> The primitive exponents, shell i exponents range from start_prim(i) to start_prim(i)+nprim(i)-1
  Real(realk),pointer :: exponents(:)   !nprim_tot
  !> The normalized contraction coefficients, range as for the exponents
  Real(realk),pointer :: cont_coeff(:)  !nprim_tot
END TYPE shell_info_type

private
CONTAINS
!> \brief Collects the AO shell information in SoA fashion (and allocates shell_info)
!> \param shell_info The object contaning the shell information
!> \param basis The basis set information
!> \param basis_info Specifies which basis set to be used (RegBasParam, ADMBasParam, etc.)
!> \param molecule The molecular information
!> \param uncontracted Use an uncontracted basis set
!> \author Simen Reine
!> \date 2016-10-13
SUBROUTINE init_shell_info(shell_info,basis,basis_type,molecule,uncontracted)
implicit none
TYPE(shell_info_type),intent(INOUT) :: shell_info
TYPE(basisinfo),intent(IN),target   :: basis
Integer,intent(IN)                  :: basis_type
TYPE(moleculeinfo),intent(IN)       :: molecule
Logical,intent(IN)                  :: uncontracted
!
TYPE(basissetinfo),pointer :: AObasis
Integer                    :: ncent,nshell,nprim_tot,icent,ishell,iprim,R
Integer                    :: n,c,iAngmom,iCharge,iCont,ioff,iSegment,atom_type
real(realk),pointer        :: ex(:),CC(:)

!Consistenct testing of the basis set type
IF ((basis_type.LE.0).OR.(basis_type.GE.size(Basis%BINFO))) THEN
  write(*,*) 'Programming error: init_shell_info called with basis_type=',basis_type
  write(*,*) 'Allowed range is from 1 to',size(Basis%BINFO)
  call lsquit('Programming error: init_shell_info called with basis_type outside allowed range',-1)
ENDIF

!ToDo need to figure out how to handle uncontracted basis sets
IF (uncontracted) call lsquit('Error in init_shell_info: uncontracted basis set not implemented for this routine',-1)

!Point to the basis of the right type
AObasis => Basis%BINFO(basis_type)


!Pick up the number of centers with orbitals
ncent = 0
DO icent=1,molecule%natoms
  IF(MOLECULE%ATOM(icent)%pointcharge) CYCLE !point charges have no AO basis functions
  ncent = ncent+1
ENDDO
shell_info%ncents = ncent

!Count the number of shells and primitives
R = AObasis%Labelindex
nshell    = 0
nprim_tot = 0
DO icent=1,ncent

  !For BASIS input each atom of the same type (H,C,O,N,etc.) has the same basis set structure. In this case
  !R is not equal to zero. The R=0 case comes from cases where for instance two C atoms have a different 
  !set of basis functions. In either case we need to pick up the atom type of the center in order to 
  !access the basis set information from the basis set structure
  IF (R.EQ.0) THEN 
    iCharge = int(molecule%atom(icent)%charge)
    atom_type = AObasis%chargeindex(icharge)
  ELSE
    atom_type = molecule%atom(icent)%IDtype(R)
  ENDIF

  IF(AObasis%atomtype(atom_type)%nAngmom.EQ.0) CYCLE !uncertain about the purpose of this, copied from BUILD_AO

  !Actual counting
  !ToDo, make special case for uncontracted basis
  DO iAngmom=1,AObasis%atomtype(atom_type)%nAngmom
    DO isegment=1,AObasis%atomtype(atom_type)%shell(iAngmom)%nsegments
      n = AObasis%atomtype(atom_type)%shell(iAngmom)%segment(isegment)%nrow !# of primitives
      c = AObasis%atomtype(atom_type)%shell(iAngmom)%segment(isegment)%ncol !# of contracted
      nprim_tot = nprim_tot + n * c
      nshell    = nshell + c
    ENDDO
  ENDDO
ENDDO
shell_info%nshells   = nshell
shell_info%nprim_tot = nprim_tot

call mem_alloc(shell_info%icent,nshell)
call mem_alloc(shell_info%ang_mom,nshell)
call mem_alloc(shell_info%nprim,nshell)
call mem_alloc(shell_info%start_prim,nshell)
call mem_alloc(shell_info%center,3,ncent)
call mem_alloc(shell_info%exponents,nprim_tot)
call mem_alloc(shell_info%cont_coeff,nprim_tot)

!Extract information
!Count the number of shells and primitives
R = AObasis%Labelindex
ishell = 0
iprim  = 0
DO icent=1,ncent

  !For BASIS input each atom of the same type (H,C,O,N,etc.) has the same basis set structure. In this case
  !R is not equal to zero. The R=0 case comes from cases where for instance two C atoms have a different 
  !set of basis functions. In either case we need to pick up the atom type of the center in order to 
  !access the basis set information from the basis set structure
  IF (R.EQ.0) THEN 
    iCharge = int(molecule%atom(icent)%charge)
    atom_type = AObasis%chargeindex(icharge)
  ELSE
    atom_type = molecule%atom(icent)%IDtype(R)
  ENDIF

  IF(AObasis%atomtype(atom_type)%nAngmom.EQ.0) CYCLE !uncertain about the purpose of this, copied from BUILD_AO

  !Actual counting
  !ToDo, make special case for uncontracted basis
  DO iAngmom=1,AObasis%atomtype(atom_type)%nAngmom !Loop over angmom
    DO isegment=1,AObasis%atomtype(atom_type)%shell(iAngmom)%nsegments !Loop over segments
      n = AObasis%atomtype(atom_type)%shell(iAngmom)%segment(isegment)%nrow !# of primitives
      c = AObasis%atomtype(atom_type)%shell(iAngmom)%segment(isegment)%ncol !# of contracted
      ex => AObasis%atomtype(atom_type)%shell(iAngmom)%segment(isegment)%exponents !primitive exponents
      CC => AObasis%atomtype(atom_type)%shell(iAngmom)%segment(isegment)%elms !normalized contraction coefficients
      ioff = 0
      DO icont=1,c !Loop over contracted functions
        ishell = ishell+1
        shell_info%icent(ishell)               = icent
        shell_info%ang_mom(ishell)             = iAngmom-1
        shell_info%nprim(ishell)               = n
        shell_info%start_prim(ishell)          = iprim+1
        shell_info%exponents(iprim+1:iprim+n)  = ex(1:n)
        shell_info%cont_coeff(iprim+1:iprim+n) = CC(ioff+1:ioff+n)
        ioff  = ioff + n
        iprim = iprim + n
      ENDDO
    ENDDO
  ENDDO

  !Center position
  shell_info%center(:,icent) = molecule%atom(icent)%center(:)
ENDDO

END SUBROUTINE init_shell_info

!> \brief Frees the AO shell information object
!> \param shell_info The object contaning the shell information
!> \author Simen Reine
!> \date 2016-10-14
SUBROUTINE free_shell_info(shell_info)
implicit none
TYPE(shell_info_type),intent(INOUT) :: shell_info
!
shell_info%ncents    = 0
shell_info%nshells   = 0
shell_info%nprim_tot = 0
call mem_dealloc(shell_info%icent)
call mem_dealloc(shell_info%ang_mom)
call mem_dealloc(shell_info%nprim)
call mem_dealloc(shell_info%start_prim)
call mem_dealloc(shell_info%center)
call mem_dealloc(shell_info%exponents)
call mem_dealloc(shell_info%cont_coeff)

END SUBROUTINE free_shell_info

!> \brief Prints the AO shell information object
!> \param shell_info The object contaning the shell information
!> \param iunit The print unit to dump the output to
!> \param iprint The print level (the higher the more is printed)
!> \param text Text identifier to specify what is printed/from where
!> \author Simen Reine
!> \date 2016-10-14
SUBROUTINE print_shell_info(shell_info,iunit,iprint,text)
implicit none
TYPE(shell_info_type),intent(INOUT) :: shell_info
integer,intent(IN)                  :: iunit,iprint
character*(*),intent(IN)            :: text
!
integer :: i,j,k
!
IF (iprint.LT.2) RETURN
!
!    IPRINT 2 and higher
!
write(iunit,'(1X,A26,A)')  '>>> Output of shell info, ',text
write(iunit,'(5X,A22,I8)') 'Number of centers:    ',shell_info%ncents
write(iunit,'(5X,A22,I8)') 'Number of shells:     ',shell_info%nshells
write(iunit,'(5X,A22,I8)') 'Number of primitives: ',shell_info%nprim_tot

IF (iprint.LT.5) RETURN
!
!    IPRINT 5 and higher
!

write(iunit,'(1X)')     !New line
write(iunit,'(3X,A21)')  '> Shell information:'
write(iunit,'(1X)')     !New line
write(iunit,'(5X,5A11)') 'Shell','Center','Ang.Mom','# Prim','Start prim'
DO i=1,shell_info%nshells
  write(iunit,'(5X,5I11)') i,shell_info%icent(i),shell_info%ang_mom(i),shell_info%nprim(i),shell_info%start_prim(i)
ENDDO

write(iunit,'(1X)')     !New line
write(iunit,'(3X,A22)')  '> Center information:'
write(iunit,'(1X)')     !New line
write(iunit,'(5X,4A11)') 'Center','X','Y','Z'
DO i=1,shell_info%ncents
  write(iunit,'(5X,I11,3F11.6)') i,(shell_info%center(j,i),j=1,3)
ENDDO

IF (iprint.LT.10) RETURN
!
!    IPRINT 10 and higher
!
write(iunit,'(1X)')     !New line
write(iunit,'(3X,A41)')  '> Exponents and contraction coefficients:'
write(iunit,'(1X)')     !New line
write(iunit,'(5X,5A11)') 'Shell','Primitive','iPrim','Exponent','Cont.coeff'
DO i=1,shell_info%nshells
  k=shell_info%start_prim(i)
  DO j=1,shell_info%nprim(i)
    write(iunit,'(5X,3I11,2F11.6)') i,k,j,shell_info%exponents(k),shell_info%cont_coeff(k)
    k=k+1
  ENDDO
ENDDO

END SUBROUTINE print_shell_info


END MODULE ls_shells
