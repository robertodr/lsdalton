module wannier_settings

use precision

use wannier_types, only: wannier_config, bravais_lattice

private

public :: wannier_read_dalton_input, wanniertypes_set_default

contains

!> @brief Setup default
!> @param wannier Contains data relevant for wannier scf calculations.
!> @author KRL
!> @date 2015
subroutine wanniertypes_set_default(wannier)
  implicit none
  type(wannier_config), intent(inout) :: wannier
  wannier%do_wannier_scf = .false.
  wannier%setup_bravais_lattice = .false.
  wannier%fmm_lmax = 15  ! TODO
  wannier%nf_cutoff = 5  ! TODO 6
  wannier%screening_thr = 1.0e-14_realk
  wannier%ao_cutoff = 2
  wannier%orthpot_strength = 1e4_realk
  wannier%debug_level = 0
  wannier%n_bvk = [-1, -1, -1]  ! 
  wannier%scfthresh = 1.0e-6_realk
  wannier%maxiter = 50
  wannier%dump_dens = 0
  wannier%no_redundant = .true.
  wannier%orthonormalise = .true.
  wannier%unitary_scf = .false.
  wannier%conv_start_guess = .false. !Start from the converged molecular MO coeff
  wannier%scf_kspace = .false. 
  wannier%nbvk_scf_kspace = -1
  wannier%scf_shuklaAO = .false.
  wannier%scf_shuklaMO = .false.
  wannier%scf_original = .false.
  wannier%nocc = -1
  wannier%localization = .false.
  wannier%dens_start = .false.
  wannier%asymptotic_expansion = .false.
  wannier%ff_cutoff = [-1, -1, -1 ]
  wannier%compute_bandstructure = .false.
  wannier%crystal_init =.false.
end subroutine wanniertypes_set_default

!> @brief Read dalton input file. All keywords under **WANNIER
!> @param wannier_config Parameters relevant to wannier scf.
!> @param keyword Last word read from file
!> @param lupri Logical unit dalton input.
!> @param lupri Logical print unit.
!> @author KRL
!> @date 2015
subroutine wannier_read_dalton_input(wannier, keyword, lucmd, lupri)
  implicit none

  type(wannier_config), intent(inout) :: wannier
  integer, intent(in) :: lucmd, lupri
  character(len = 70), intent(inout) :: keyword 

  character(len = 1) :: prompt
  character(len = 7), dimension(1:25) :: kwordtable
  integer :: filestatus, tmp

  kwordtable = [ &
       & '.MAX_MU', &
       & '.FMM_TH', &
       & '.N_BVK_', &
       & '.AO_CUT', &
       & '.SCREEN', &
       & '.ORTHPO', &
       & '.NF_CUT', &
       & '.DEBUG_', &
       & '.SCF_CO', &
       & '.SCF_MA', &
       & '.DUMP_D', &
       & '.NO_RED', & 
       & '.NO_ORT', & 
       & '.UNITAR', &
       & '.CONV_S', &
       & '.DENS_S', &
       & '.SCF_KS', &
       & '.AO_SHU', &
       & '.MO_SHU', &
       & '.LOCALI', &
       & '.SCF_OR', &
       & '.CRYSTA', &
       & '.FF_CUT', &
       & '.ASYMPT', &
       & '.BANDST'&
       & ]

  do 
     read(lucmd,'(A70)', iostat = filestatus) keyword
     if (filestatus > 0) call lsquit('Error reading lucmd', lupri)
     if ((filestatus < 0)) exit

     prompt = keyword(1:1)
     if ( (prompt == '#') .or. (prompt == '!') ) cycle
     if (prompt == '*') exit

     if (prompt == '.') then
        if (any(kwordtable == keyword(1:7))) then
           select case (keyword)
              !! .AO_CUTOFF
           case('.N_BVK_')
           !   read(lucmd, *) wannier%n_bvk
              write (*,*) &
                   'Warning: .N_BVK_ is deprecated'
              write (*,*) &
                   'the number of k-points is set to the number of layers in each direction'
           case('.ORTHPOT_STRENGTH')
              read(lucmd, *) wannier%orthpot_strength
           case('.AO_CUTOFF')
              read(lucmd, *) wannier%ao_cutoff
              wannier%n_bvk = 2*wannier%ao_cutoff + 1
           case('.NF_CUTOFF')
              read(lucmd, *) wannier%nf_cutoff
              ! .FMMTHR
              !case('.FMMTHR')
              !	read(lucmd,*) wannier%fmm_thresh
              ! .MAX_MULTIPOLE_MOMENT
           case('.SCREENING_THRESHOLD')
              read(lucmd,*) wannier%screening_thr
           case('.MAX_MULTIPOLE_MOMENT')
              read(lucmd,*) wannier%fmm_lmax
           case('.DEBUG_MODE')
              read(lucmd,*) wannier%debug_level
              write (lupri, *) 'Debug printing level',wannier%debug_level
           case('.SCF_CONVERGENCE_THRESHHOLD')
              read(lucmd,*) wannier%scfthresh
           case('.SCF_MAX_ITER')
              read(lucmd,*) wannier%maxiter
           case('.DUMP_DENS')
              read(lucmd,*) wannier%dump_dens
           case('.NO_REDUNDANT')
              wannier%no_redundant = .false.
           case('.NO_ORTHONORMALISATION')
              wannier%orthonormalise = .false.
           case('.UNITARY_SCF')
              wannier%unitary_scf = .true.
           case('.AO_SHUKLA')
              wannier%scf_shuklaAO = .true.
           case('.MO_SHUKLA')
              wannier%scf_shuklaMO = .true.
           case('.SCF_KSPACE')
              wannier%scf_kspace = .true.
              read(lucmd, *) tmp
           case('.SCF_ORIGINAL')
              wannier%scf_original = .true.
           case('.CONV_STARTGUESS')
              wannier%conv_start_guess = .true. 
           case('.DENS_STARTGUESS')
              wannier%dens_start = .true.
           case('.LOCALIZATION')
              wannier%localization = .true.
           case('.ASYMPTOTIC_EXPANSION')
              wannier%asymptotic_expansion = .true.
           case('.FF_CUTOFF')
              read(lucmd, *) wannier%ff_cutoff
           case('.BANDSTRUCTURE')
              wannier%compute_bandstructure = .true.
           case('.CRYSTAL_INIT')
              wannier%crystal_init = .true.
           case default
              write(lupri,'(/,3A,/)') ' Keyword "', keyword, &
                   & '" is not implemented for **WANNIER.'
           end select
        else
           write(lupri,'(/,3A,/)') ' Keyword "',keyword, &
            	& '" not recognized.'
           call lsquit('Illegal keyword for **WANNIER.', lupri)
        endif
     endif
  enddo
  if (wannier%scf_kspace) then 
	  wannier%nbvk_scf_kspace = tmp*wannier%ao_cutoff
  else 
	  wannier%nbvk_scf_kspace = wannier%ao_cutoff  ! TODO quick fix for the implementation
  endif
  ! of the scf_compute_bandstructure.
  ! The fock matrix must be calculated for an accordingly large area (set in wannier_driver
  ! for cercain routines to function, ex. scf_compute_bandstructure.

!  if (any(wannier%ao_cutoff>wannier%nf_cutoff)) then
!	  call lsquit(-1, 'The wannier NF_CUTOFF can not be smaller than the AO_CUTOFF.')
!  endif

  !Default
  if (.not.((wannier%unitary_scf).or.(wannier%scf_kspace).or.(wannier%scf_shuklaAO).or. &
       (wannier%scf_shuklaMO).or.(wannier%scf_original))) then
     wannier%scf_shuklaAO = .true.
  endif

  ! 0 along nonperiodic directions
  wannier%nf_cutoff(wannier%blat%dims+1:3) = 0
  
  if ((wannier%asymptotic_expansion) .and.(wannier%ff_cutoff(1) == -1).and.(wannier%ff_cutoff(2) == -1).and. &
       (wannier%ff_cutoff(3) == -1)) then
      wannier%ff_cutoff(:) = 100*wannier%nf_cutoff(:)
      write(lupri,*) 'As the far-field cutoffs were not specified in input, they are set to',&
           wannier%ff_cutoff(:)
   endif
  
   wannier%ff_cutoff(wannier%blat%dims+1:3) = 0

  call report_after_wannier_read_dalton_input(wannier, lupri)

end subroutine wannier_read_dalton_input

!> \brief Print relevant settings to lupri. 
!> \param lupri Logical print unit.
!> \param wannier Parameters relevant for Wannier SCF calculations 
!> \date 2015
!> \author KRL
subroutine report_after_wannier_read_dalton_input(wannier, lupri)

!	use ls_util, only: lsheader

   type(wannier_config), intent(in) :: wannier
   integer, intent(in) :: lupri

!   call lsheader(lupri, 'Processing input for A Priori Wannier Functions')
	!write(lupri, *) &
   !   & 'ao_basis, number of layers:', wannier%ao_basis%layers
	!write(lupri, *) &
   !   & 'FMM threshold:', wannier%fmm_thresh
	write(lupri, *) &
      & ''

	!call citations

end subroutine

end module
