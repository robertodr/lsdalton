module ls_frame

    ! TODO:
    !   convert from FraME real/integer to LSDalton realk/intk

    use precision, only: realk, intk
    use typedeftype, only: lssetting

    implicit none

    private

    logical, public :: use_frame = .false.

    ! public procedures
    public :: ls_frame_initialize
    public :: ls_frame_terminate
    public :: ls_frame_define_core
    public :: ls_frame_scf_driver
    public :: ls_frame_response_driver
    public :: ls_frame_molecular_gradient

    integer :: ls_lupri
    integer :: ls_luerr
    integer :: ls_nbast
    type(lssetting), pointer :: ls_setting => null()

contains


subroutine ls_frame_initialize(lupri, luerr, nbast, setting)

    use frame_api, only: frame_initialize

    integer, intent(in) :: lupri
    integer, intent(in) :: luerr
    integer, intent(in) :: nbast
    type(lssetting), target, intent(in) :: setting

    ls_lupri = lupri
    ls_luerr = luerr
    ls_nbast = nbast
    ls_setting => setting

    call frame_initialize('frame.json', ls_electronic_potentials, &
                          ls_multipole_potential_integrals)

end subroutine ls_frame_initialize


subroutine ls_frame_terminate()

    use frame_api, only: frame_terminate

    call frame_terminate()

end subroutine ls_frame_terminate


subroutine ls_frame_define_core(molecule)

    use frame_api, only: frame_define_core_fragment
    use molecule_typetype

    type(moleculeinfo), intent(in) :: molecule

    integer :: i
    real(realk), dimension(:,:), allocatable :: coordinates
    real(realk), dimension(:), allocatable :: charges
    real(realk), dimension(:), allocatable :: masses
    character(len=2), dimension(:), allocatable :: elements
    character(len=2), dimension(109) :: number2elements

    number2elements = ['H ', 'He', 'Li', 'Be', 'B ', 'C ', 'N ', 'O ', 'F ', &
                       'Ne', 'Na', 'Mg', 'Al', 'Si', 'P ', 'S ', 'Cl', 'Ar', &
                       'K ', 'Ca', 'Sc', 'Ti', 'V ', 'Cr', 'Mn', 'Fe', 'Co', &
                       'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr', &
                       'Rb', 'Sr', 'Y ', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', &
                       'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I ', 'Xe', &
                       'Cs', 'Ba', 'La', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', &
                       'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Hf', &
                       'Ta', 'W ', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl', &
                       'Pb', 'Bi', 'Po', 'At', 'Rn', 'Fr', 'Ra', 'Ac', 'Th', &
                       'Pa', 'U ', 'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es', &
                       'Fm', 'Md', 'No', 'Lr', 'Rf', 'Db', 'Sg', 'Bh', 'Hs', &
                       'Mt']

    allocate(coordinates(3,molecule%natoms))
    allocate(charges(molecule%natoms))
    allocate(masses(molecule%natoms))
    allocate(elements(molecule%natoms))

    do i = 1, molecule%natoms
        coordinates(:,i) = molecule%atom(i)%center(:)
        charges(i) = molecule%atom(i)%charge
        masses(i) = molecule%atom(i)%mass
        elements(i) = number2elements(molecule%atom(i)%atomic_number)
    end do

    call frame_define_core_fragment(molecule%natoms, int(molecule%charge), &
                                    coordinates, charges, masses, elements)

    deallocate(coordinates)
    deallocate(charges)
    deallocate(masses)
    deallocate(elements)

end subroutine ls_frame_define_core


subroutine ls_frame_scf_driver(ls_density_matrix, ls_fock_matrix, energy)

    use qcmatrix_f, only: QcMat, QInt, QREALMAT, QcMatCreate_f, &
                          QcMatBlockCreate_f, QcMatSetExternalMat_f, &
                          QcMatDestroy_f
    use frame_api, only: frame_scf_driver
    use matrix_module, only: matrix, matrixp
    use matrix_operations, only: mat_zero
    use matrixp_operations, only: matrixp_create, matrixp_nullify, &
                                  matrixp_setmat

    type(matrix), intent(in) :: ls_density_matrix
    type(matrix), intent(out) :: ls_fock_matrix
    real(realk), intent(out) :: energy

    integer :: qcmat_error
    type(matrixp), pointer :: density_matrix_ptr
    type(matrixp), pointer :: fock_matrix_ptr
    type(QcMat) :: density_matrix
    type(QcMat) :: fock_matrix

    call mat_zero(ls_fock_matrix)

    allocate(density_matrix_ptr)
    call matrixp_setmat(density_matrix_ptr, ls_density_matrix)

    qcmat_error = QcMatCreate_f(A=density_matrix)
    qcmat_error = QcMatBlockCreate_f(A=density_matrix, dim_block=1_QInt)
    qcmat_error = QcMatSetExternalMat_f(A=density_matrix, &
                                        idx_block_row=1_QInt, &
                                        idx_block_col=1_QInt, &
                                        data_type=QREALMAT, &
                                        A_ext=density_matrix_ptr)

    allocate(fock_matrix_ptr)
    call matrixp_setmat(fock_matrix_ptr, ls_fock_matrix)

    qcmat_error = QcMatCreate_f(A=fock_matrix)
    qcmat_error = QcMatBlockCreate_f(A=fock_matrix, dim_block=1_QInt)
    qcmat_error = QcMatSetExternalMat_f(A=fock_matrix, &
                                        idx_block_row=1_QInt, &
                                        idx_block_col=1_QInt, &
                                        data_type=QREALMAT, &
                                        A_ext=fock_matrix_ptr)

    call frame_scf_driver(density_matrix, fock_matrix, energy, 2.0_realk)

    qcmat_error = QcMatDestroy_f(density_matrix)
    call matrixp_nullify(density_matrix_ptr)
    deallocate(density_matrix_ptr)

    qcmat_error = QcMatDestroy_f(fock_matrix)
    call matrixp_nullify(fock_matrix_ptr)
    deallocate(fock_matrix_ptr)

end subroutine ls_frame_scf_driver


subroutine ls_frame_molecular_gradient()

    use qcmatrix_f
    use frame_api, only: frame_molecular_gradient

    call frame_molecular_gradient()

end subroutine ls_frame_molecular_gradient


subroutine ls_frame_response_driver(ls_density_matrix, ls_fock_matrix)

    use qcmatrix_f, only: QcMat, QInt, QREALMAT, QcMatCreate_f, &
                          QcMatBlockCreate_f, QcMatSetExternalMat_f, &
                          QcMatDestroy_f
    use frame_api, only: frame_response_driver
    use matrix_module, only: matrix, matrixp
    use matrix_operations, only: mat_zero
    use matrixp_operations, only: matrixp_create, matrixp_nullify, &
                                  matrixp_setmat

    type(matrix), intent(in) :: ls_density_matrix
    type(matrix), intent(out) :: ls_fock_matrix

    integer :: qcmat_error
    type(matrixp), pointer :: density_matrix_ptr
    type(matrixp), pointer :: fock_matrix_ptr
    type(QcMat) :: density_matrix
    type(QcMat) :: fock_matrix

    call mat_zero(ls_fock_matrix)

    allocate(density_matrix_ptr)
    call matrixp_setmat(density_matrix_ptr, ls_density_matrix)

    qcmat_error = QcMatCreate_f(A=density_matrix)
    qcmat_error = QcMatBlockCreate_f(A=density_matrix, dim_block=1_QInt)
    qcmat_error = QcMatSetExternalMat_f(A=density_matrix, &
                                        idx_block_row=1_QInt, &
                                        idx_block_col=1_QInt, &
                                        data_type=QREALMAT, &
                                        A_ext=density_matrix_ptr)

    allocate(fock_matrix_ptr)
    call matrixp_setmat(fock_matrix_ptr, ls_fock_matrix)

    qcmat_error = QcMatCreate_f(A=fock_matrix)
    qcmat_error = QcMatBlockCreate_f(A=fock_matrix, dim_block=1_QInt)
    qcmat_error = QcMatSetExternalMat_f(A=fock_matrix, &
                                        idx_block_row=1_QInt, &
                                        idx_block_col=1_QInt, &
                                        data_type=QREALMAT, &
                                        A_ext=fock_matrix_ptr)

    call frame_response_driver(density_matrix, fock_matrix, 2.0_realk)

    qcmat_error = QcMatDestroy_f(density_matrix)
    call matrixp_nullify(density_matrix_ptr)
    deallocate(density_matrix_ptr)

    qcmat_error = QcMatDestroy_f(fock_matrix)
    call matrixp_nullify(fock_matrix_ptr)
    deallocate(fock_matrix_ptr)

end subroutine ls_frame_response_driver


subroutine ls_electronic_potentials(potentials, density, coordinates, &
                                    num_sites, pot_deriv_order, &
                                    num_nuclei, geom_deriv_order)

    use QcMatrix_f, only: QcMat, QInt, QSUCCESS, QREALMAT, &
                          QcMatGetExternalMat_f
    use matrix_module, only: Matrix, Matrixp
    use matrix_operations, only: mat_init, mat_free, mat_assign
    use matrixp_operations, only: Matrixp_GetMat
    use integralinterfacemod, only: II_get_ep_potential
    use ls_util, only: binomial_coefficient

    real(realk), dimension(:,:,:), intent(out) :: potentials
    type(QcMat), intent(in) :: density
    real(realk), dimension(:,:), intent(in) :: coordinates
    integer, intent(in) :: num_sites
    integer, intent(in) :: pot_deriv_order
    integer, intent(in) :: num_nuclei
    integer, intent(in) :: geom_deriv_order

    integer :: i, j, k
    integer :: qcmat_error
    integer :: num_pot_comps
    integer :: num_geo_comps
    real(realk), dimension(:), allocatable :: ls_potentials
    type(Matrix) :: ls_density
    type(Matrix), pointer :: tmp_density
    type(Matrixp), pointer :: ptr_density

    potentials = 0.0_realk

    num_pot_comps = (pot_deriv_order + 1) * (pot_deriv_order + 2) / 2

    num_geo_comps = binomial_coefficient(3 * num_nuclei + &
                                         geom_deriv_order - 1, &
                                         geom_deriv_order)

    if (size(potentials, 1) /= num_sites) then
        call lsquit('ERROR: potentials array has wrong size', -1)
    else if (size(potentials, 2) /= num_pot_comps) then
        call lsquit('ERROR: potentials array has wrong size', -1)
    else if (size(potentials, 3) /= num_geo_comps) then
        call lsquit('ERROR: potentials array has wrong size', -1)
    end if

    allocate(ls_potentials(num_sites*num_pot_comps*num_geo_comps))

    ls_potentials = 0.0_realk

    qcmat_error = QcMatGetExternalMat_f(A=density, idx_block_row=1_QInt, &
                                        idx_block_col=1_QInt, &
                                        data_type=QREALMAT, &
                                        A_ext=ptr_density)

    if (qcmat_error /= QSUCCESS) then
        call lsquit('ERROR: QcMatGetExternalMat failed', -1)
    end if

    call Matrixp_GetMat(ptr_density, tmp_density)

    call mat_init(ls_density, ls_nbast, ls_nbast)
    call mat_assign(ls_density, tmp_density)

    ! potentials_C = sum_ab (C|ab) D_ab, with C the coordinate centers and
    ! D_ab the ls_density matrix components
    call II_get_ep_potential(ls_lupri, ls_luerr, ls_setting, coordinates, &
                             num_sites, ls_density, ls_potentials, &
                             pot_deriv_order, pot_deriv_order, &
                             num_pot_comps, geom_deriv_order, num_geo_comps)

    potentials = reshape(ls_potentials, [num_sites, num_pot_comps, num_geo_comps])

    call mat_free(ls_density)

    deallocate(ls_potentials)

    nullify(tmp_density)

end subroutine ls_electronic_potentials


subroutine ls_multipole_potential_integrals(integrals, coordinates, &
                                            multipoles, num_sites, &
                                            min_multipole_order, &
                                            max_multipole_order, num_nuclei, &
                                            geom_deriv_order)

    use QcMatrix_f, only: QcMat, QInt, QSUCCESS, QREALMAT, &
                          QcMatGetExternalMat_f
    use memory_handling, only: mem_alloc, mem_dealloc
    use matrix_module, only: Matrix, Matrixp
    use matrix_operations, only: mat_init, mat_free, mat_daxpy
    use matrixp_operations, only: Matrixp_GetMat, Matrixp_Nullify
    use integralinterfacemod, only: II_get_ep_mat
    use ls_util, only: binomial_coefficient

    type(QcMat), dimension(:), intent(inout) :: integrals
    real(realk), dimension(:,:), intent(in) :: coordinates
    real(realk), dimension(:,:), intent(in) :: multipoles
    integer, intent(in) :: num_sites
    integer, intent(in) :: min_multipole_order
    integer, intent(in) :: max_multipole_order
    integer, intent(in) :: num_nuclei
    integer, intent(in) :: geom_deriv_order

    integer :: i, j
    integer :: qcmat_error
    integer :: num_multipole_comps
    integer :: num_deriv_comps
    real(realk), dimension(:,:), allocatable :: ls_multipoles
    type(Matrix), dimension(:), pointer :: ls_integrals
    type(Matrix), pointer :: tmp_integrals
    type(Matrixp), pointer :: ptr_integrals

    num_multipole_comps = size(multipoles, 1)

    allocate(ls_multipoles(num_sites, num_multipole_comps))

    do i = 1, num_sites
        do j = 1, num_multipole_comps
            ls_multipoles(i,j) = multipoles(j,i)
        end do
    end do

    num_deriv_comps = binomial_coefficient(3 * num_nuclei + &
                                           geom_deriv_order - 1, &
                                           geom_deriv_order)

    if (size(integrals) /= num_deriv_comps) then
        call lsquit('ERROR: integrals array has wrong size', -1)
    end if

    call mem_alloc(ls_integrals, num_deriv_comps)

    do i = 1, num_deriv_comps
        call mat_init(ls_integrals(i), ls_nbast, ls_nbast)
    end do

    ! ls_integrals{ab} = sum_C (ab|C) multipoles{C} with C the coordinate
    ! centers
    call II_get_ep_mat(ls_lupri, ls_luerr, ls_setting, ls_integrals, &
                       num_sites, coordinates, ls_multipoles, &
                       min_multipole_order, max_multipole_order, &
                       num_multipole_comps, geom_deriv_order, num_deriv_comps)

    do i = 1, num_deriv_comps
        qcmat_error = QcMatGetExternalMat_f(A=integrals(i), &
                                            idx_block_row=1_QInt, &
                                            idx_block_col=1_QInt, &
                                            data_type=QREALMAT, &
                                            A_ext=ptr_integrals)
        if (qcmat_error /= QSUCCESS) then
            call lsquit('ERROR: QcMatGetExternalMat failed', -1)
        end if
        call Matrixp_GetMat(ptr_integrals, tmp_integrals)
        call mat_daxpy(1.0_REALK, ls_integrals(i), tmp_integrals)
    end do

    deallocate(ls_multipoles)

    do i = 1, num_deriv_comps
        call mat_free(ls_integrals(i))
    end do

    call mem_dealloc(ls_integrals)

    nullify(tmp_integrals)

    ! call Matrixp_Nullify(ptr_integrals)

end subroutine ls_multipole_potential_integrals

end module ls_frame
