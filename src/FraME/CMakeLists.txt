option(USE_INTERNAL_FRAME "Extract and build FraME archive supplied with LSDalton" OFF)

find_package(FraME QUIET)
if(FraME_FOUND)
    message(STATUS "FraME found")
    include_directories(${FraME_INCLUDE_DIRS})
else()
    if(USE_INTERNAL_FRAME)
        message(STATUS "Internal FraME archive will be built")
        set(FRAME_PATH "${PROJECT_SOURCE_DIR}/src/FraME/FraME.tar.gz")
        set(USE_INTERNAL_JSONFORTRAN ON)
    else()
        message(STATUS "FraME will be downloaded and built")
        set(GIT_HASH "ddde68e11530b0e574c5d7fe344ca5a92215a1e8")
        set(FRAME_PATH "https://gitlab.com/FraME-projects/FraME/-/archive/${GIT_HASH}/FraME-${GIT_HASH}.tar.gz")
        set(USE_INTERNAL_JSONFORTRAN OFF)
    endif()
    ExternalProject_Add(FraME
                        PREFIX ${PROJECT_BINARY_DIR}/external
                        URL ${FRAME_PATH}
                        DOWNLOAD_NAME FraME.tar.gz
                        CMAKE_ARGS
                            -DBUILD_QCMATRIX=OFF
                            -DBUILD_JSONFORTRAN=ON
                            -DUSE_INTERNAL_JSONFORTRAN=${USE_INTERNAL_JSONFORTRAN}
                            -DENABLE_64BIT_INTEGER=${ENABLE_64BIT_INTEGERS}
                            -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
                            -DCMAKE_Fortran_COMPILER=${CMAKE_Fortran_COMPILER}
                            -DCMAKE_INSTALL_PREFIX=${PROJECT_BINARY_DIR}
                            -DCMAKE_INSTALL_INCLUDEDIR=${CMAKE_Fortran_MODULE_DIRECTORY}
                            -DCMAKE_LIBRARY_OUTPUT_DIRECTORY=${CMAKE_LIBRARY_OUTPUT_DIRECTORY}
                            -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY=${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}
                            -DCMAKE_Fortran_MODULE_DIRECTORY=${CMAKE_Fortran_MODULE_DIRECTORY})
endif()
set(FraME_LIBS
    ${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR}/libFraME.a
    ${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR}/libjsonfortran.a)

add_library(lsframe
  OBJECT
    ${CMAKE_CURRENT_LIST_DIR}/ls_frame_input.f90
    ${CMAKE_CURRENT_LIST_DIR}/ls_frame_wrappers.f90
  )

target_compile_options(lsframe
  PRIVATE
    "${LSDALTON_Fortran_FLAGS};${LSDALTON_Fortran_FLAGS_${CMAKE_BUILD_TYPE}}"
  )

target_link_libraries(lsdalton
  PUBLIC
    lsframe
    ${FRAME_LIBS}
  )
