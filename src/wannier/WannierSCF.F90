module wannier_scf

  use precision
  use timings_module
  use wannier_types, only: &
       wannier_config, &
       bravais_lattice
  use molecule_typetype, only: &
       moleculeinfo
  use typedeftype, only: &
       daltoninput, &
       lssetting
  use lattice_storage
  use wlattice_domains, only: &
       ltdom_init, &
       ltdom_free, &
       ltdom_getdomain, &
       lattice_domains_type
  use wannier_utils, only: &
       w_set_kmesh
  use wannier_complex_utils
  use wannier_startguess_module, only: &
       wannier_kspace_lowdin, wannier_normalization
  use memory_handling, only: &
       mem_alloc, mem_dealloc
  use matrix_module, only: &
       matrix, matrixp
  use matrix_operations, only: &
       mat_init, &
       mat_zero, &
       mat_free, &
       mat_mul, &
       mat_diag_f, &
       mat_print, &
       mat_abs_max_elm, &
       mat_sqnorm2, &
       mat_section, &
       mat_daxpy, &
       mat_insert_section
  use wannier_fockMO, only: &
       w_calc_fock_mo, w_calc_mo_l!, w_calc_fock_shukla
  use wannier_test_module, only: &
       check_unitary_U, check_unitary_G,wtest_orthonormality_wspace,wtest_projocc
  use wannier_startguess_module, only: &
       wannier_orthogonalisation
  use wannier_orthpot
  use wannier_dens
  
  implicit none
  private

  public :: scf_mokspace, scf_kspace, scf_wspace, scf_compute_bandstructure

contains

  !> @brief Transform the Fock matrix to Bloch space and solve the eigenproblem
  !> @author Elisa Rebolini
  !> @date August 2016
  !> @param fockAO Fock matrix AO basis in lattice storage
  !> @param mos Input/output MO coefficients in lattice storage
  !> @param wconf  Contains the Wannier configuration
  !> @param lupri Default print-unit for output
  !> @param luerr Default print-unit for termination
  subroutine scf_mokspace(fockAO, mos, smat, wconf, lupri, luerr)

    type(lmatrixp), intent(in)          :: fockAO
    type(lmatrixp), intent(inout)       :: mos, smat
    type(wannier_config), intent(inout) :: wconf
    integer, intent(in)                 :: lupri, luerr

    type(lattice_domains_type)  :: dom, dom_mos
    type(bloch_arrays), pointer :: matU_k(:)
    type(lmatrixp)              :: fockMO, G_pq_0L, mos_tmp
    type(matrix), pointer       :: FMO_0L_ptr, G_pq_0L_ptr, mos_tmp_ptr, mos_ptr
    type(matrix)                :: tmp
    real(realk), pointer        :: kpoint_array(:, :)
    complex(complexk), pointer  :: fock_k(:,:)
    integer                     :: n_bvk(3), nkpoint, kpoint, i, l, indx_l, m, indx_m, norb, nbas
    real(realk), pointer        :: eps_k(:)
    type(matrixp), pointer      :: matpointer_tmp(:)
    complex(complexk)           :: norm
    real(realk)                 :: res,dasum,rms,maxelm
    integer, dimension(3) 		  :: bvk_cutoffs

    bvk_cutoffs(:) = 0 
    bvk_cutoffs(1:wconf%blat%dims) = wconf%nbvk_scf_kspace 

    norb = mos%ncol
    nbas = mos%nrow

    !Set up the domain d0
    call ltdom_init(dom, mos%periodicdims, maxval(bvk_cutoffs))
    call ltdom_getdomain(dom, bvk_cutoffs, incl_redundant=.true.)

    !Transform F_mu,nu^L to F_p,q^L with L in L_BVK (changed from ao cutoff)
    call lts_init(fockMO, wconf%blat%dims, bvk_cutoffs, norb, norb, &
         & .true., .false.)
    call lts_zero(fockMO)
    loopl1: do l=1, dom%nelms
       indx_l = dom%indcs(l)
       call lts_mat_init(fockMO,indx_l)
       FMO_0L_ptr => lts_get(fockMO,indx_l)
       call mat_zero(FMO_0L_ptr)
       call w_calc_mo_l(FMO_0L_ptr,indx_l,dom,fockAO,mos,wconf,lupri,luerr)
    enddo loopl1
    ! note: Have not tested whether the FM is correct after introducing new cuoffs

    !Set up the BvK zone
    !    n_bvk = wconf%n_bvk
    n_bvk = 2*bvk_cutoffs + 1
    nkpoint = 1
    do i = 1, wconf%blat%dims
       nkpoint = nkpoint * n_bvk(i)
    enddo
    call w_set_kmesh(kpoint_array, nkpoint, wconf%blat, n_bvk) 

    write (*, *) bvk_cutoffs, n_bvk, nkpoint, wconf%n_bvk

    !Allocate and initialise matrix U_pq(k)
    allocate(matU_k(nkpoint))
    do i = 1, nkpoint
       allocate(matU_k(i)%bloch_array(norb, norb))
       matU_k(i)%bloch_array = 0.0_complexk
    enddo

    !Initialise Fock MO matrix in kspace and tmp matrices for eigenproblem
    call mem_alloc(fock_k,norb,norb)
    call mem_alloc(eps_k,norb)

    !Loop over k points
    !TODO: only k>=0
    loopk: do kpoint = 1, nkpoint
       !Fourier transform F_pq(k) = sum F_pq^L e^ikL
       call bloch_transform(fockMO,fock_k,kpoint_array(:, kpoint),nkpoint,norb,wconf,lupri,luerr)

       !Diagnoalise F_pq^k
       call bloch_diagonalisation(fock_k,eps_k,matU_k(kpoint)%bloch_array,norb,lupri,luerr)
       !do i = 1,norb
       !   write(*,*) 'Epsilon',i,eps_k(i),matU_k(kpoint)%bloch_array(:,i)
       !enddo

       !Sort the eigenvalues and eigenvectors, renormalisation
       !call sort_mos(matU_k(kpoint)%bloch_array,eps_k,norb,lupri,luerr)
       do i = 1,norb
          write(*,*) 'Epsilon',i,eps_k(i),matU_k(kpoint)%bloch_array(:,i)
       enddo
       write(*,*) ''
    enddo loopk

    call band_disentangle(matU_k,nkpoint,norb,lupri,luerr)

    call check_unitary_U(matU_k,nkpoint,norb,lupri,luerr)

    !Deallocate tmp matrices
    call mem_dealloc(fock_k)
    call mem_dealloc(eps_k)

    !Free F_pq^L
    call lts_free(fockMO)

    !Initiate G_p,q^L with L in AO cutoff
    call lts_init(G_pq_0L, wconf%blat%dims, bvk_cutoffs, norb, norb, &
         & .true., .false.)
    call lts_zero(G_pq_0L)
    do l=1, dom%nelms
       indx_l = dom%indcs(l)
       call lts_mat_init(G_pq_0L, indx_l)
    enddo

    !	 call lsquit(-1, '..')


    !Compute the transformation matrix G_pq^M = 1/N_BvK sum_k U_pq(k) e^-ikM for all M in AO cutoff
    norm =  1._realk / real(nkpoint, realk)
    call wannier_transform(G_pq_0L, matU_k, kpoint_array, nkpoint, norb, wconf, norm)

    !Test that G is unitary
    call check_unitary_G(G_pq_0L,wconf,norb,lupri,luerr)

    call lts_print(G_pq_0L,6)

    !Update MOs
    call lts_init(mos_tmp, wconf%blat%dims, mos%cutoffs, norb, norb, &
         & .true., .false.)
    call lts_zero(mos_tmp)
    !Set up the domain (aocutoff)
    call ltdom_init(dom_mos, mos%periodicdims, maxval(mos%cutoffs))
    call ltdom_getdomain(dom_mos, mos%cutoffs, incl_redundant=.true.)

    do l=1, dom_mos%nelms
       indx_l = dom_mos%indcs(l)
       call lts_mat_init(mos_tmp, indx_l)
    enddo

    !new_C_mu,p^L = sum M sum q C_mu,q^L+M G_qp^-M 
    do l=1, dom_mos%nelms
       indx_l = dom_mos%indcs(l)
       mos_tmp_ptr => lts_get(mos_tmp, indx_l)
       call mat_zero(mos_tmp_ptr)
       do m=1, dom%nelms
          indx_m = dom%indcs(m)

          mos_ptr => lts_get(mos, indx_l + indx_m)
          if (.not.associated(mos_ptr)) cycle
          G_pq_0L_ptr => lts_get(G_pq_0L, -indx_m)
          !TODO check transpose
          call mat_mul(mos_ptr, G_pq_0L_ptr, 'N', 'N', 1.0_realk, 1.0_realk, mos_tmp_ptr)
       enddo
    enddo

    call lts_swap_pointers(mos, mos_tmp)

    !Cleanup
    do i = 1, nkpoint
       deallocate(matU_k(i)%bloch_array)
    enddo
    deallocate(matU_k)
    call lts_free(mos_tmp)
    call lts_free(G_pq_0L)
    call mem_dealloc(kpoint_array)

    call ltdom_free(dom)
    call ltdom_free(dom_mos)

  end subroutine scf_mokspace

  !> @brief Diogonalize the MO Fock matrix at one given k point F(k) U(k) = eps(k) U(k)
  !> @author Elisa Rebolini
  !> @date August 2016
  subroutine bloch_diagonalisation(fock_k,eps_k,matU_k,norb,lupri,luerr)
    integer, intent(in)               :: norb, lupri, luerr
    complex(complexk), intent(inout)  :: fock_k(norb,norb),matU_k(norb,norb)
    real(realk),intent(inout)         :: eps_k(norb)

    complex(complexk), pointer  :: vl(:,:),work(:),matepsilon(:)
    real(realk), pointer        :: rwork(:)
    integer                     :: info, i

    complex(complexk), parameter :: c1 = cmplx(1.0), c0 = cmplx(0.0)

    call mem_alloc(matepsilon,norb)
    call mem_alloc(vl,1,norb)
    call mem_alloc(work,2*norb)
    call mem_alloc(rwork,2*norb)

    !Diagnoalise F_pq^k       
    call zgeev( 'N', 'V', norb, fock_k, norb, matepsilon, vl, 1, &
         matU_k, norb, &
         work, 2*norb, rwork, info)
    if (info /= 0) call lsquit('zgeev failed in wannierSCF',-1)
    do i =1,norb
       eps_k(i) = real(matepsilon(i), realk)
    enddo

    call mem_dealloc(vl)
    call mem_dealloc(work)
    call mem_dealloc(rwork)
    call mem_dealloc(matepsilon)

  end subroutine bloch_diagonalisation

  !> @brief
  !> @author
  !> @date
  !> @param
  subroutine band_disentangle(matU_k,nkpoint,norb,lupri,luerr)
    integer, intent(in)               :: nkpoint,norb,lupri,luerr
    type(bloch_arrays), intent(inout) :: matU_k(nkpoint)

    complex(complexk),pointer :: tmpU(:,:),matM(:,:),matM2(:,:)
    integer                   :: i,j,posmax,izamax,k0

    complex(complexk), parameter :: c1 = cmplx(1.0), c0 = cmplx(0.0)

    call mem_alloc(tmpU,norb,norb)
    call mem_alloc(matM,norb,norb)
    call mem_alloc(matM2,norb,norb)
    !do i =1, nkpoint-1
    do i = 0, (nkpoint-1)/2-1
       k0 =  (nkpoint-1)/2 +1
       tmpU = c0
       call zgemm('C','N',norb,norb,norb,c1,matU_k(k0+i)%bloch_array,norb,&
            matU_k(k0+i+1)%bloch_array,norb,c0,matM,norb)
       call zgemm('C','N',norb,norb,norb,c1,matU_k(k0-i)%bloch_array,norb,&
            matU_k(k0-i-1)%bloch_array,norb,c0,matM2,norb)
       write(*,*) ''
       write(*,*) 'matU kpoint',k0+i
       call ba_print(matU_k(k0+i)%bloch_array,norb,6)
       write(*,*) ''
       write(*,*) 'matU kpoint',k0+i+1
       call ba_print(matU_k(k0+i+1)%bloch_array,norb,6)
       write(*,*) ''
       write(*,*) 'U(k)^C U(k+1)'
       call ba_print(matM,norb,6)
       write(*,*) ''
       write(*,*) 'matU kpoint',k0-i
       call ba_print(matU_k(k0-i)%bloch_array,norb,6)
       write(*,*) ''
       write(*,*) 'matU kpoint',k0-i-1
       call ba_print(matU_k(k0-i-1)%bloch_array,norb,6)
       write(*,*) ''
       write(*,*) 'U(-k)^T U(-k-1)'
       call ba_print(matM2,norb,6)
       do j = 1,norb
          !posmax = irealmax(norb,matM(j,:))
          posmax = izamax(norb,matM(j,:),1)
          write(*,*) 'Swap colums',j,posmax
          tmpU(:,j) = matU_k(k0+i+1)%bloch_array(:,posmax)
       enddo
       matU_k(k0+i+1)%bloch_array = tmpU
       do j = 1,norb
          !posmax = irealmax(norb,matM(j,:))
          posmax = izamax(norb,matM2(j,:),1)
          write(*,*) 'Swap colums',j,posmax
          tmpU(:,j) = matU_k(k0-i-1)%bloch_array(:,posmax)
       enddo
       matU_k(k0-i-1)%bloch_array = tmpU
       write(*,*) ''
    enddo
    call mem_dealloc(tmpU)
    call mem_dealloc(matM)
    call mem_dealloc(matM2)

  end subroutine band_disentangle

  integer function irealmax(n,zmat)
    integer,intent(in) :: n
    complex(complexk),intent(in) :: zmat(:)

    real(realk) :: max
    integer :: i

    irealmax = 0
    if (n.lt.1) return
    irealmax = 1
    if (n.eq.1) return

    max = abs(real(zmat(1),realk))
    do i = 2,n
       if (abs(real(zmat(i),realk)).gt.max) then
          irealmax = i
          max = abs(real(zmat(i),realk))

       endif
    enddo
    return
  end function irealmax

  subroutine scf_kspace(fockmat, smat, mos, wconf, nbast, lupri, luerr)

    type(lmatrixp), intent(in)          :: fockmat, smat
    type(lmatrixp), intent(inout)       :: mos
    type(wannier_config), intent(inout) :: wconf
    integer, intent(in)                 :: nbast, lupri, luerr

    real(realk), pointer        :: kpoint_array(:, :)
    type(bloch_arrays), pointer :: mos_k_array(:)
    integer                     :: n_bvk(3), nkpoint, i, j, k, info
    complex(complexk), pointer  :: fock_k(:,:), smat_k(:,:)
    complex(complexk), pointer  :: alpha(:), beta(:), vl(:,:), work(:)
    complex(complexk), pointer  :: tmp(:,:), tmp2(:,:)
    complex(complexk)           :: sum
    real(realk), pointer        :: rwork(:),matepsilon(:)

    complex(complexk), parameter :: c1 = cmplx(1.0), c0 = cmplx(0.0)

    !Set up ok the BvK zone
    n_bvk = wconf%n_bvk
    nkpoint = 1
    do i = 1, wconf%blat%dims
       nkpoint = nkpoint * n_bvk(i)
    enddo
    call w_set_kmesh(kpoint_array, nkpoint, wconf%blat, n_bvk)  

    allocate(mos_k_array(nkpoint))
    do i = 1, nkpoint
       allocate(mos_k_array(i)%bloch_array(nbast, nbast))
       mos_k_array(i)%bloch_array = 0.0_complexk
    enddo

    call mem_alloc(fock_k,nbast,nbast)
    call mem_alloc(smat_k,nbast,nbast)
    call mem_alloc(alpha,nbast)
    call mem_alloc(beta,nbast)
    call mem_alloc(vl,1,nbast)
    call mem_alloc(work,3*nbast)
    call mem_alloc(rwork, 8*nbast)
    call mem_alloc(matepsilon,nbast)

    fock_K = 0.0_complexk
    smat_k = 0.0_complexk

    !Loop over k points
    loopk: do i = 1, nkpoint
       !Fourier transform of F and S
       call bloch_transform(fockmat, fock_k, kpoint_array(:,i), nkpoint, nbast, wconf, lupri, luerr)
       call bloch_transform(smat, smat_k, kpoint_array(:,i), nkpoint, nbast, wconf, lupri, luerr)

       !General eigenvalue solver A * v(j) = lambda(j) * B * v(j)
       !where lambda(j) = alpha(j)/beta(j)
       call zggev('N', 'V', nbast, fock_k, nbast, smat_k, nbast, alpha, beta, vl, 1,&
            mos_k_array(i)%bloch_array, nbast, work, 3*nbast, rwork, info)
       !VR is COMPLEX*16 array, dimension (LDVR,N)
       !If JOBVR = 'V', the right generalized eigenvectors v(j) are
       !stored one after another in the columns of VR, in the same
       !order as their eigenvalues.
       !Each eigenvector is scaled so the largest component has
       !abs(real part) + abs(imag. part) = 1.
       !Not referenced if JOBVR = 'N'.
       !Warning: The coefficients are scaled and need to be renormalized, and they are not ordered
       if (info /= 0) call lsquit('zggev failed in wannierSCF',-1)

       !Eigenvalues
       do k=1,nbast
          matepsilon(k) = real(alpha(k)/beta(k),realk)
       enddo

       !Sort and renormalise the MO so that C(k)^T_p S(k) C(k)_q = delta pq
       call sort_mos(mos_k_array(i)%bloch_array,matepsilon,nbast,lupri,luerr)
       call renormalisation_kspace(mos_k_array(i)%bloch_array, smat_k, nbast, lupri, luerr)

!!$       write (luerr,*) ''
!!$       write (luerr,*) matepsilon
!!$       write (luerr,*) 'MO in k space after sorting'
!!$       do k=1,nbast
!!$          write (luerr,*) mos_k_array(i)%bloch_array(k,:)
!!$       enddo


       !Normalisation of the coefficients
       call wannier_kspace_lowdin(mos_k_array(i)%bloch_array, nbast, mos%ncol, smat, kpoint_array(:,i),&
            nkpoint, wconf, lupri, luerr)

    enddo loopk

    call wannier_transform_mos(mos,mos_k_array,kpoint_array,nkpoint,nbast,wconf)

    call wannier_normalization(mos, smat, nbast, wconf, lupri, luerr)

    !deallocate bloch_arrays
    do i = 1, nkpoint
       deallocate(mos_k_array(i)%bloch_array)
    enddo
    deallocate(mos_k_array)

    call mem_dealloc(fock_k)
    call mem_dealloc(smat_k)
    call mem_dealloc(alpha)
    call mem_dealloc(beta)
    call mem_dealloc(vl)
    call mem_dealloc(work)
    call mem_dealloc(rwork)
    call mem_dealloc(matepsilon)

  end subroutine scf_kspace

  subroutine renormalisation_kspace(mos_k, smat_k, nbast, lupri, luerr)

    integer, intent(in)              :: nbast, lupri, luerr
    complex(complexk), intent(inout) :: mos_k(nbast,nbast)
    complex(complexk), intent(in)    :: smat_k(nbast,nbast)

    integer                    :: p
    complex(complexk), pointer :: tmp2(:,:),tmp(:,:),cmo_p(:,:)
    complex(complexk)          :: norm

    complex(complexk), parameter :: c1 = cmplx(1.0), c0 = cmplx(0.0)

    call mem_alloc(tmp,nbast,nbast)
    call mem_alloc(tmp2,nbast,nbast)
    call zgemm('c', 'n', nbast, nbast, nbast, c1, mos_k, nbast, smat_k, nbast,&
         & c0, tmp, nbast)
    call zgemm('n', 'n', nbast, nbast, nbast, c1, tmp, nbast, mos_k, nbast,&
         & c0, tmp2, nbast)
    do p = 1,nbast
       write(luerr,*) tmp2(p,:)
    enddo
    call mem_dealloc(tmp2)
    call mem_dealloc(tmp)


    call mem_alloc(cmo_p,nbast,1)
    call mem_alloc(tmp,1,nbast)
    do p = 1, nbast
       cmo_p(:,1) = mos_k(:,p)
       call zgemm('c', 'n', 1, nbast, nbast, c1, cmo_p, nbast, smat_k, nbast,&
            & c0, tmp, 1)
       call zgemm('n','n',1,1,nbast,c1,tmp,1,cmo_p,nbast,&
            & c0,norm,1)
       write(luerr,*) p, norm
       mos_k(:,p) = mos_k(:,p) / sqrt(real(norm,realk))
    enddo
    call mem_dealloc(cmo_p)
    call mem_dealloc(tmp)

  end subroutine renormalisation_kspace

  subroutine sort_mos(mos_k,matepsilon,nbast,lupri,luerr)

    integer, intent(in)              :: nbast,lupri,luerr
    complex(complexk), intent(inout) :: mos_k(nbast,nbast)
    real(realk), intent(inout)       :: matepsilon(nbast)

    integer :: i, Location

    do i=1,nbast
       Location = FindMinimum(matepsilon, i, nbast)
       call dswap(matepsilon,i,location,nbast)
       call zswap(mos_k,i,location,nbast)
    enddo

  end subroutine sort_mos

  ! --------------------------------------------------------------------
  ! INTEGER FUNCTION  FindMinimum():
  !    This function returns the location of the minimum in the section
  ! between Start and End.
  ! --------------------------------------------------------------------

  integer function  FindMinimum(x, Start, end)
    implicit  none
    real(realk), dimension(1:), intent(IN) :: x
    integer, intent(IN)                    :: Start, end
    real(realk)                            :: Minimum
    integer                                :: Location
    integer                                :: i

    Minimum  = x(Start)		! assume the first is the min
    Location = Start			! record its position
    do i = Start+1, end		! start with next elements
       if (x(i) < Minimum) then	!   if x(i) less than the min?
          Minimum  = x(i)		!      Yes, a new minimum found
          Location = i                !      record its position
       end if
    end do
    FindMinimum = Location        	! return the position
  end function  FindMinimum

  ! --------------------------------------------------------------------
  ! SUBROUTINE  Swap():
  !    This subroutine swaps the values of its two formal arguments.
  ! --------------------------------------------------------------------

  subroutine  dswap(x, a, b, n)
    implicit  none
    integer, intent(in)        :: n
    integer, intent(IN)        :: a, b
    real(realk), intent(inout) :: x(n)

    real(realk)                :: tmp

    tmp = x(a)
    x(a) = x(b)
    x(b) = tmp

  end subroutine  Dswap

  ! --------------------------------------------------------------------
  ! SUBROUTINE  Swap():
  !    This subroutine swaps the values of its two formal arguments.
  ! --------------------------------------------------------------------

  subroutine  zswap(x, a, b, n)
    implicit  none
    integer, intent(in)              :: n
    integer, intent(IN)              :: a, b
    complex(complexk), intent(inout) :: x(n,n)

    complex(complexk),pointer        :: tmp(:)

    call mem_alloc(tmp,n)
    tmp = x(:,a)
    x(:,a) = x(:,b)
    x(:,b) = tmp
    call mem_dealloc(tmp)
  end subroutine  Zswap

  subroutine scf_wspace(fockmat, cmo, wconf, nocc, lupri, luerr)

    type(lmatrixp), intent(inout) :: fockmat, cmo
    type(wannier_config), intent(inout) :: wconf
    integer, intent(in) :: nocc, lupri, luerr

    type(matrix) :: FMO, SMO, U, C
    type(matrix), pointer :: ptr
    real(realk), pointer :: eival(:)
    integer nbas, norb, p, l, indx
    type(lattice_domains_type) :: dom
    logical lstat_C

    nbas=cmo%nrow
    norb=cmo%ncol

    ! allocate memory for eigenvectors
    call mat_init(U,norb,norb)

    ! allocate memory for eigenvalues
    call mem_alloc(eival,norb)

    ! compute Fock matrix in MO basis
    call mat_init(FMO,norb,norb)
    call w_calc_fock_mo(wconf%blat,fockmat,cmo,FMO)

    ! set up "overlap" matrix (unit matrix)
    call mat_init(SMO,norb,norb)
    call mat_zero(SMO)
    do p=1,norb
       SMO%elms(norb*(p-1)+p)=1.0e0_realk
    enddo

    ! diagonalize
    call mat_diag_f(FMO,SMO,eival,U)
    call mat_free(SMO) ! no longer needed
    call mat_free(FMO) ! no longer needed

    ! print eigenvalues
    write (lupri,*) 'Eigenvalues from MO diagonalization:'
    do p=1,norb
       write (lupri,'(A8,I2,X,A,X,F12.8)') 'Epsilon',p,'=',eival(p)
    enddo
    call mem_dealloc(eival) ! no longer needed

    ! update CMO array
    call ltdom_init(dom,cmo%periodicdims,maxval(cmo%cutoffs))
    call ltdom_getdomain(dom,cmo%cutoffs,incl_redundant=.true.)
    call mat_init(C,nbas,norb)
    do l=1,dom%nelms; indx=dom%indcs(l)
       call lts_copy(cmo,-indx,lstat_C,C)
       if (lstat_C) then
          ptr=>lts_get(cmo,-indx)
          if (.not.associated(ptr)) then
             call lsquit('scf_wspace: logical (internal) error!',-1)
          endif
          call mat_mul(C,U,'N','N',1.0e0_realk,0.0e0_realk,ptr)
       endif
    enddo
    call mat_free(C)
    call ltdom_free(dom)
    call mat_free(U)

  end subroutine scf_wspace

  !> @brief Compute the band structure for the input fock matrix. 
  !> TODO (best way?) The band structure is calculated separately for the occ-occ and the virt-virt block og the matrix.
  !> TODO move to bandstructure tools?
  !>
  !> @author Karl R. Leikanger
  !> @date November 2016
  !> @param fockAO Fock matrix AO basis in lattice storage
  !> @param mos Input/output MO coefficients in lattice storage
  !> @param wconf  Contains the Wannier configuration
  !> @param lupri Default print-unit for output
  !> @param luerr Default print-unit for termination
  subroutine scf_compute_bandstructure(fockAO, mos, smat, wconf, lupri, luerr)

    type(lmatrixp), intent(in)          :: fockAO
    type(lmatrixp), intent(inout)       :: mos, smat
    type(wannier_config), intent(inout) :: wconf
    integer, intent(in)                 :: lupri, luerr

    type(lattice_domains_type) :: dom
    type(lmatrixp) :: fockmo
    type(matrix), pointer :: FMO_0L_ptr
    type(matrix) :: tmp
    real(realk), pointer :: kpoint_array(:, :)
    integer :: n_bvk(3), nkpoint, kpoint, i, l, indx_l, m, indx_m, norb, nbas, nocc, nvirt
    type(matrixp), pointer :: matpointer_tmp(:)
    complex(complexk) :: norm
    real(realk) :: res,dasum,rms,maxelm
    integer, dimension(3) :: bvk_cutoffs
    real(realk), pointer :: band_occ(:, :), band_virt(:, :)

    bvk_cutoffs(:) = 0 
    bvk_cutoffs(1:wconf%blat%dims) = wconf%nbvk_scf_kspace 

    norb = mos%ncol
    nbas = mos%nrow
	 nocc = wconf%nocc
	 nvirt = norb - nocc

    !Set up the domain d0
    call ltdom_init(dom, mos%periodicdims, maxval(bvk_cutoffs))
    call ltdom_getdomain(dom, bvk_cutoffs, incl_redundant=.true.)

    !Transform F_mu,nu^L to F_p,q^L with L in L_BVK (changed from ao cutoff)
    call lts_init(fockMO, wconf%blat%dims, bvk_cutoffs, norb, norb, &
         & .true., .false.)
    call lts_zero(fockMO)
    loopl1: do l=1, dom%nelms
       indx_l = dom%indcs(l)
       call lts_mat_init(fockMO,indx_l)
       FMO_0L_ptr => lts_get(fockMO,indx_l)
       call mat_zero(FMO_0L_ptr)
       call w_calc_mo_l(FMO_0L_ptr,indx_l,dom,fockAO,mos,wconf,lupri,luerr)
    enddo loopl1

    !Set up the BvK zone
    !    n_bvk = wconf%n_bvk
    n_bvk = 2*bvk_cutoffs + 1
    nkpoint = 1
    do i = 1, wconf%blat%dims
       nkpoint = nkpoint * n_bvk(i)
    enddo
    call w_set_kmesh(kpoint_array, nkpoint, wconf%blat, n_bvk) 
    
    ! get bandstrutures
    band_occ => scf_diag_F_section(& 
         & fockMO, 1, nocc, bvk_cutoffs, kpoint_array, nkpoint, dom, wconf, lupri, luerr)
    band_virt => scf_diag_F_section(&
         & fockMO, nocc+1, norb, bvk_cutoffs, kpoint_array, nkpoint, dom, wconf, lupri, luerr)
    
    write (*, *) '============'
    write (*, *) 'Band structures:'
    write (*, *) '============'
    do i = 1, nkpoint
       write (*, *) 'K - point:', kpoint_array(:, i)
       write (*, *) 'Eigenvalues:'
       write (*, *) band_occ(:, i), band_virt(:, i)
    enddo
    write (*, *) '============'
    
    
    !Cleanup
    call lts_free(fockMO)
    call mem_dealloc(kpoint_array)
    call ltdom_free(dom)
    deallocate(band_occ)
    deallocate(band_virt)

  end subroutine scf_compute_bandstructure

  ! @brief Calculate the band structures from a section the Fock matrix 
  !> (oc-occ or virt-virt).
  !> @param fockmo The fock matrix in MO - basis
  !> @param nf The first orbital or the section
  !> @param ne The last orbital of the section
  !> @param bvk_cutoffs The Born von Karman cutoff corresponding to the k - grid 
  !> @param kpoint_array  All k points
  !> @param nkpoint The number of k - points
  !> @param dom the domain corresponding to the cutoff
  !> @param wconf wannierconfig type
  !> @param lupri logical print unit print to file
  !> @param luerr logical print unit error
  !> @author Karl R. Leikanger.
  !> @date October 2016
  function scf_diag_F_section( &
		  & fockmo, nf, ne, bvk_cutoffs, kpoint_array, nkpoint, dom, wconf, lupri, luerr) &
		  & result(eps_k)

	  type(lmatrixp), intent(in) :: fockmo
	  real(realk), pointer :: kpoint_array(:, :)
	  type(wannier_config), intent(inout) :: wconf
	  integer, intent(in) :: nf, ne, bvk_cutoffs(3), nkpoint, lupri, luerr
	  type(lattice_domains_type) :: dom

	  type(matrix), pointer :: FMO_0L_ptr, FMO_occ_0L_ptr
	  complex(complexk), pointer :: bloch_array(:, :)
	  complex(complexk), pointer :: fock_k(:, :)
	  real(realk), pointer :: eps_k(:, :)
	  type(lmatrixp) :: fockMO_sec
	  integer :: norb, l, kpoint, indx_l

	  norb = ne - nf + 1

	  allocate(bloch_array(norb, norb))
	  bloch_array = 0.0_complexk

	  !Initialise Fock MO matrix in kspace and tmp matrices for eigenproblem
	  call mem_alloc(fock_k, norb, norb)
	  call mem_alloc(eps_k, norb, nkpoint)

	  ! init the occupied matrix
	  call lts_init(fockMO_sec, wconf%blat%dims, bvk_cutoffs, norb, norb, &
		  & .true., .false.)
	  call lts_zero(fockMO_sec)
	  do l=1, dom%nelms
		  indx_l = dom%indcs(l)
		  call lts_mat_init(fockMO_sec,indx_l)
		  FMO_occ_0L_ptr => lts_get(fockMO_sec,indx_l)
		  call mat_zero(FMO_occ_0L_ptr)  ! TODO not necc
		  FMO_0L_ptr => lts_get(fockMO,indx_l)
		  call mat_section(FMO_0L_ptr, nf, ne, nf, ne, FMO_occ_0L_ptr)
		  !call mat_print(FMO_occ_0L_ptr, 1, norb, 1, norb, lupri)
	  enddo

	  !Loop over k points
	  do kpoint = 1, nkpoint
		  !Fourier transform F_pq(k) = sum F_pq^L e^ikL
		  call bloch_transform(fockMO_sec,fock_k,kpoint_array(:, kpoint),nkpoint,norb,wconf,lupri,luerr)
		  !Diagnoalise F_pq^k
		  call bloch_diagonalisation(fock_k,eps_k(:, kpoint),bloch_array,norb,lupri,luerr)
	  enddo

	  deallocate(bloch_array)
     call mem_dealloc(fock_k)
     !call mem_dealloc(eps_k)
  end function

end module wannier_scf
