option(ENABLE_OMP "Enable OpenMP parallelization" OFF)

if(ENABLE_OMP)
  find_package(OpenMP REQUIRED COMPONENTS Fortran C)
  set(ENABLE_THREADED_MKL TRUE)
else()
  set(ENABLE_THREADED_MKL FALSE)
endif()
