The files in this folder define a set of compiler flags that will be appended
to the ones determined by CMake.
Do **NOT EVER** think of touching `CMAKE_<LANG>_FLAGS_<CONFIG>`.

Yes, I'm talking to you. Do **NOT EVER THINK** of touching `CMAKE_<LANG>_FLAGS_<CONFIG>`.

### Why?

The `CMAKE_<LANG>_FLAGS_<CONFIG>** variables are the mechanism offered by CMake
to set compiler flags from **outside**, _e.g._ when you are configuring as a
user of the project.
Furthermore, these flags are **always** applied globally, whereas it might make
more sense to apply certain flags per-directory or even per-source file.

### So, how do you set compiler flags?

Parallel to `CMAKE_<LANG>_FLAGS_<CONFIG>` there are the project-specific flag variables: `LSDALTON_<LANG>_FLAGS_<CONFIG>`.
These are **fair game**, but with some rules:

1. **If in doubt about a flag, CHECK that it works**. Certain compilers accept
certain flags only with certain versions. Rather than bend over backwards to get
the version information, there's a CMake function that will check whether the
flag works or not: `set_compiler_flag`. Look at the file
`set_compiler_flags.cmake` for documentation on its usage.

Then in order of priority:
1. **Set flags as compile options on targets**. 
```
target_compile_options(SolverUtilities
  PRIVATE
    "${LSDALTON_Fortran_FLAGS};${LSDALTON_Fortran_FLAGS_${CMAKE_BUILD_TYPE}}"
  )
```
2. **Set flags as `COMPILE_FLAGS` property on source files**.
```
set_property(
  SOURCE
    ${_f_srcs}
  APPEND_STRING
  PROPERTY
    COMPILE_FLAGS
      "${_LSDALTON_Fortran_FLAGS} ${_LSDALTON_Fortran_FLAGS_${CMAKE_BUILD_TYPE}}"
  )

set_property(
  SOURCE
    ${_c_srcs}
  APPEND_STRING
  PROPERTY
    COMPILE_FLAGS
      "${_LSDALTON_C_FLAGS} ${_LSDALTON_C_FLAGS_${CMAKE_BUILD_TYPE}}"
  )
```
**NOTE** the lack of semicolons ";" **and** the underscore in the project-local variable names!!!

Yes, this is more verbose, but it leads to less WTFs and fine-grained control.

### Exceptions

Some flags just need to be set globally, these are usually flags for which CMake
itself defines a property and/or a variable.
Generation of position-independent code is indeed done globally with:
```
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
```
Rule of thumb: if there's a CMake variable, it's OK to set a flag globally for
the whole project.

### What's in this folder?

Each compiler vendor gets a folder, with a file for each language.
See each file to see the pattern.
```
├── README.md
├── set_compiler_flags.cmake
├── CFlags.cmake
├── CXXFlags.cmake
├── FortranFlags.cmake
├── Cray
│   └── Fortran.cmake
├── Fujitsu
│   └── Fortran.cmake
├── GNU
│   ├── C.cmake
│   ├── CXX.cmake
│   └── Fortran.cmake
├── Intel
│   ├── C.cmake
│   ├── CXX.cmake
│   └── Fortran.cmake
├── PGI
│   ├── C.cmake
│   ├── CXX.cmake 
│   └── Fortran.cmake
└── XL
    └── Fortran.cmake
```
