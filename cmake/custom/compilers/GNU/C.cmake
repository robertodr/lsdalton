list(APPEND LSDALTON_C_FLAGS
  "-DRESTRICT=restrict"
  "-DFUNDERSCORE=1"
  "-DHAVE_NO_LSEEK64"
  "-ffloat-store"
  )

if(CMAKE_HOST_SYSTEM_PROCESSOR MATCHES "i386")
  list(APPEND LSDALTON_C_FLAGS
    "-m32"
    )
elseif(CMAKE_HOST_SYSTEM_PROCESSOR MATCHES "x86_64")
  list(APPEND LSDALTON_C_FLAGS
    "-m64"
    )
endif()

if(ENABLE_STATIC_LINKING)
  list(APPEND LSDALTON_C_FLAGS
    "-static"
    )
endif()

list(APPEND LSDALTON_C_FLAGS_DEBUG
  "-Og"
  "-g3"
  "-Wno-unused"
  )

list(APPEND LSDALTON_C_FLAGS_RELEASE
  "-O3"
  "-ffast-math"
  "-funroll-loops"
  "-ftree-vectorize"
  "-Wno-unused"
  )
