list(APPEND LSDALTON_C_FLAGS
  "-Mpreprocess"
  )

list(APPEND LSDALTON_C_FLAGS_DEBUG
  "-g"
  "-O0"
  )

list(APPEND LSDALTON_C_FLAGS_RELEASE
  "-O3"
  "-fast"
  "-Munroll"
  "-Mvect=idiom"
  "-DRESTRICT=restrict"
  )
