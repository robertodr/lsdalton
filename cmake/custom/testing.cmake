# set cdash buildname
set(BUILDNAME
  "${CMAKE_SYSTEM_NAME}-${CMAKE_HOST_SYSTEM_PROCESSOR}-${CMAKE_Fortran_COMPILER_ID}-${BLAS_TYPE}-${CMAKE_BUILD_TYPE}"
  CACHE STRING
  "Name of build on the dashboard"
  )

# set ctest own timeout
if(ENABLE_LARGE_TEST)
  set(DART_TESTING_TIMEOUT
    "18000" # 5 hours
    CACHE STRING
    "Set timeout in seconds for every single test"
    )
else()
  set(DART_TESTING_TIMEOUT
    "1200"  # 20 minutes
    CACHE STRING
    "Set timeout in seconds for every single test"
    )
endif()

macro(add_lsdalton_pytest)
  set(oneValueArgs NAME FILE COST)
  set(multiValueArgs LABELS)
  cmake_parse_arguments(__test
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
    )

  install(
    FILES
      ${__test_FILE}
    DESTINATION
      ${PYMOD_INSTALL_FULLDIR}/..
    )

  file(
    CREATE_LINK
      ${CMAKE_CURRENT_SOURCE_DIR}/${__test_FILE}
      ${PROJECT_BINARY_DIR}/${PYMOD_INSTALL_FULLDIR}/../${__test_FILE}
    SYMBOLIC
    )
  set(__test_FILE ${PROJECT_BINARY_DIR}/${PYMOD_INSTALL_FULLDIR}/../${__test_FILE})

  add_test(
    NAME
      ${__test_NAME}
    COMMAND
      ${Python_EXECUTABLE} -m pytest -rws -vv ${__test_FILE}
    WORKING_DIRECTORY
      ${CMAKE_CURRENT_BINARY_DIR}
    )

  set_tests_properties(${__test_NAME}
    PROPERTIES
    LABELS "${__test_LABELS};pylsdalton"
    )

  if(__test_COST)
    set_tests_properties(${__test_NAME}
      PROPERTIES
      COST ${__test_COST}
      )
  endif()
endmacro()

# This macro uses runtest v2
macro(add_lsdalton_runtest)
  set(oneValueArgs NAME COST LAUNCH_AGENT)
  set(multiValueArgs LABELS DEPENDS REFERENCE_FILES)
  cmake_parse_arguments(__test
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
    )

  add_test(
    NAME
      ${__test_NAME}
    COMMAND
      ${Python_EXECUTABLE} ${CMAKE_CURRENT_LIST_DIR}/${__test_NAME}/test
      --binary-dir=$<TARGET_FILE_DIR:lsdalton.x>
      --work-dir=${CMAKE_CURRENT_BINARY_DIR}/${__test_NAME}
      --verbose
      #--launch-agent ${__test_LAUNCH_AGENT}  # TODO Understand how to use this for MPI testing
    WORKING_DIRECTORY
      ${CMAKE_CURRENT_BINARY_DIR}
    )

  set_tests_properties(${__test_NAME}
    PROPERTIES
      LABELS "${__test_LABELS};linsca"
    )

  if(__test_COST)
    set_tests_properties(${__test_NAME}
      PROPERTIES
        COST ${__test_COST}
      )
  endif()

  if(__test_DEPENDS)
    set_tests_properties(${__test_NAME}
      PROPERTIES
        DEPENDS ${__test_DEPENDS}
      )
  endif()

  if(__test_REFERENCE_FILES)
    file(
      COPY
        ${__test_REFERENCE_FILES}
      DESTINATION
        ${CMAKE_CURRENT_BINARY_DIR}
      )
  endif()
endmacro()

# This macro uses runtest v1
macro(add_lsdalton_runtest_v1)
  set(oneValueArgs NAME)
  set(multiValueArgs LABELS)
  cmake_parse_arguments(__test
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
    )

  add_test(
    NAME
      ${__test_NAME}
    COMMAND
      ${Python_EXECUTABLE} ${CMAKE_CURRENT_LIST_DIR}/${__test_NAME}/test
      --binary-dir=$<TARGET_FILE_DIR:lsdalton.x>
      --work-dir=${CMAKE_CURRENT_BINARY_DIR}
      --verbose
      --log=${CMAKE_CURRENT_BINARY_DIR}/${__test_NAME}/runtest.stderr.log
    WORKING_DIRECTORY
      ${CMAKE_CURRENT_BINARY_DIR}
    )

  set_tests_properties(${__test_NAME}
    PROPERTIES
      LABELS "${__test_LABELS};linsca"
    )
endmacro()
# Deprecate add_lsdalton_runtest_v1
#macro(add_lsdalton_runtest_v1)
#  message(DEPRECATION "add_lsdalton_runtest_v1 is deprecated - use add_lsdalton_runtest instead")
#  _add_lsdalton_runtest_v1(${ARGV})
#endmacro()

# This macro uses the TEST bash script
macro(add_lsdalton_test)
  set(oneValueArgs NAME)
  set(multiValueArgs LABELS)
  cmake_parse_arguments(__test
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
    )

  get_filename_component(__folder ${CMAKE_CURRENT_LIST_DIR} NAME)

  add_test(
    NAME
      ${__test_NAME}
    COMMAND
      ${CMAKE_CURRENT_LIST_DIR}/../TEST -lsdalton ${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_BINDIR}/lsdalton ${__folder}/${__test_NAME}
    WORKING_DIRECTORY
      ${CMAKE_CURRENT_BINARY_DIR}
    )

  set_tests_properties(${__test_NAME}
    PROPERTIES
      LABELS "${__test_LABELS};linsca"
    )
endmacro()
# Deprecate add_lsdalton_test
#macro(add_lsdalton_test)
#  message(DEPRECATION "add_lsdalton_test is deprecated - use add_lsdalton_runtest instead")
#  _add_lsdalton_test(${ARGV})
#endmacro()

# Also this one uses the TEST bash script
macro(add_restart_test)
  set(oneValueArgs NAME)
  set(multiValueArgs LABELS)
  cmake_parse_arguments(__test
    "${options}"
    "${oneValueArgs}"
    "${multiValueArgs}"
    ${ARGN}
    )

  get_filename_component(__folder ${CMAKE_CURRENT_LIST_DIR} NAME)

  add_test(
    NAME
      ${__test_NAME}
    COMMAND
      ${CMAKE_CURRENT_LIST_DIR}/../TEST -lsdalton ${PROJECT_BINARY_DIR}/${CMAKE_INSTALL_BINDIR}/lsdalton -param "-restart -D" ${__folder}/${__test_NAME}
    WORKING_DIRECTORY
      ${CMAKE_CURRENT_BINARY_DIR}
    )

  set_tests_properties(${__test_NAME}
    PROPERTIES
      LABELS "${__test_LABELS};linsca"
    )
endmacro()
# Deprecate add_lsdalton_test
#macro(add_restart_test)
#  message(DEPRECATION "add_restart_test is deprecated - use add_lsdalton_runtest instead")
#  _add_restart_test(${ARGV})
#endmacro()

#Use the LSDALTON_DEVELOPER FLAG TO SET SOME SPECIFIC TESTCASES
set(LSDALTON_DEV $ENV{LSDALTON_DEVELOPER})

enable_testing()
include(CTest)
add_subdirectory(test) # This must come last!!
