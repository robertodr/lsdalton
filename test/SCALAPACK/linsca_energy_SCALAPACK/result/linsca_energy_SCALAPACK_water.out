 THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDALTON - An electronic structure program  **********    
     ******************************************************************    
  
  
      This is output from LSDALTON 2016.alpha
  
  
      IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
      THE FOLLOWING PAPER SHOULD BE CITED:
      
      K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
      L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
      P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
      J. J. Eriksen, P. Ettenhuber, B. Fernandez,
      L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
      A. Halkier, C. Haettig, H. Heiberg,
      T. Helgaker, A. C. Hennum, H. Hettema,
      E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
      M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
      D. Jonsson, P. Joergensen, J. Kauczor,
      S. Kirpekar, T. Kjaergaard, W. Klopper,
      S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
      A. Krapp, K. Kristensen, A. Ligabue,
      O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
      C. Neiss, C. B. Nielsen, P. Norman,
      J. Olsen, J. M. H. Olsen, A. Osted,
      M. J. Packer, F. Pawlowski, T. B. Pedersen,
      P. F. Provasi, S. Reine, Z. Rinkevicius,
      T. A. Ruden, K. Ruud, V. Rybkin,
      P. Salek, C. C. M. Samson, A. Sanchez de Meras,
      T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
      K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
      P. R. Taylor, A. M. Teale, E. I. Tellgren,
      D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
      O. Vahtras, M. A. Watson, D. J. D. Wilson,
      M. Ziolkowski, and H. AAgren,
      "The Dalton quantum chemistry program system",
      WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                                
      LSDALTON authors in alphabetical order (main contribution(s) in parenthesis)
      ----------------------------------------------------------------------------
      Vebjoern Bakken,         University of Oslo,        Norway    (geometry optimizer)
      Radovan Bast,            KTH Stockholm,             Sweden    (CMake, Testing)
      Sonia Coriani,           University of Trieste,     Italy     (response)
      Patrick Ettenhuber,      Aarhus University,         Denmark   (CCSD)
      Trygve Helgaker,         University of Oslo,        Norway    (supervision)
      Stinne Hoest,            Aarhus University,         Denmark   (SCF optimization)
      Ida-Marie Hoeyvik,       Aarhus University,         Denmark   (orbital localization, SCF optimization)
      Robert Izsak,            University of Oslo,        Norway    (ADMM)
      Branislav Jansik,        Aarhus University,         Denmark   (trilevel, orbital localization)
      Poul Joergensen,         Aarhus University,         Denmark   (supervision)
      Joanna Kauczor,          Aarhus University,         Denmark   (response solver)
      Thomas Kjaergaard,       Aarhus University,         Denmark   (response, integrals)
      Andreas Krapp,           University of Oslo,        Norway    (FMM, dispersion-corrected DFT)
      Kasper Kristensen,       Aarhus University,         Denmark   (response, DEC)
      Patrick Merlot,          University of Oslo,        Norway    (ADMM)
      Cecilie Nygaard,         Aarhus University,         Denmark   (SOEO)
      Jeppe Olsen,             Aarhus University,         Denmark   (supervision)
      Simen Reine,             University of Oslo,        Norway    (integrals, geometry optimizer)
      Vladimir Rybkin,         University of Oslo,        Norway    (geometry optimizer, dynamics)
      Pawel Salek,             KTH Stockholm,             Sweden    (FMM, DFT functionals)
      Andrew M. Teale,         University of Nottingham   England   (E-coefficients)
      Erik Tellgren,           University of Oslo,        Norway    (density fitting, E-coefficients)
      Andreas J. Thorvaldsen,  University of Tromsoe,     Norway    (response)
      Lea Thoegersen,          Aarhus University,         Denmark   (SCF optimization)
      Mark Watson,             University of Oslo,        Norway    (FMM)
      Marcin Ziolkowski,       Aarhus University,         Denmark   (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
   This is an MPI run using       1 processes.
 Who compiled             | tkjaer
 Host                     | localhost.localdomain
 System                   | Linux-3.16.6-200.fc20.x86_64
 CMake generator          | Unix Makefiles
 Processor                | x86_64
 64-bit integers          | OFF
 MPI                      | ON
 Fortran compiler         | /home/tkjaer/mpich2-install-debug/bin/mpif90
 Fortran compiler version | ifort (IFORT) 14.0.3 20140422
 C compiler               | /home/tkjaer/mpich2-install-debug/bin/mpicc
 C compiler version       | icc (ICC) 14.0.3 20140422
 C++ compiler             | /home/tkjaer/mpich2-install-debug/bin/mpic++
 C++ compiler version     | unknown
 BLAS                     | -Wl,--start-group;/opt/intel/composer_xe_2013_sp1.
                          | 3.174/mkl/lib/intel64/libmkl_scalapack_lp64.so;/op
                          | t/intel/composer_xe_2013_sp1.3.174/mkl/lib/intel64
                          | /libmkl_intel_lp64.so;/opt/intel/composer_xe_2013_
                          | sp1.3.174/mkl/lib/intel64/libmkl_intel_thread.so;/
                          | opt/intel/composer_xe_2013_sp1.3.174/mkl/lib/intel
                          | 64/libmkl_core.so;/opt/intel/composer_xe_2013_sp1.
                          | 3.174/mkl/lib/intel64/libmkl_blacs_intelmpi_lp64.s
                          | o;/usr/lib64/libpthread.so;/usr/lib64/libm.so;-ope
                          | nmp;-Wl,--end-group
 LAPACK                   | -Wl,--start-group;/opt/intel/composer_xe_2013_sp1.
                          | 3.174/mkl/lib/intel64/libmkl_lapack95_lp64.a;/opt/
                          | intel/composer_xe_2013_sp1.3.174/mkl/lib/intel64/l
                          | ibmkl_intel_lp64.so;-openmp;-Wl,--end-group
 Static linking           | OFF
 Last Git revision        | 875085c1e3c1c0ce7bd105e88d5585fe087262d5
 Git branch               | master
 Configuration time       | 2015-03-08 21:10:42.151734



 WARNING - deprecated old .mol fixed format input has been detected:
   2     0         *                                                            
 WARNING - this input format may not be supported in future releases.



 New recommended format looks like:
 Atomtypes=2 Angstrom
 (example using 2 atomtypes)
 Note only integer charges are allowed in current LsDalton version



 WARNING - deprecated old .mol fixed format input has been detected:
        8.    1                                                                 
 WARNING - this input format may not be supported in future releases.



 New recommended format looks like
 Charge=6.0 Atoms=2
 Using an example of 2 Carbon atoms of this type
 Note only integer charges are allowed in current LsDalton ve



 WARNING - deprecated old .mol fixed format input has been detected:
        1.    2                                                                 
 WARNING - this input format may not be supported in future releases.



 New recommended format looks like
 Charge=6.0 Atoms=2
 Using an example of 2 Carbon atoms of this type
 Note only integer charges are allowed in current LsDalton ve

Start simulation
     Date and time (Linux)  : Sun Mar  8 21:33:33 2015
     Host name              : localhost.localdomain                   
                      
 -----------------------------------------
          PRINTING THE MOLECULE.INP FILE 
 -----------------------------------------
                      
  BASIS                                   
  3-21G                                   
  water R(OH) = 0.95Aa , <HOH = 109 deg.  
  Distance in Aangstroms                  
     2     0         *                                        
          8.    1                                             
  O      0.00000   0.00000   0.00000                          
          1.    2                                             
  H      0.55168   0.77340   0.00000                          
  H      0.55168  -0.77340   0.00000                          
                      
 -----------------------------------------
          PRINTING THE LSDALTON.INP FILE 
 -----------------------------------------
                      
  **GENERAL                                                                                           
  .SCALAPACK                                                                                          
  .SCALAPACKBLOCKSIZE                                                                                 
  11                                                                                                  
  .SCALAPACKWORKAROUND                                                                                
  **WAVE FUNCTIONS                                                                                    
  .HF                                                                                                 
  *DENSOPT                                                                                            
  .RH                                                                                                 
  .DIIS                                                                                               
  .NVEC                                                                                               
  8                                                                                                   
  .CONVTHR                                                                                            
  3.D-5                                                                                               
  .START                                                                                              
  H1DIAG                                                                                              
  **INFO                                                                                              
  .DEBUG_MPI_MEM                                                                                      
  *END OF INPUT                                                                                       
  
  MOLPRINT IS SET TO       0



 WARNING - deprecated old .mol fixed format input has been detected:
   2     0         *                                                            
 WARNING - this input format may not be supported in future releases.



 New recommended format looks like:
 Atomtypes=2 Angstrom
 (example using 2 atomtypes)
 Note only integer charges are allowed in current LsDalton version
  Coordinates are entered in Angstroms and converted to atomic units.
  Conversion factor : 1 bohr = 0.52917721 A



 WARNING - deprecated old .mol fixed format input has been detected:
        8.    1                                                                 
 WARNING - this input format may not be supported in future releases.



 New recommended format looks like
 Charge=6.0 Atoms=2
 Using an example of 2 Carbon atoms of this type
 Note only integer charges are allowed in current LsDalton ve



 WARNING - deprecated old .mol fixed format input has been detected:
        1.    2                                                                 
 WARNING - this input format may not be supported in future releases.



 New recommended format looks like
 Charge=6.0 Atoms=2
 Using an example of 2 Carbon atoms of this type
 Note only integer charges are allowed in current LsDalton ve


                    Cartesian Coordinates Linsca (au)
                    ---------------------------------

   Total number of coordinates:  9
   Written in atomic units    
 
   1   O        x      0.0000000000
   2            y      0.0000000000
   3            z      0.0000000000
 
   4   H        x      1.0425241136
   5            y      1.4615141920
   6            z      0.0000000000
 
   7   H        x      1.0425241136
   8            y     -1.4615141920
   9            z      0.0000000000
 
 
 BASISSETLIBRARY  : REGULAR  
   
BASISSETLIBRARY
 Number of Basisset           1
 BASISSET:  3-21G                                             
  CHARGES:    8.0000    1.0000
                      
   
  CALLING BUILD BASIS WITH DEFAULT REGULAR   BASIS
   
 OPENING FILE
 /home/tkjaer/DaltonDevelopment2015/MatrixModule/build-debug-scalapack-mpi/basis
 /3-21G
 PRINT MOLECULE AND BASIS IN FULL INPUT
                      
THE MOLECULE
 --------------------------------------------------------------------
 Molecular Charge                    :    0.0000
    Regular basisfunctions             :       13
    Valence basisfunctions             :        0
    Primitive Regular basisfunctions   :       21
    Primitive Valence basisfunctions   :        0
 --------------------------------------------------------------------
                      
                      
 --------------------------------------------------------------------
  atom  charge  Atomicbasis    Phantom   nPrimREG  nContREG
 --------------------------------------------------------------------
     1   8.000  3-21G           F             15           9
     2   1.000  3-21G           F              3           2
     3   1.000  3-21G           F              3           2
                      
 The cartesian centers in Atomic units. 
  ATOM  NAME  ISOTOPE            X                 Y                 Z     
     1  O           1        0.00000000        0.00000000        0.00000000
     2  H           1        1.04252411        1.46151419        0.00000000
     3  H           1        1.04252411       -1.46151419        0.00000000
                      
                      
Atoms and basis sets
  Total number of atoms        :      3
  THE  REGULAR   is on R =   1
---------------------------------------------------------------------
  atom label  charge basisset                prim     cont   basis
---------------------------------------------------------------------
      1 O      8.000 3-21G                     15        9 [6s3p|3s2p]                                  
      2 H      1.000 3-21G                      3        2 [3s|2s]                                      
      3 H      1.000 3-21G                      3        2 [3s|2s]                                      
---------------------------------------------------------------------
total         10                               21       13
---------------------------------------------------------------------
                      
 Configuration:
 ==============
    This is an OpenMP calculation using   4 threads.
    This is an MPI calculation using   1 processors
 
Density subspace min. method    : DIIS                    
Density optimization : Diagonalization                    
 
 Maximum size of Fock/density queue in averaging:           8
 
Convergence threshold for gradient:   0.30E-04
 
We perform the calculation in the Grand Canonical basis
(see PCCP 2009, 11, 5805-5813)
To use the standard input basis use .NOGCBASIS
 
Since the input basis set is a segmented contracted basis we
perform the integral evaluation in the more efficient
standard input basis and then transform to the Grand 
Canonical basis, which is general contracted.
You can force the integral evaluation in Grand 
Canonical basis by using the keyword
.NOGCINTEGRALTRANSFORM
 
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
    This is an MPI calculation using   1 processors combinded
    with SCALAPACK for memory distribution and parallelization.
 
Matrix type: mtype_scalapack
Scalapack GroupSize From Input =     1
Scalapack Grid initiation Block Size =    11
Scalapack Grid initiation nprow      =     1
Scalapack Grid initiation npcol      =     1
 The SCF Convergence Criteria is applied to the gradnorm in AO basis
 
 End of configuration!
 
 
Matrix type: mtype_dense
 
 A set of atomic calculations are performed in order to construct
 the Grand Canonical Basis set (see PCCP 11, 5805-5813 (2009))
 as well as JCTC 5, 1027 (2009)
 This is done in order to use the TRILEVEL starting guess and 
 perform orbital localization
 This is Level 1 of the TRILEVEL starting guess and is performed per default.
 The use of the Grand Canonical Basis can be deactivated using .NOGCBASIS
 under the **GENERAL section. This is NOT recommended if you do TRILEVEL 
 or orbital localization.
 
 
 Level 1 atomic calculation on 3-21G Charge   8
 ================================================
 The Coulomb energy contribution    49.7803543454545     
 The Exchange energy contribution    9.35687927117893     
 The Fock energy contribution   -40.4234750742756     
*******************************************************************************************###
 it        E(HF)            dE(HF)         exit      alpha RHshift   RHinfo  AO gradient     ###
*******************************************************************************************###
  1    -69.1958692875    0.00000000000    0.00      0.00000    0.00    0.0000000    7.66E+00  ###
 The Coulomb energy contribution    32.3200271168756     
 The Exchange energy contribution    7.17721887301396     
 The Fock energy contribution   -25.1428082438617     
  2    -73.3827432449   -4.18687395742    0.00      0.00000    0.00    0.0000000    3.24E+00  ###
 The Coulomb energy contribution    36.0177942107831     
 The Exchange energy contribution    7.58356684718489     
 The Fock energy contribution   -28.4342273635982     
  3    -73.8637832343   -0.48103998935   -1.00      0.00000    0.00    0.0000000    1.91E-01  ###
 The Coulomb energy contribution    36.2303500950324     
 The Exchange energy contribution    7.60511464562423     
 The Fock energy contribution   -28.6252354494082     
  4    -73.8653859895   -0.00160275526   -1.00      0.00000    0.00    0.0000000    3.25E-02  ###
 The Coulomb energy contribution    36.1920991989315     
 The Exchange energy contribution    7.60036983193259     
 The Fock energy contribution   -28.5917293669989     
  5    -73.8654283554   -0.00004236591   -1.00      0.00000    0.00    0.0000000    7.33E-04  ###
 
 Level 1 atomic calculation on 3-21G Charge   1
 ================================================
 The Coulomb energy contribution   0.319990996790884     
 The Exchange energy contribution   0.159995498395442     
 The Fock energy contribution  -0.159995498395442     
*******************************************************************************************###
 it        E(HF)            dE(HF)         exit      alpha RHshift   RHinfo  AO gradient     ###
*******************************************************************************************###
  1     -0.3362031110    0.00000000000    0.00      0.00000    0.00    0.0000000    1.99E-01  ###
 The Coulomb energy contribution   0.296183681133060     
 The Exchange energy contribution   0.148091840566530     
 The Fock energy contribution  -0.148091840566530     
  2     -0.3429646205   -0.00676150947    0.00      0.00000    0.00    0.0000000    1.32E-02  ###
 The Coulomb energy contribution   0.294614532546898     
 The Exchange energy contribution   0.147307266273449     
 The Fock energy contribution  -0.147307266273449     
  3     -0.3429945468   -0.00002992637   -1.00      0.00000    0.00    0.0000000    3.65E-05  ###
 
Matrix type: mtype_scalapack
Scalapack GroupSize From Input =     1
Scalapack Grid initiation Block Size =    11
Scalapack Grid initiation nprow      =     1
Scalapack Grid initiation npcol      =     1
 
 Optimize first density for H1 operator
 
 
Max no. of matrices allocated in Level 2 / get_initial_dens:         21
 Preparing to do S^1/2 decomposition...
  Relative convergence threshold for solver:  1.000000000000000E-002
  SCF Convergence criteria for gradient norm:  3.000000000000000E-005
** Get Fock matrix number   1
 The Coulomb energy contribution    73.0850669395612     
 The Exchange energy contribution   -12.4199941741990     
 The Fock energy contribution   -60.6650727653623     
*******************************************************************************************###
 it        E(HF)            dE(HF)         exit      alpha RHshift   RHinfo  AO gradient     ###
*******************************************************************************************###
  1    -66.1476748721    0.00000000000    0.00      0.00000    0.00    0.0000000    1.80E+01  ###
** Make average of the last F and D matrices
** Get new density 
No. of matmuls in get_density:     1
 >>>  CPU Time used in SCF iteration is   0.11 seconds
 >>> wall Time used in SCF iteration is   0.03 seconds
** Get Fock matrix number   2
 The Coulomb energy contribution    32.7689659129241     
 The Exchange energy contribution   -7.57049594548894     
 The Fock energy contribution   -25.1984699674352     
  2    -70.2984321121   -4.15075724000    0.00      0.00000    0.00    0.0000000    1.04E+01  ###
** Make average of the last F and D matrices
** Get new density 
No. of matmuls in get_density:     1
 >>>  CPU Time used in SCF iteration is   0.11 seconds
 >>> wall Time used in SCF iteration is   0.03 seconds
** Get Fock matrix number   3
 The Coulomb energy contribution    51.2578637511217     
 The Exchange energy contribution   -9.47688665772095     
 The Fock energy contribution   -41.7809770934007     
  3    -75.2687726350   -4.97034052288   -1.00      0.00000    0.00    0.0000000    4.56E+00  ###
** Make average of the last F and D matrices
** Get new density 
No. of matmuls in get_density:     1
 >>>  CPU Time used in SCF iteration is   0.11 seconds
 >>> wall Time used in SCF iteration is   0.03 seconds
** Get Fock matrix number   4
 The Coulomb energy contribution    48.3807719075856     
 The Exchange energy contribution   -9.14443279063585     
 The Fock energy contribution   -39.2363391169498     
  4    -75.5453210638   -0.27654842882   -1.00      0.00000    0.00    0.0000000    1.45E+00  ###
** Make average of the last F and D matrices
** Get new density 
No. of matmuls in get_density:     1
 >>>  CPU Time used in SCF iteration is   0.11 seconds
 >>> wall Time used in SCF iteration is   0.03 seconds
** Get Fock matrix number   5
 The Coulomb energy contribution    46.9904232853827     
 The Exchange energy contribution   -8.98789830650960     
 The Fock energy contribution   -38.0025249788731     
  5    -75.5847866987   -0.03946563492   -1.00      0.00000    0.00    0.0000000    9.41E-02  ###
** Make average of the last F and D matrices
** Get new density 
No. of matmuls in get_density:     1
 >>>  CPU Time used in SCF iteration is   0.11 seconds
 >>> wall Time used in SCF iteration is   0.03 seconds
** Get Fock matrix number   6
 The Coulomb energy contribution    46.8878254867814     
 The Exchange energy contribution   -8.97681382100172     
 The Fock energy contribution   -37.9110116657796     
  6    -75.5853855466   -0.00059884783   -1.00      0.00000    0.00    0.0000000    4.28E-02  ###
** Make average of the last F and D matrices
** Get new density 
No. of matmuls in get_density:     1
 >>>  CPU Time used in SCF iteration is   0.11 seconds
 >>> wall Time used in SCF iteration is   0.03 seconds
** Get Fock matrix number   7
 The Coulomb energy contribution    46.9233502711626     
 The Exchange energy contribution   -8.98110168145832     
 The Fock energy contribution   -37.9422485897043     
  7    -75.5854205701   -0.00003502353   -1.00      0.00000    0.00    0.0000000    1.34E-02  ###
** Make average of the last F and D matrices
** Get new density 
No. of matmuls in get_density:     1
 >>>  CPU Time used in SCF iteration is   0.11 seconds
 >>> wall Time used in SCF iteration is   0.03 seconds
** Get Fock matrix number   8
 The Coulomb energy contribution    46.9123192502882     
 The Exchange energy contribution   -8.97989999541578     
 The Fock energy contribution   -37.9324192548724     
  8    -75.5854229095   -0.00000233943   -1.00      0.00000    0.00    0.0000000    3.03E-04  ###
** Make average of the last F and D matrices
** Get new density 
No. of matmuls in get_density:     1
 >>>  CPU Time used in SCF iteration is   0.11 seconds
 >>> wall Time used in SCF iteration is   0.03 seconds
** Get Fock matrix number   9
 The Coulomb energy contribution    46.9125145967543     
 The Exchange energy contribution   -8.97991768878341     
 The Fock energy contribution   -37.9325969079709     
  9    -75.5854229142   -0.00000000468   -1.00      0.00000    0.00    0.0000000    1.61E-04  ###
** Make average of the last F and D matrices
** Get new density 
No. of matmuls in get_density:     1
 >>>  CPU Time used in SCF iteration is   0.11 seconds
 >>> wall Time used in SCF iteration is   0.03 seconds
** Get Fock matrix number  10
 The Coulomb energy contribution    46.9123845676063     
 The Exchange energy contribution   -8.97990240010129     
 The Fock energy contribution   -37.9324821675050     
 10    -75.5854229150   -0.00000000080   -1.00      0.00000    0.00    0.0000000    1.90E-05  ###
SCF converged in     10 iterations
 >>>  CPU Time used in **ITER is   1.08 seconds
 >>> wall Time used in **ITER is   0.27 seconds
 
Total no. of matmuls in SCF optimization:        114
 
 Number of occupied orbitals:           5
 Number of virtual orbitals:           8
 
 Number of occupied orbital energies to be found:           1
 Number of virtual orbital energies to be found:           1
 
 
Calculation of HOMO-LUMO gap
============================
 
Calculation of occupied orbital energies converged in     3 iterations!
 
Calculation of virtual orbital energies converged in     4 iterations!
 
    E(LUMO):                         0.266568 au
   -E(HOMO):                        -0.529463 au
   ------------------------------
    HOMO-LUMO Gap (iteratively):     0.796031 au
 
 
********************************************************
 it       dE(HF)          exit   RHshift    RHinfo 
********************************************************
  1    0.00000000000    0.0000    0.0000    0.0000000
  2   -4.15075724000    0.0000    0.0000    0.0000000
  3   -4.97034052288   -1.0000    0.0000    0.0000000
  4   -0.27654842882   -1.0000    0.0000    0.0000000
  5   -0.03946563492   -1.0000    0.0000    0.0000000
  6   -0.00059884783   -1.0000    0.0000    0.0000000
  7   -0.00003502353   -1.0000    0.0000    0.0000000
  8   -0.00000233943   -1.0000    0.0000    0.0000000
  9   -0.00000000468   -1.0000    0.0000    0.0000000
 10   -0.00000000080   -1.0000    0.0000    0.0000000
 
======================================================================
                   LINSCF ITERATIONS:
  It.nr.               Energy                 AO Gradient norm
======================================================================
    1           -66.14767487212309049482      0.180212150366347D+02
    2           -70.29843211212219955542      0.104056505077565D+02
    3           -75.26877263499939374469      0.456127083252894D+01
    4           -75.54532106381944345230      0.144702856247221D+01
    5           -75.58478669873503008603      0.940538952815926D-01
    6           -75.58538554656331598380      0.427745715013175D-01
    7           -75.58542057009330505934      0.133986394560189D-01
    8           -75.58542290952689768346      0.302963179235536D-03
    9           -75.58542291420909009503      0.160742838715669D-03
   10           -75.58542291500670273763      0.189502401978389D-04
 
      SCF converged !!!! 
         >>> Final results from LSDALTON <<<
 
 
      Final HF energy:                       -75.585422915007
      Nuclear repulsion:                       9.254577176284
      Electronic energy:                     -84.840000091291
 
 >>>  CPU Time used in *SCF is   2.42 seconds
 >>> wall Time used in *SCF is   0.92 seconds
 
Total no. of matmuls used:                       122
Total no. of Fock/KS matrix evaluations:          10
Max no. of matrices allocated in Level 3:         42
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                  Memory statistics          
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
  Allocated memory (TOTAL):                     0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(matrix)):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (real):                      0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MPI):                       0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (complex):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integer):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (logical):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (character):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (AOBATCH):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ODBATCH):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (LSAOTENSOR):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (SLSAOTENSOR):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (GLOBALLSAOTENSOR):          0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ATOMTYPEITEM):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ATOMITEM):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (LSMATRIX):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECORBITAL):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECFRAG):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (overlapType):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (BATCHTOORB):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (DECAOBATCHINFO):            0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MYPOINTER):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (fragmentAOS):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY2):                    0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY4):                    0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ARRAY):                     0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (PNOSpaceInfo):              0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MP2DENS):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (TRACEBACK):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (MP2GRAD):                   0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(lvec_data)):           0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (type(lattice_cell)):        0 byte  - Should be zero - otherwise a leakage is present
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                  Additional Memory information          
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
  Allocated memory (linkshell):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integrand):               0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (integralitem):            0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (intwork):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (overlap):                 0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (ODitem):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (lstensor):                0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (FMM   ):                  0 byte  - Should be zero - otherwise a leakage is present
  Allocated memory (XC    ):                  0 byte  - Should be zero - otherwise a leakage is present
  Max allocated memory, TOTAL                         85.234 MB
  Max allocated memory, Type(matrix) on Master node   57.528 kB
  Max allocated memory, Type(matrix) on all nodes     56.784 kB
  Max allocated memory, real(realk)                    2.783 MB
  Max allocated memory, MPI                            0.000 Byte
  Max allocated memory, complex(complexk)              0.000 Byte
  Max allocated memory, integer                       38.960 kB
  Max allocated memory, logical                      203.108 kB
  Max allocated memory, character                     74.880 kB
  Max allocated memory, AOBATCH                       35.280 kB
  Max allocated memory, DECORBITAL                     0.000 Byte
  Max allocated memory, DECFRAG                        0.000 Byte
  Max allocated memory, BATCHTOORB                     0.000 Byte
  Max allocated memory, DECAOBATCHINFO                 0.000 Byte
  Max allocated memory, MYPOINTER                      0.000 Byte
  Max allocated memory, fragmentAOS                    0.000 Byte
  Max allocated memory, ARRAY2                         0.000 Byte
  Max allocated memory, ARRAY4                         0.000 Byte
  Max allocated memory, ARRAY                         84.400 MB
  Max allocated memory, PNOSpaceInfo                   0.000 Byte
  Max allocated memory, MP2DENS                        0.000 Byte
  Max allocated memory, TRACEBACK                      0.000 Byte
  Max allocated memory, MP2GRAD                        0.000 Byte
  Max allocated memory, ODBATCH                        6.776 kB
  Max allocated memory, LSAOTENSOR                    15.552 kB
  Max allocated memory, SLSAOTENSOR                   12.288 kB
  Max allocated memory, GLOBALLSAOTENSOR               0.000 Byte
  Max allocated memory, ATOMTYPEITEM                 225.056 kB
  Max allocated memory, ATOMITEM                       2.176 kB
  Max allocated memory, LSMATRIX                       7.360 kB
  Max allocated memory, OverlapT                     451.584 kB
  Max allocated memory, linkshell                      1.260 kB
  Max allocated memory, integrand                    258.048 kB
  Max allocated memory, integralitem                 819.200 kB
  Max allocated memory, IntWork                       72.992 kB
  Max allocated memory, Overlap                        1.554 MB
  Max allocated memory, ODitem                         4.928 kB
  Max allocated memory, LStensor                      18.080 kB
  Max allocated memory, FMM                            0.000 Byte
  Max allocated memory, XC                             0.000 Byte
  Max allocated memory, Lvec_data                      0.000 Byte
  Max allocated memory, Lattice_cell                   0.000 Byte
*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
 
  The total memory used across all MPI nodes
  Max allocated memory, TOTAL                         85.234 MB
time spent in unlock:        0.0000000000
  Largest memory allocated on a single MPI slave node (including Master)
  Max allocated memory, TOTAL                         85.234 MB
  Largest memory allocated on a single MPI slave node (exclusing Master)
  Max allocated memory, TOTAL                          0.000 Byte
  Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
 >>>  CPU Time used in LSDALTON is   2.82 seconds
 >>> wall Time used in LSDALTON is   1.04 seconds

End simulation
     Date and time (Linux)  : Sun Mar  8 21:33:34 2015
     Host name              : localhost.localdomain                   
