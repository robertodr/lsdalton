#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSresponse_HF_alpha.info <<'%EOF%'
   LSresponse_HF_alpha
   -------------------
   Molecule:         H2O2
   Wave Function:    HF / 6-31G
   Test Purpose:     Test real and complex polarizability in LSDALTON (Kasper K).
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSresponse_HF_alpha.mol <<'%EOF%'
BASIS
6-31G
Hydrogen Peroxide

AtomTypes=2 Generators=0
Charge=8.0 Atoms=2
O    -0.00000000  1.40784586 -0.09885600
O     0.00000000 -1.40784586 -0.09885600
Charge=1.0 Atoms=2
H     0.69081489  1.72614891  1.56891868
H    -0.69081489 -1.72614891  1.56891868
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSresponse_HF_alpha.dal <<'%EOF%'
**WAVE FUNCTIONS
.HF
*DENSOPT
.ARH DAVID
.CONVDYN
TIGHT
**RESPONS
*SOLVER
.CONVDYN
TIGHT
*ALPHA
.BFREQ
2
0.1  0.20007543
.IMBFREQ
2
0.0  0.001
*END OF INPUT  
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > LSresponse_HF_alpha.check
cat >> LSresponse_HF_alpha.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi


# Energy
CRIT1=`$GREP "Final * HF energy\: * \-150\.69387" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"

# Memory test
CRIT2=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT2`
CTRL[2]=1
ERROR[2]="Memory leak -"

# Real Polarizability tests
CRIT1=`$GREP "Ex *  3.55[0-9][0-9][0-9][0-9][0-9] *  1.61[0-9][0-9][0-9][0-9][0-9]" $log | wc -l`
CRIT2=`$GREP "Ey * 1.61[0-9][0-9][0-9][0-9][0-9] * 18.10[0-9][0-9][0-9][0-9]" $log | wc -l`

TEST[3]=`expr  $CRIT1 \+ $CRIT2 `
CTRL[3]=2
ERROR[3]="Error in real polarizability"

# Imaginary part of complex polarizability tests
CRIT1=`$GREP "Ex * 1.8[5-6][0-9][0-9][0-9][0-9][0-9] * 4.27[0-9][0-9][0-9][0-9][0-9]" $log | wc -l`
CRIT2=`$GREP "Ey * 4.27[0-9][0-9][0-9][0-9][0-9] * 9.87[0-9][0-9][0-9][0-9[0-9]" $log | wc -l`

TEST[4]=`expr  $CRIT1 \+ $CRIT2 `
CTRL[4]=2
ERROR[4]="Error in imaginary part of complex polarizability"


PASSED=1
for i in 1 2 3 4
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
