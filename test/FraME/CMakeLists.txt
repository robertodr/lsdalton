add_lsdalton_runtest_v1(energy_m3 "linsca;runtest;frame;ContinuousIntegration")
add_lsdalton_runtest_v1(energy_m3p11 "linsca;runtest;frame;ContinuousIntegration")
add_lsdalton_runtest_v1(response_m3p11 "linsca;runtest;frame;lsresponse;ContinuousIntegration")
# TODO Uncomment or remove
#add_lsdalton_runtest_v1(energy_m3p11_df "linsca;runtest;frame;ContinuousIntegration")
#add_lsdalton_runtest_v1(optimize_m3 "linsca;runtest;frame")
#add_lsdalton_runtest_v1(optimize_simple "linsca;runtest;frame")
