     THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton 1.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,        Norway   (Geometry optimizer)
    Radovan Bast,           UiT The Arctic University of Norway (CMake, Testing)
    Pablo Baudin,           Aarhus University,         Denmark  (DEC,CCSD)
    Sonia Coriani,          University of Trieste,     Italy    (Response)
    Patrick Ettenhuber,     Aarhus University,         Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,         Denmark  (CCSD(T), DEC)
    Trygve Helgaker,        University of Oslo,        Norway   (Supervision)
    Stinne Hoest,           Aarhus University,         Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,         Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,        Norway   (ADMM)
    Branislav Jansik,       Aarhus University,         Denmark  (Trilevel, orbital localization)
    Poul Joergensen,        Aarhus University,         Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,         Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,         Denmark  (RSP, INT, DEC, SCF, Readin, MPI, MAT)
    Andreas Krapp,          University of Oslo,        Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,         Denmark  (Response, DEC)
    Patrick Merlot,         University of Oslo,        Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,         Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,         Denmark  (Supervision)
    Simen Reine,            University of Oslo,        Norway   (Integrals, geometry optimizer)
    Vladimir Rybkin,        University of Oslo,        Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,             Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham   England  (E-coefficients)
    Erik Tellgren,          University of Oslo,        Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,     Norway   (Response)
    Lea Thoegersen,         Aarhus University,         Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,        Norway   (FMM)
    Marcin Ziolkowski,      Aarhus University,         Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
     Who compiled             | simensr
     Host                     | 1x-193-157-182-154.uio.no
     System                   | Darwin-14.5.0
     CMake generator          | Unix Makefiles
     Processor                | x86_64
     64-bit integers          | OFF
     MPI                      | OFF
     Fortran compiler         | /usr/local/bin/gfortran
     Fortran compiler version | GNU Fortran (GCC) 5.0.0 20141012 (experimental)
     C compiler               | /usr/local/bin/gcc
     C compiler version       | gcc (GCC) 5.0.0 20141012 (experimental)
     C++ compiler             | /usr/local/bin/g++
     C++ compiler version     | g++ (GCC) 5.0.0 20141012 (experimental)
     BLAS                     | /usr/lib/libblas.dylib
     LAPACK                   | /usr/lib/liblapack.dylib
     Static linking           | OFF
     Last Git revision        | 69e2c0b06c7f769740dc7ae7dd08bbb35333117d
     Git branch               | frame-openrsp
     Configuration time       | 2018-03-21 09:40:15.432117
  
 Default 1st order method will be used: BFGS update.
 Optimization will be performed in Cartesian coordinates.
 Initial diagonal  Hessian will have elements equal to:   1.000000

 Current Hessian will not be written out to DALTON.HES.


 Trust region method will be used to control step (default).


 Baker's convergence criteria will be used.


 New thresholding scheme for geometry convergence will be used
 -------------------------------------------------------------
 Root-mean-square gradient threshold set to    :      1.00D-05
 Maximum gradient element threshold set to     :      5.00D-05
 Root-mean-square step threshold set to        :      3.00D-05
 Maximum step element threshold set to         :      1.50D-04

         Start simulation
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                   
    STO-3G                                  
    Acrolein                                
    ------------------------                
    AtomTypes=1 NoSymmetry Angstrom                                                                                         
    Charge=1.0 Atoms=2                                                                                                      
    H              0.0   0.0    0.0                                                                                         
    H              1.0   1.0    1.0                                                                                         
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **GENERAL
    .NOGCBASIS
    **OPTIMI
    .CARTES
    .INITEV
    1.D0
    .BAKER
    .MAX IT
    1
    **FraME
    **INTEGRALS
    .NO SCREEN
    **WAVE FUNCTIONS
    .HF
    *END OF INPUT
 
WARNING:  No bonds - no atom pairs are within normal bonding distances
WARNING:  maybe coordinates were in Bohr, but program were told they were in Angstrom ?
 
                      
    Atoms and basis sets
      Total number of atoms        :      2
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 H      1.000 STO-3G                     3        1 [3s|1s]                                      
          2 H      1.000 STO-3G                     3        1 [3s|1s]                                      
    ---------------------------------------------------------------------
    total          2                                6        2
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :        2
      Primitive Regular basisfunctions   :        6
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)

    You have requested Augmented Roothaan-Hall optimization
    => explicit averaging is turned off!

    Expand trust radius if ratio is larger than:            0.75
    Contract trust radius if ratio is smaller than:         0.25
    On expansion, trust radius is expanded by a factor      1.20
    On contraction, trust radius is contracted by a factor  0.70

    Maximum size of subspace in ARH linear equations:       2

    Density subspace min. method    : None                    
    Density optimization            : Augmented RH optimization          

    Maximum size of Fock/density queue in averaging:   10

    Convergence threshold for gradient        :   0.10E-03
    We perform the calculation in the standard input basis
     
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
    The SCF Convergence Criteria is applied to the gradnorm in OAO basis

    End of configuration!


    First density: Atoms in molecule guess

    Iteration 0 energy:       -0.556688724854
 
    Diagonalize the Atoms in Molecule Fock Matrix to obtain Initial Density
    Preparing to do cholesky decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  9.99999975E-05
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift OAO gradient ###
    ******************************************************************************** ###
      1     -0.8446815242    0.00000000000    0.00      0.00000    0.00    3.899E-02 ###
      2     -0.8465960253   -0.00191450112    0.00      0.00000   -0.00    1.626E-02 ###
      3     -0.8469932020   -0.00039717667    0.00      0.00000   -0.00    1.687E-05 ###
    SCF converged in      3 iterations
    >>>  CPU Time used in SCF iterations is   0.01 seconds
    >>> wall Time used in SCF iterations is   0.01 seconds

    Total no. of matmuls in SCF optimization:        117

    Number of occupied orbitals:       1
    Number of virtual orbitals:        1

    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1


    Calculation of HOMO-LUMO gap
    ============================

    Calculation of occupied orbital energies converged in     1 iterations!

    Calculation of virtual orbital energies converged in     1 iterations!

     E(LUMO):                         0.259843 au
    -E(HOMO):                        -0.212790 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     0.472633 au


    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -0.00191450112    0.0000   -0.0000    0.0000000
      3   -0.00039717667    0.0000   -0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 OAO Gradient norm
    ======================================================================
        1            -0.84468152419109077123      0.389894120891196D-01
        2            -0.84659602531533351133      0.162628018398786D-01
        3            -0.84699320198941141502      0.168693529035645D-04

          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<


          Final HF energy:                        -0.846993201989
          Nuclear repulsion:                       0.305520603661
          Electronic energy:                      -1.152513805650




        *************************************************************
        *            MOLECULAR GRADIENT RESULTS (in a.u.)           *
        *************************************************************




                   nuclear rep contribution to gradient
                   ------------------------------------

   H        0.0538915134            0.0538915134            0.0538915134
   H       -0.0538915134           -0.0538915134           -0.0538915134


                     Coulomb contribution to gradient
                     --------------------------------

   H        0.2051434328            0.2051434328            0.2051434328
   H       -0.2051434328           -0.2051434328           -0.2051434328


                    exchange contribution to gradient
                    ---------------------------------

   H       -0.1025717164           -0.1025717164           -0.1025717164
   H        0.1025717164            0.1025717164            0.1025717164


                   twoElectron contribution to gradient
                   ------------------------------------

   H        0.1025717164            0.1025717164            0.1025717164
   H       -0.1025717164           -0.1025717164           -0.1025717164


                   nuc-el attr contribution to gradient
                   ------------------------------------

   H       -0.2994324065           -0.2994324065           -0.2994324065
   H        0.2994324065            0.2994324065            0.2994324065


                     kinetic contribution to gradient
                     --------------------------------

   H        0.0193820711            0.0193820711            0.0193820711
   H       -0.0193820711           -0.0193820711           -0.0193820711
 Calling frame_molecular_gradient.


                  reorthonomal contribution to gradient
                  -------------------------------------

   H        0.0305430096            0.0305430096            0.0305430096
   H       -0.0305430096           -0.0305430096           -0.0305430096


                         Molecular gradient (au)
                         -----------------------

   H       -0.0930440960           -0.0930440960           -0.0930440960
   H        0.0930440960            0.0930440960            0.0930440960


 RMS gradient norm (au):      0.0930440960181858



 Initial Hessian is a diagonal matrix.


 Predicted energy change        0.000000000000000


                     Step in diagonal representation
                     -------------------------------



                       Optimization Control Center
                       ---------------------------



                            Next geometry (au)
                            ------------------

   Total number of coordinates:  6
   Written in atomic units    
 
   1   H        x      0.0000000000
   2            y      0.0000000000
   3            z      0.0000000000
 
   4   H        x      1.8897261339
   5            y      1.8897261339
   6            z      1.8897261339


                         Optimization information
                         ------------------------

 Iteration number               :       0
 End of optimization            :       F 
 Energy at this geometry is     :      -0.846993
 Norm of gradient               :       0.000000
 Norm of step                   :       0.000000
 Updated trust radius           :       0.500000

    First density: Atoms in molecule guess

    Iteration 0 energy:       -0.556688724854
 
    Diagonalize the Atoms in Molecule Fock Matrix to obtain Initial Density
    Preparing to do cholesky decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  9.99999975E-05
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift OAO gradient ###
    ******************************************************************************** ###
      1     -0.8446815242    0.00000000000    0.00      0.00000    0.00    3.899E-02 ###
      2     -0.8465960253   -0.00191450112    0.00      0.00000   -0.00    1.626E-02 ###
      3     -0.8469932020   -0.00039717667    0.00      0.00000   -0.00    1.687E-05 ###
    SCF converged in      3 iterations
    >>>  CPU Time used in SCF iterations is   0.01 seconds
    >>> wall Time used in SCF iterations is   0.01 seconds

    Total no. of matmuls in SCF optimization:        244

    Number of occupied orbitals:       1
    Number of virtual orbitals:        1

    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1


    Calculation of HOMO-LUMO gap
    ============================

    Calculation of occupied orbital energies converged in     1 iterations!

    Calculation of virtual orbital energies converged in     1 iterations!

     E(LUMO):                         0.259843 au
    -E(HOMO):                        -0.212790 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     0.472633 au


    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -0.00191450112    0.0000   -0.0000    0.0000000
      3   -0.00039717667    0.0000   -0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 OAO Gradient norm
    ======================================================================
        1            -0.84468152419109077123      0.389894120891196D-01
        2            -0.84659602531533351133      0.162628018398786D-01
        3            -0.84699320198941141502      0.168693529035645D-04

          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<


          Final HF energy:                        -0.846993201989
          Nuclear repulsion:                       0.305520603661
          Electronic energy:                      -1.152513805650



             StepInfo:Energy difference to previous geometry:
             ------------------------------------------------


      Energy       Old_Energy     Actual_diff  Predicted_diff  Ratio

       -0.8469932020       -0.8469932020   0.0000000000   0.0000000000            NaN

 Trust radius squarely increased due to very good ratio.
 Updated trust radius   0.50000



        *************************************************************
        *            MOLECULAR GRADIENT RESULTS (in a.u.)           *
        *************************************************************




                   nuclear rep contribution to gradient
                   ------------------------------------

   H        0.0538915134            0.0538915134            0.0538915134
   H       -0.0538915134           -0.0538915134           -0.0538915134


                     Coulomb contribution to gradient
                     --------------------------------

   H        0.2051434328            0.2051434328            0.2051434328
   H       -0.2051434328           -0.2051434328           -0.2051434328


                    exchange contribution to gradient
                    ---------------------------------

   H       -0.1025717164           -0.1025717164           -0.1025717164
   H        0.1025717164            0.1025717164            0.1025717164


                   twoElectron contribution to gradient
                   ------------------------------------

   H        0.1025717164            0.1025717164            0.1025717164
   H       -0.1025717164           -0.1025717164           -0.1025717164


                   nuc-el attr contribution to gradient
                   ------------------------------------

   H       -0.2994324065           -0.2994324065           -0.2994324065
   H        0.2994324065            0.2994324065            0.2994324065


                     kinetic contribution to gradient
                     --------------------------------

   H        0.0193820711            0.0193820711            0.0193820711
   H       -0.0193820711           -0.0193820711           -0.0193820711
 Calling frame_molecular_gradient.


                  reorthonomal contribution to gradient
                  -------------------------------------

   H        0.0305430096            0.0305430096            0.0305430096
   H       -0.0305430096           -0.0305430096           -0.0305430096


                         Molecular gradient (au)
                         -----------------------

   H       -0.0930440960           -0.0930440960           -0.0930440960
   H        0.0930440960            0.0930440960            0.0930440960


 RMS gradient norm (au):      0.0930440960181858



 Predicted energy change        0.000000000000000


                     Step in diagonal representation
                     -------------------------------



                       Optimization Control Center
                       ---------------------------



                            Next geometry (au)
                            ------------------

   Total number of coordinates:  6
   Written in atomic units    
 
   1   H        x      0.0000000000
   2            y      0.0000000000
   3            z      0.0000000000
 
   4   H        x      1.8897261339
   5            y      1.8897261339
   6            z      1.8897261339


                         Optimization information
                         ------------------------

 Iteration number               :       1
 End of optimization            :       F 
 Energy at this geometry is     :      -0.846993
 Energy change from last geom.  :       0.000000
 Norm of gradient               :       0.000000
 Norm of step                   :       0.000000
 Updated trust radius           :       0.500000

    First density: Atoms in molecule guess

    Iteration 0 energy:       -0.556688724854
 
    Diagonalize the Atoms in Molecule Fock Matrix to obtain Initial Density
    Preparing to do cholesky decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  9.99999975E-05
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift OAO gradient ###
    ******************************************************************************** ###
      1     -0.8446815242    0.00000000000    0.00      0.00000    0.00    3.899E-02 ###
      2     -0.8465960253   -0.00191450112    0.00      0.00000   -0.00    1.626E-02 ###
      3     -0.8469932020   -0.00039717667    0.00      0.00000   -0.00    1.687E-05 ###
    SCF converged in      3 iterations
    >>>  CPU Time used in SCF iterations is   0.01 seconds
    >>> wall Time used in SCF iterations is   0.01 seconds

    Total no. of matmuls in SCF optimization:        371

    Number of occupied orbitals:       1
    Number of virtual orbitals:        1

    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1


    Calculation of HOMO-LUMO gap
    ============================

    Calculation of occupied orbital energies converged in     1 iterations!

    Calculation of virtual orbital energies converged in     1 iterations!

     E(LUMO):                         0.259843 au
    -E(HOMO):                        -0.212790 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     0.472633 au


    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2   -0.00191450112    0.0000   -0.0000    0.0000000
      3   -0.00039717667    0.0000   -0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 OAO Gradient norm
    ======================================================================
        1            -0.84468152419109077123      0.389894120891196D-01
        2            -0.84659602531533351133      0.162628018398786D-01
        3            -0.84699320198941141502      0.168693529035645D-04

          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<


          Final HF energy:                        -0.846993201989
          Nuclear repulsion:                       0.305520603661
          Electronic energy:                      -1.152513805650



             StepInfo:Energy difference to previous geometry:
             ------------------------------------------------


      Energy       Old_Energy     Actual_diff  Predicted_diff  Ratio

       -0.8469932020       -0.8469932020   0.0000000000   0.0000000000            NaN

 Trust radius squarely increased due to very good ratio.
 Updated trust radius   0.50000


                       Optimization Control Center
                       ---------------------------



                              Final geometry
                              --------------

   Total number of coordinates:  6
   Written in atomic units    
 
   1   H        x      0.0000000000
   2            y      0.0000000000
   3            z      0.0000000000
 
   4   H        x      1.8897261339
   5            y      1.8897261339
   6            z      1.8897261339
 Iter      Energy        Change       GradNorm  Index   StepLen   TrustRad #Rej
 -------------------------------------------------------------------------------
   0       -0.846993     0.000000     0.000000    0    0.000000*   0.500000   0 
   1       -0.846993     0.000000     0.000000    0    0.000000*   0.500000   0 

 Geometry has NOT converged!
 Maximum number of iterations (           1 ) has been reached and
 optimization halted. Increase number or restart from last geometry.

    Total no. of matmuls used:                       387
    Total no. of Fock/KS matrix evaluations:          12
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):      3104 byte Should be zero, otherwise a leakage is present
      Allocated memory (type(matrix)):           3104 byte  - Should be zero - otherwise a leakage is present
 
      Max allocated memory, TOTAL                        381.788 kB
      Max allocated memory, type(matrix)                   4.128 kB
      Max allocated memory, real(realk)                  372.272 kB
      Max allocated memory, integer                        9.564 kB
      Max allocated memory, logical                        0.672 kB
      Max allocated memory, character                      1.664 kB
      Max allocated memory, AOBATCH                        7.800 kB
      Max allocated memory, ODBATCH                        0.528 kB
      Max allocated memory, LSAOTENSOR                     3.040 kB
      Max allocated memory, ATOMTYPEITEM                  38.264 kB
      Max allocated memory, ATOMITEM                       1.440 kB
      Max allocated memory, LSMATRIX                       0.672 kB
      Max allocated memory, OverlapT                       9.472 kB
      Max allocated memory, linkshell                      0.120 kB
      Max allocated memory, integrand                     64.512 kB
      Max allocated memory, integralitem                 184.320 kB
      Max allocated memory, IntWork                       13.984 kB
      Max allocated memory, Overlap                      102.316 kB
      Max allocated memory, ODitem                         0.384 kB
      Max allocated memory, LStensor                       0.759 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is   0.55 seconds
    >>> wall Time used in LSDALTON is   0.55 seconds

    End simulation
