#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > redint_bfgs.info <<'%EOF%'
   dft_lda_molhes 
   -------------
   Molecule:         Water
   Wave Function:    DFT / BLYP / 6-31G / Ahlrichs-Coulomb-Fit
   Test Purpose:     Test geometry optimizer in redundant internal
   coordinates with model Hessian updated with BFGS formula
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > redint_bfgs.mol <<'%EOF%'
ATOMBASIS
Aceton
First order minimization in redundant internals
Atomtypes=3 Generators=0
Charge=8.0 Atoms=1 Bas=6-31G Aux=Ahlrichs-Coulomb-Fit
O     0.0000000000        3.4669575743        0.0000000000
Charge=6.0 Atoms=3 Bas=6-31G Aux=Ahlrichs-Coulomb-Fit
C     0.0000000000        1.1403259418        0.0000000000
C     0.0000000000       -0.2954284067        2.5013817152
C     0.0000000000       -0.2954284067       -2.5013817152
Charge=1.0 Atoms=6 Bas=6-31G Aux=Ahlrichs-Coulomb-Fit
H     0.0000000000        1.0044065219        4.1375406882
H     0.0000000000        1.0044065219       -4.1375406882
H     1.6936030390       -1.5063099366        2.6698480410
H    -1.6936030390       -1.5063099366        2.6698480410
H     1.6936030390       -1.5063099366       -2.6698480410
H    -1.6936030390       -1.5063099366       -2.6698480410
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > redint_bfgs.dal <<'%EOF%'
**OPTIMI
.REDINT
.INIMOD
.BAKER
**WAVE FUNCTIONS
.DFT
 BLYP
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.RH
.DIIS
.RESTART
.START
H1DIAG
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > redint_bfgs.check
cat >> redint_bfgs.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * DFT energy\: * \-193\.0220297" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT2=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT2`
CTRL[2]=1
ERROR[2]="Memory leak -"

# Number of energies
CRIT3=`$GREP "Final * DFT energy" $log | wc -l` 
TEST[3]=`expr   $CRIT3`
CTRL[3]=5
ERROR[3]="Wrong number of geometry steps"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
