from pathlib import Path

import numpy as np
import pytest
from scipy.linalg import eigh

import pylsdalton as pylsd


def test_diagonalize(shared_datadir, pylsd_instance):
    hamiltonian = np.load(
        Path(shared_datadir / "hamiltonian.npy").resolve(),
    )
    metric = np.load(
        Path(shared_datadir / "metric.npy").resolve(),
    )
    ref_density = np.load(
        Path(shared_datadir / "density_CMO.npy").resolve(),
    )
    ref_E = np.load(
        Path(shared_datadir / "energies.npy").resolve(),
    )

    nocc = pylsd.number_electrons() // 2
    # Diagonalize using LSDALTON
    energies, CMO = pylsd.diagonalize(hamiltonian, metric)
    # Diagonalize using scipy.linalg.eigh
    energies_, CMO_ = eigh(a=hamiltonian, b=metric, check_finite=False)
    # Transpose eigenvector matrix obtained with SciPy
    CMO_ = np.transpose(CMO_)

    # Create density matrix
    density = np.einsum("ki,kj->ij", CMO[0:nocc, :], CMO[0:nocc, :])
    density_ = np.einsum("ki,kj->ij", CMO_[0:nocc, :], CMO_[0:nocc, :])
    density__ = pylsd.diagonal_density(hamiltonian, metric)

    energies = energies
    assert energies == pytest.approx(ref_E)
    # Cross-check with SciPy eigh
    assert energies_ == pytest.approx(ref_E)
    assert energies_ == pytest.approx(energies)

    assert density == pytest.approx(ref_density)
    assert density_ == pytest.approx(density)
    assert density__ == pytest.approx(density)


def test_read_write_matrix(shared_datadir, pylsd_instance):
    ref_density = np.load(
        Path(shared_datadir / "density_CMO.npy").resolve(),
    )
    pylsd.write_matrix(ref_density, "CMO.bin")
    density = pylsd.read_matrix("CMO.bin")
    assert density == pytest.approx(ref_density)


def test_read_matrix_exception(shared_datadir, pylsd_instance):
    with pytest.raises(
        FileNotFoundError,
        match="Requested binary LSDalton matrix file CMO.data was not found",
    ):
        density = pylsd.read_matrix("CMO.data")


def test_write_matrix_exception(shared_datadir, pylsd_instance):
    density = np.zeros((2, 3))
    with pytest.raises(
        RuntimeError, match="write_matrix error: Only quadratic matrices supported"
    ):
        pylsd.write_matrix(density, "CMO.data")
