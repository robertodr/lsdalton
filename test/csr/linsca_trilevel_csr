#!/bin/sh 
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > linsca_trilevel_csr.info <<'%EOF%'
   linsca_trilevel_csr
   -------------
   Molecule:         H2O
   Wave Function:    DFT GGAKey slater=1 becke=1 vwn5=1 p86c=1
   Test Purpose:     Check trilevel code and LSDALTON
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > linsca_trilevel_csr.mol <<'%EOF%'
BASIS
6-31G Aux=Ahlrichs-Coulomb-Fit
water R(OH) = 0.95Aa , <HOH = 109 deg.
Distance in Aangstroms
   2     0         A
        8.    1
O      0.00000   0.00000   0.00000
        1.    2
H      0.55168   0.77340   0.00000
H      0.55168  -0.77340   0.00000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > linsca_trilevel_csr.dal <<'%EOF%'
**GENERAL
.TESTMPICOPY
.CSR
**INTEGRAL
.DENSFIT
**WAVE FUNCTIONS
.DFT
GGAKey slater=1 becke=1 vwn5=1 p86c=1
*DFT INPUT
.GRID3
.GRID TYPE
BLOCK
.DFTTHR
1.d-6 0.d0 1.d-7 1.d-9
#.DFTELS
#1
*DENSOPT
.ARH
!.NOINCREM
.START
 TRILEVEL
.CONVTHR
 1.0d-4
**INFO
.INFO_LINEQ
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >linsca_trilevel_csr.check
cat >> linsca_trilevel_csr.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ENERGY test
CRIT1=`$GREP "Final DFT energy:                  -76.263773" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="LEVEL 2 ENERGY NOT CORRECT -"

CRIT1=`$GREP "Final DFT energy:                  -76.389237" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="LEVEL 3 ENERGY NOT CORRECT -"

CRIT1=`$GREP "HOMO-LUMO Gap \(iteratively\):     0.33302" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="HOMO-LUMO GAP NOT CORRECT -"

## Test individual iterations, level 2
#CRIT1=`$GREP "1    -76.2071385588" $log | wc -l`
#TEST[4]=`expr  $CRIT1`
#CTRL[4]=1
#ERROR[4]="1st L2 energy not correct -"
#
## Test individual iterations, level 2
#CRIT1=`$GREP "2    -76.2519527552" $log | wc -l`
#TEST[5]=`expr  $CRIT1`
#CTRL[5]=1
#ERROR[5]="2nd L2 energy not correct -"
#
## Test individual iterations, level 2
#CRIT1=`$GREP "4    -76.2634339121" $log | wc -l`
#TEST[6]=`expr  $CRIT1`
#CTRL[6]=1
#ERROR[6]="Last L2 energy not correct -"
#
## Test individual iterations, level 3
#CRIT1=`$GREP "1    -76.2634339125" $log | wc -l`
#TEST[7]=`expr  $CRIT1`
#CTRL[7]=1
#ERROR[7]="1st L3 energy not correct -"
#
## Test individual iterations, level 3
#CRIT1=`$GREP "2    -76.3860359822" $log | wc -l`
#TEST[8]=`expr  $CRIT1`
#CTRL[8]=1
#ERROR[8]="2nd L3 energy not correct -"
#
## Test individual iterations, level 3
#CRIT1=`$GREP "6    -76.3888866507" $log | wc -l`
#TEST[9]=`expr  $CRIT1`
#CTRL[9]=1
#ERROR[9]="Last L3 energy not correct -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=1
ERROR[4]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[5]=`expr  $CRIT1`
CTRL[5]=0
ERROR[5]="MPI Memory leak -"

PASSED=1
for i in 1 2 3 4 5
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
