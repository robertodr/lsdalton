#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > linsca_energy_csr.info <<'%EOF%'
   linsca_energy_csr
   -------------
   Molecule:         H2O
   Wave Function:    HF
   Test Purpose:     Check Linsca 
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > linsca_energy_csr.mol <<'%EOF%'
BASIS
3-21G
water R(OH) = 0.95Aa , <HOH = 109 deg.
Distance in Aangstroms
   2     0         *
        8.    1
O      0.00000   0.00000   0.00000
        1.    2
H      0.55168   0.77340   0.00000
H      0.55168  -0.77340   0.00000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > linsca_energy_csr.dal <<'%EOF%'
**GENERAL
.CSR
.ZERO
1.d-9
**WAVE FUNCTIONS
.HF
*DENSOPT
.RH
.DIIS
.NVEC
8
.CONVTHR
3.D-5
.START
H1DIAG
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >linsca_energy_csr.check
cat >> linsca_energy_csr.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Test combination of diagonalization and CSR
CRIT1=`$GREP "The combination of diagonalization and CSR is very inefficient!" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
