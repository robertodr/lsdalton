     THIS IS A DEBUG BUILD
  
     ******************************************************************    
     **********  LSDalton - An electronic structure program  **********    
     ******************************************************************    
  
  
    This is output from LSDalton v2018.0
  
  
     IF RESULTS OBTAINED WITH THIS CODE ARE PUBLISHED,
     THE FOLLOWING PAPER SHOULD BE CITED:
     
     K. Aidas, C. Angeli, K. L. Bak, V. Bakken, R. Bast,
     L. Boman, O. Christiansen, R. Cimiraglia, S. Coriani,
     P. Dahle, E. K. Dalskov, U. Ekstroem, T. Enevoldsen,
     J. J. Eriksen, P. Ettenhuber, B. Fernandez,
     L. Ferrighi, H. Fliegl, L. Frediani, K. Hald,
     A. Halkier, C. Haettig, H. Heiberg,
     T. Helgaker, A. C. Hennum, H. Hettema,
     E. Hjertenaes, S. Hoest, I.-M. Hoeyvik,
     M. F. Iozzi, B. Jansik, H. J. Aa. Jensen,
     D. Jonsson, P. Joergensen, J. Kauczor,
     S. Kirpekar, T. Kjaergaard, W. Klopper,
     S. Knecht, R. Kobayashi, H. Koch, J. Kongsted,
     A. Krapp, K. Kristensen, A. Ligabue,
     O. B. Lutnaes, J. I. Melo, K. V. Mikkelsen, R. H. Myhre,
     C. Neiss, C. B. Nielsen, P. Norman,
     J. Olsen, J. M. H. Olsen, A. Osted,
     M. J. Packer, F. Pawlowski, T. B. Pedersen,
     P. F. Provasi, S. Reine, Z. Rinkevicius,
     T. A. Ruden, K. Ruud, V. Rybkin,
     P. Salek, C. C. M. Samson, A. Sanchez de Meras,
     T. Saue, S. P. A. Sauer, B. Schimmelpfennig,
     K. Sneskov, A. H. Steindal, K. O. Sylvester-Hvid,
     P. R. Taylor, A. M. Teale, E. I. Tellgren,
     D. P. Tew, A. J. Thorvaldsen, L. Thoegersen,
     O. Vahtras, M. A. Watson, D. J. D. Wilson,
     M. Ziolkowski, and H. AAgren,
     "The Dalton quantum chemistry program system",
     WIREs Comput. Mol. Sci. (doi: 10.1002/wcms.1172)
  
  
                                               
    LSDalton authors in alphabetical order (main contribution(s) in parenthesis)
    ----------------------------------------------------------------------------
    Vebjoern Bakken,        University of Oslo,                    Norway   (Geometry optimizer)
    Ashleigh Barnes,        Oak Ridge National Laboratory,         USA      (ML-DEC)
    Radovan Bast,           UiT The Arctic University of Norway,   Norway   (CMake, Testing)
    Pablo Baudin,           Aarhus University,                     Denmark  (DEC,CCSD)
    Dmytro Bykov,           Oak Ridge National Laboratory,         USA      (ML-DEC)
    Sonia Coriani,          Technical University of Denmark,       Denmark  (Response)
    Roberto Di Remigio,     UiT The Arctic University of Norway,   Norway   (PCM)
    Karen Dundas,           UiT The Arctic University of Norway,   Norway   (OpenRSP)
    Patrick Ettenhuber,     Aarhus University,                     Denmark  (CCSD)
    Janus Juul Eriksen,     Aarhus University,                     Denmark  (CCSD(T), DEC)
    Luca Frediani,          UiT The Arctic University of Norway,   Norway   (PCM, Supervision)
    Daniel Henrik Friese,   Heinrich-Heine-Universitat Dusseldorf, Germany  (OpenRSP)
    Bin Gao,                UiT The Arctic University of Norway,   Norway   (OpenRSP, QcMatrix)
    Trygve Helgaker,        University of Oslo,                    Norway   (Supervision)
    Stinne Hoest,           Aarhus University,                     Denmark  (SCF optimization)
    Ida-Marie Hoeyvik,      Aarhus University,                     Denmark  (Orbital localization, SCF opt)
    Robert Izsak,           University of Oslo,                    Norway   (ADMM)
    Branislav Jansik,       Aarhus University,                     Denmark  (Trilevel, orbital localization)
    Frank Jensen,           Aarhus University,                     Denmark  (ADMM basis sets)
    Poul Joergensen,        Aarhus University,                     Denmark  (Supervision)
    Joanna Kauczor,         Aarhus University,                     Denmark  (Response solver)
    Thomas Kjaergaard,      Aarhus University,                     Denmark  (RSP, INT, DEC, SCF, Input, MPI, 
                                                                             MAT)
    Andreas Krapp,          University of Oslo,                    Norway   (FMM, dispersion-corrected DFT)
    Kasper Kristensen,      Aarhus University,                     Denmark  (Response, DEC)
    Chandan Kumar,          University of Oslo,                    Norway   (Nuclei-selected NMR shielding)
    Patrick Merlot,         University of Oslo,                    Norway   (ADMM)
    Cecilie Nygaard,        Aarhus University,                     Denmark  (SOEO)
    Jeppe Olsen,            Aarhus University,                     Denmark  (Supervision)
    Elisa Rebolini,         University of Oslo,                    Norway   (MO-PARI-K)
    Simen Reine,            University of Oslo,                    Norway   (Integrals and approximations, 
                                                                             XC, MPI, OpenMP, geo.opt.)
    Magnus Ringholm,        UiT The Arctic University of Norway,   Norway   (OpenRSP)
    Kenneth Ruud,           UiT The Arctic University of Norway,   Norway   (OpenRSP, Supervision)
    Vladimir Rybkin,        University of Oslo,                    Norway   (Geometry optimizer, dynamics)
    Pawel Salek,            KTH Stockholm,                         Sweden   (FMM, DFT functionals)
    Andrew M. Teale,        University of Nottingham               England  (E-coefficients)
    Erik Tellgren,          University of Oslo,                    Norway   (Density fitting, E-coefficients)
    Andreas J. Thorvaldsen, University of Tromsoe,                 Norway   (Response)
    Lea Thoegersen,         Aarhus University,                     Denmark  (SCF optimization)
    Mark Watson,            University of Oslo,                    Norway   (FMM)
    Lukas Wirz,             University of Oslo,                    Norway   (NR-PARI)
    Marcin Ziolkowski,      Aarhus University,                     Denmark  (DEC)
  
  
     NOTE:
      
     This is an experimental code for the evaluation of molecular
     energies and properties using various electronic structure models.
     The authors accept no responsibility for the performance of the code or
     for the correctness of the results.
      
     The code (in whole or part) is provided under a licence and
     is not to be reproduced for further distribution without
     the written permission of the authors or their representatives.
      
     See the home page "http://daltonprogram.org"
     for further information.
  
  
 ================================================================
Version information
-------------------
Version       | v2018.0-85-gac2a206-dirty
Commit hash   | ac2a2060e79e3775b140fc9c5ef86c975c4347a7
Commit author | Roberto Di Remigio
Commit date   | Fri Jan 17 14:54:43 2020 +0100
Branch        | update_xcfun

Configuration and build information
-----------------------------------
Who compiled             | roberto
Compilation hostname     | minazo
Operating system         | Linux-4.19.95
Processor                | x86_64
CMake version            | 3.15.1
CMake generator          | Unix Makefiles
Build type               | DEBUG
Configuration time       | 2020-01-20 08:05:12
Fortran compiler         | /nix/store/3fnsihsjlxsg7jrc84rgsdj1q0flvfg9-gfortran-wrapper-8.3.0/bin/gfortran
Fortran compiler version | 8.3.0
C compiler               | /nix/store/5yyx688q9qxhb4ypawq7v80fm3ix27dm-gcc-wrapper-8.3.0/bin/gcc
C compiler version       | 8.3.0
C++ compiler             | /nix/store/5yyx688q9qxhb4ypawq7v80fm3ix27dm-gcc-wrapper-8.3.0/bin/g++
C++ compiler version     | 8.3.0
64-bit integers          | OFF
MPI parallelization      | OFF
OpenMP parallelization   | OFF

Further components
------------------

Runtime information
-------------------
Hostname:
    minazo
Current working dir:
    /tmp/DALTON_scratch_roberto/arh_pbe_h2o_6397
  
 The XCfun module is activated
    The Functional chosen is a GGA type functional
    The Functional chosen contains no exact exchange contribution

         Start simulation
     Date and time (Linux)  : Mon Jan 20 08:07:13 2020
     Host name              : minazo                                  
                      
    ---------------------------------------------------
             PRINTING THE MOLECULE.INP FILE 
    ---------------------------------------------------
                      
    BASIS                                   
    STO-3G                                  
    LSint test, H2O                         
    STO-3G                                  
    Atomtypes=2 Nosymmetry                                                                                                  
    Charge=8.0 Atoms=1                                                                                                      
    O     0.000000000  -0.224905893   0.00000000                                                                            
    Charge=1.0 Atoms=2                                                                                                      
    H     1.452350000   0.899623000   0.00000000                                                                            
    H    -1.452350000   0.899623000   0.00000000                                                                            
                      
    ---------------------------------------------------
             PRINTING THE LSDALTON.INP FILE 
    ---------------------------------------------------
                      
    **GENERAL
    .XCFUN
    **WAVE FUNCTIONS
    .DFT
     PBE
    *END OF INPUT
 
 
                      
    Atoms and basis sets
      Total number of atoms        :      3
      THE  REGULAR   is on R =   1
    ---------------------------------------------------------------------
      atom label  charge basisset                prim     cont   basis
    ---------------------------------------------------------------------
          1 O      8.000 STO-3G                    15        5 [6s3p|2s1p]                                  
          2 H      1.000 STO-3G                     3        1 [3s|1s]                                      
          3 H      1.000 STO-3G                     3        1 [3s|1s]                                      
    ---------------------------------------------------------------------
    total         10                               21        7
    ---------------------------------------------------------------------
                      
                      
    Basic Molecule/Basis information
    --------------------------------------------------------------------
      Molecular Charge                   :    0.0000
      Regular basisfunctions             :        7
      Primitive Regular basisfunctions   :       21
    --------------------------------------------------------------------
                      
    Configuration:
    ==============
    This is a Single core calculation. (no OpenMP)
    This is a serial calculation (no MPI)
    The Functional chosen is a GGA type functional
    The Functional chosen contains no exact exchange contribution
     
    The Exchange-Correlation Grid specifications:
    Radial Quadrature : Treutler-Ahlrichs M4-T2 scheme
                        (J. Chem. Phys. (1995) 102, 346).
                        Implies also that the angular integration quality becomes Z-dependant
    Space partitioning: Stratmann-Scuseria-Frisch partitioning scheme
                        Chem. Phys. Lett. (1996) vol. 213 page 257,
                        Combined with a blockwise handling of grid points
                        J. Chem. Phys. (2004) vol 121, page 2915.
                        Useful for large molecules.

    We use grid pruning according to Mol. Phys. (1993) vol 78 page 997

     DFT LSint Radial integration threshold:   0.5012D-13
     DFT LSint integration order range     :   [  5:  35]
     Hardness of the partioning function   :   3
     DFT LSint screening thresholds        :   0.10D-08   0.20D-09   0.20D-11
     Threshold for number of electrons     :   0.10D-02
     The Exact Exchange Factor             : 0.0000D+00

    You have requested Augmented Roothaan-Hall optimization
    => explicit averaging is turned off!

    Expand trust radius if ratio is larger than:            0.75
    Contract trust radius if ratio is smaller than:         0.25
    On expansion, trust radius is expanded by a factor      1.20
    On contraction, trust radius is contracted by a factor  0.70

    Maximum size of subspace in ARH linear equations:       2

    Density subspace min. method    : None                    
    Density optimization            : Augmented RH optimization          

    Maximum size of Fock/density queue in averaging:    7

    Convergence threshold for gradient        :   0.10E-03
     
    We perform the calculation in the Grand Canonical basis
    (see PCCP 2009, 11, 5805-5813)
    To use the standard input basis use .NOGCBASIS
 
    Since the input basis set is a segmented contracted basis we
    perform the integral evaluation in the more efficient
    standard input basis and then transform to the Grand 
    Canonical basis, which is general contracted.
    You can force the integral evaluation in Grand 
    Canonical basis by using the keyword
    .NOGCINTEGRALTRANSFORM
     
    The Overall Screening threshold is set to              :  1.0000E-08
    The Screening threshold used for Coulomb               :  1.0000E-10
    The Screening threshold used for Exchange              :  1.0000E-08
    The Screening threshold used for One-electron operators:  1.0000E-15
    The SCF Convergence Criteria is applied to the gradnorm in OAO basis

    End of configuration!

 
    Matrix type: mtype_dense
 
    A set of atomic calculations are performed in order to construct
    the Grand Canonical Basis set (see PCCP 11, 5805-5813 (2009))
    as well as JCTC 5, 1027 (2009)
    This is done in order to use the TRILEVEL starting guess and 
    perform orbital localization
    This is Level 1 of the TRILEVEL starting guess and is performed per default.
    The use of the Grand Canonical Basis can be deactivated using .NOGCBASIS
    under the **GENERAL section. This is NOT recommended if you do TRILEVEL 
    or orbital localization.
 

    Level 1 atomic calculation on STO-3G Charge   8
    ================================================
  
    Total Number of grid points:       14368

    Max allocated memory, Grid                    1.038 MB

    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift  AO gradient ###
    ******************************************************************************** ###
      1    -73.8760472658    0.00000000000    0.00      0.00000    0.00    2.508E-14 ###

    Level 1 atomic calculation on STO-3G Charge   1
    ================================================
  
    Total Number of grid points:       10026

    Max allocated memory, Grid                  753.473 kB

    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift  AO gradient ###
    ******************************************************************************** ###
      1     -0.4093253549    0.00000000000    0.00      0.00000    0.00    0.000E+00 ###
 
    Matrix type: mtype_dense
 ndim =           7

    First density: Atoms in molecule guess

    Total Number of grid points:       34420

    Max allocated memory, Grid                    1.934 MB

    Iteration 0 energy:      -75.167334809369
 
    Diagonalize the Atoms in Molecule Fock Matrix to obtain Initial Density
    Preparing to do S^1/2 decomposition...
  
    Relative convergence threshold for solver:  1.00000000E-02
    SCF Convergence criteria for gradient norm:  9.99999975E-05
    ******************************************************************************** ###
     it            E(SCF)          dE(SCF)    exit        alpha RHshift OAO gradient ###
    ******************************************************************************** ###
      1    -75.2086741220    0.00000000000    0.00      0.00000    0.00    1.681E-01 ###
      2    -75.2086741220    0.00000000000    0.00      0.00000   -0.00    1.876E-01 ###
      3    -75.2134584546   -0.00478433260    0.00      0.00000    0.10    1.564E-01 ###
      4    -75.2157920374   -0.00233358285    0.00      0.00000   -0.00    1.447E-01 ###
      5    -75.2292797236   -0.01348768615    0.00      0.00000   -0.00    1.800E-03 ###
      6    -75.2292838586   -0.00000413501    0.00      0.00000   -0.00    3.826E-04 ###
      7    -75.2292839589   -0.00000010035    0.00      0.00000   -0.00    9.191E-07 ###
    SCF converged in      7 iterations
    >>>  CPU Time used in SCF iterations is   2.72 seconds
    >>> wall Time used in SCF iterations is   2.72 seconds

    Total no. of matmuls in SCF optimization:        529

    Number of occupied orbitals:       5
    Number of virtual orbitals:        2

    Number of occupied orbital energies to be found:       1
    Number of virtual orbital energies to be found:        1


    Calculation of HOMO-LUMO gap
    ============================

    Calculation of occupied orbital energies converged in     3 iterations!

    Calculation of virtual orbital energies converged in     2 iterations!

     E(LUMO):                         0.297373 au
    -E(HOMO):                        -0.162958 au
    -------------------------------------------------
     HOMO-LUMO Gap (iteratively):     0.460332 au


    ********************************************************
     it       dE(HF)          exit   RHshift    RHinfo 
    ********************************************************
      1    0.00000000000    0.0000    0.0000    0.0000000
      2    0.00000000000    0.0000   -0.0000    0.0000000
      3   -0.00478433260    0.0000    0.1000    0.0000000
      4   -0.00233358285    0.0000   -0.0000    0.0000000
      5   -0.01348768615    0.0000   -0.0000    0.0000000
      6   -0.00000413501    0.0000   -0.0000    0.0000000
      7   -0.00000010035    0.0000   -0.0000    0.0000000

    ======================================================================
                       LINSCF ITERATIONS:
      It.nr.               Energy                 OAO Gradient norm
    ======================================================================
        1           -75.20867412197289070264      0.168140839336454D+00
        2           -75.20867412197289070264      0.187607928297869D+00
        3           -75.21345845457402390366      0.156430016620056D+00
        4           -75.21579203742527397480      0.144662077089932D+00
        5           -75.22927972357912551615      0.179959250482093D-02
        6           -75.22928385859083277865      0.382617502316072D-03
        7           -75.22928395893839592645      0.919144075848132D-06

          SCF converged !!!! 
             >>> Final SCF results from LSDALTON <<<


          Final DFT energy:                      -75.229283958938
          Nuclear repulsion:                       9.055004525638
          Electronic energy:                     -84.284288484576

    Total no. of matmuls used:                       545
    Total no. of Fock/KS matrix evaluations:           8
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
                      Memory statistics          
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
      Allocated memory (TOTAL):         0 byte Should be zero, otherwise a leakage is present
 
      Max allocated memory, TOTAL                          1.711 MB
      Max allocated memory, type(matrix)                  14.504 kB
      Max allocated memory, real(realk)                    1.199 MB
      Max allocated memory, integer                      233.504 kB
      Max allocated memory, logical                      138.320 kB
      Max allocated memory, character                      2.448 kB
      Max allocated memory, AOBATCH                       18.200 kB
      Max allocated memory, ODBATCH                        1.760 kB
      Max allocated memory, LSAOTENSOR                     6.912 kB
      Max allocated memory, SLSAOTENSOR                    6.960 kB
      Max allocated memory, ATOMTYPEITEM                 201.056 kB
      Max allocated memory, ATOMITEM                       2.368 kB
      Max allocated memory, LSMATRIX                       3.456 kB
      Max allocated memory, OverlapT                      47.616 kB
      Max allocated memory, integrand                     64.512 kB
      Max allocated memory, integralitem                 829.440 kB
      Max allocated memory, IntWork                       25.720 kB
      Max allocated memory, Overlap                      388.304 kB
      Max allocated memory, ODitem                         1.280 kB
      Max allocated memory, LStensor                       5.187 kB
    *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*

    Allocated MPI memory a cross all slaves:          0 byte  - Should be zero - otherwise a leakage is present
    This is a non MPI calculation so naturally no memory is allocated on slaves!
    >>>  CPU Time used in LSDALTON is   3.54 seconds
    >>> wall Time used in LSDALTON is   3.54 seconds

    End simulation
     Date and time (Linux)  : Mon Jan 20 08:07:17 2020
     Host name              : minazo                                  
