foreach(_dir
  LSint
  LSlib
  LSresponse
  ddynam
  dft
  geomopt
  linsca
  pbc2
  plt
  wannier
  )
  add_subdirectory(${_dir})
endforeach()

if(ENABLE_OPENRSP)
  add_subdirectory(openrsp)
endif()

if(ENABLE_MPI AND ENABLE_SCALAPACK AND NOT USE_32BIT_MPI_INTERFACE)
  add_subdirectory(SCALAPACK)
endif()

if(ENABLE_CSR)
  add_subdirectory(csr)
endif()

if(ENABLE_DEC)
  add_subdirectory(dectests)
endif()

if(ENABLE_PCMSOLVER)
  add_subdirectory(pcm)
endif()

if(ENABLE_FRAME)
  add_subdirectory(FraME)
endif()

if(ENABLE_CHEMSHELL)
  add_subdirectory(interface)
endif()

if(ENABLE_PYTHON_INTERFACE)
  add_subdirectory(pylsdalton)
endif()
