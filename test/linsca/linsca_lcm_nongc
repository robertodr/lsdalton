#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > linsca_lcm_nongc.info <<'%EOF%'
   linsca_lcm_nongc
   -------------------
   Molecule:         Formaldehyde
   Wave Function:    HF / 6-31G
   Test Purpose:     Test combination of trilevel without gcbasis
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > linsca_lcm_nongc.mol <<'%EOF%'
BASIS
6-31G


Atomtypes=3 Nosymmetry Angstrom
Charge=1.0 Atoms=2
H 0.0000   0.9429       -0.5876
H 0.0000   -0.9429      -0.5876
Charge=6.0 Atoms=1
C  0.00000000   0.00000000   0.00000000
Charge=8.0 Atoms=1
O  0.00000000   0.00000000   1.20500000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > linsca_lcm_nongc.dal <<'%EOF%'
**GENERAL
.NOGCBASIS
**WAVE FUNCTIONS
.HF
*DENSOPT
.ARH
.START
TRILEVEL
.CONVDYN
STANDARD
.LCM
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > linsca_lcm_nongc.check
cat >> linsca_lcm_nongc.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * HF energy\: * \-113\.807256" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"

# Memory test
CRIT2=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT2`
CTRL[2]=1
ERROR[2]="Memory leak -"

PASSED=1
for i in 1 2
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
