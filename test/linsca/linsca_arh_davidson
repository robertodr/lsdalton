#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > linsca_arh_davidson.info <<'%EOF%'
   linsca_arh_davidson
   ---------------
   Molecule:         C4H2 
   Wave Function:    BLYP/6-31G
   Test Purpose:     Check scf opt using davidson solver
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > linsca_arh_davidson.mol <<'%EOF%'
BASIS
6-31G


AtomTypes=2 Angstrom Generators=0
Charge=6.0 Atoms=4
C                       -14.35700     0.00000     0.00000
C                       -13.15400     0.00000     0.00000
C                       -11.61900     0.00000     0.00000
C                       -10.41600     0.00000     0.00000
Charge=1.0 Atoms=2
H                       -15.000     0.00000     0.00000
H                        -9.61800     0.00000     0.00000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > linsca_arh_davidson.dal <<'%EOF%'
**WAVE FUNCTIONS
.DFT
BLYP
*DENSOPT
.ARH DAVID
.START
TRILEVEL
.CONVDYN
STANDARD
.LCM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >linsca_arh_davidson.check
cat >> linsca_arh_davidson.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# test energy
CRIT1=`$GREP "Final DFT energy: * -152.9418" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"


# Test individual iterations
CRIT1=`$GREP " 2   -0\.568249" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="2nd energy shift not correct -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
