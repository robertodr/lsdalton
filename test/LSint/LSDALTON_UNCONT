#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_UNCONT.info <<'%EOF%'
   LSDALTON_UNCONT
   -------------
   Molecule:         2 HCN molecules placed 20 atomic units apart
   Wave Function:    B3LYP/STO-2G(H)+6-31G(N)+pc-0(C)
   Test Purpose:     Check .UNCONT  Keyword
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_UNCONT.mol <<'%EOF%'
ATOMBASIS
2 HCN molecules placed 20 atomic units apart
STRUCTURE IS NOT OPTIMIZED. STO-2G(H)+6-31G(N)+pc-0(C) basis
Atomtypes=3 Nosymmetry
Charge=1. Atoms=2 Basis=STO-2G
H   0.000   0.000  -1.000 
H   0.000  20.000  -1.000 
Charge=7. Atoms=2 Basis=6-31G
N   0.000   0.000   1.500
N   0.000  20.000   1.500
Charge=6. Atoms=2 Basis=pc-0
C   0.000   0.000   0.000
C   0.000  20.000   0.000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_UNCONT.dal <<'%EOF%'
**INTEGRALS
.UNCONT
.NOLINK
.NOJENGINE
**WAVE FUNCTIONS
.DFT
LDA
*DFT INPUT
.GRID TYPE
 BECKEORIG LMG
.RADINT
1.0D-11
.ANGINT
31
*DENSOPT
.RH
.DIIS
.START
H1DIAG
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_UNCONT.check
cat >> LSDALTON_UNCONT.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final DFT energy\: * \-182\.084318799" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=0
ERROR[3]="MPI Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
