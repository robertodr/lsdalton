#!/usr/bin/env python

import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))  # isort: skip

from runtest import version_info, get_filter, cli, run
from runtest_config import configure


assert version_info.major == 2

options = cli()

f_hf = [
    get_filter(string="Nuclear repulsion:", rel_tolerance=1.0e-9),
    get_filter(string="Electronic energy:", rel_tolerance=1.0e-9),
    get_filter(string="PCM polarization energy:", abs_tolerance=1.0e-7),
    get_filter(string="Final HF energy:", rel_tolerance=1.0e-9),
    get_filter(
        from_string="POLARIZABILITY",
        to_string="End of polarizability calculation",
        skip_below=1.0e-7,
        rel_tolerance=1.0e-7,
    ),
]

ierr = 0
# Equilibrium PCM formulation
for inp in ["static_alpha_hf.dal", "dynamic_alpha_hf.dal"]:
    ierr += run(
        options,
        configure,
        input_files=[inp, "H2O.mol", "eq_pcmsolver.pcm"],
        filters={"out": f_hf},
    )
# Nonequilibrium PCM formulation
ierr += run(
    options,
    configure,
    input_files=["dynamic_alpha_hf.dal", "H2O.mol", "noneq_pcmsolver.pcm"],
    filters={"out": f_hf},
)

f_hfOPA = [
    get_filter(string="Nuclear repulsion:", rel_tolerance=1.0e-9),
    get_filter(string="Electronic energy:", rel_tolerance=1.0e-9),
    get_filter(string="PCM polarization energy:", abs_tolerance=1.0e-7),
    get_filter(string="Final HF energy:", rel_tolerance=1.0e-9),
    get_filter(
        from_string="ONE-PHOTON ABSORPTION",
        to_string="End of excitation energy calculation",
        ignore_sign=True,
        abs_tolerance=1.0e-4,
    ),
]
# Nonequilibrium PCM formulation
ierr += run(
    options,
    configure,
    input_files=["opa_hf.dal", "H2O.mol", "noneq_pcmsolver.pcm"],
    filters={"out": f_hfOPA},
)

f_lda = [
    get_filter(string="Nuclear repulsion:", rel_tolerance=1.0e-9),
    get_filter(string="Electronic energy:", rel_tolerance=1.0e-9),
    get_filter(string="PCM polarization energy:", abs_tolerance=1.0e-7),
    get_filter(string="Final DFT energy:", rel_tolerance=1.0e-9),
    get_filter(
        from_string="POLARIZABILITY",
        to_string="End of polarizability calculation",
        skip_below=1.0e-7,
        rel_tolerance=1.0e-7,
    ),
]
# Equilibrium PCM formulation
for inp in ["static_alpha_lda.dal", "dynamic_alpha_lda.dal"]:
    ierr += run(
        options,
        configure,
        input_files=[inp, "H2O.mol", "eq_pcmsolver.pcm"],
        filters={"out": f_lda},
    )
# Nonequilibrium PCM formulation
ierr += run(
    options,
    configure,
    input_files=["dynamic_alpha_lda.dal", "H2O.mol", "noneq_pcmsolver.pcm"],
    filters={"out": f_lda},
)

f_ldaOPA = [
    get_filter(string="Nuclear repulsion:", rel_tolerance=1.0e-9),
    get_filter(string="Electronic energy:", rel_tolerance=1.0e-9),
    get_filter(string="PCM polarization energy:", abs_tolerance=1.0e-7),
    get_filter(string="Final DFT energy:", rel_tolerance=1.0e-9),
    get_filter(
        from_string="ONE-PHOTON ABSORPTION",
        to_string="End of excitation energy calculation",
        ignore_sign=True,
        abs_tolerance=1.0e-4,
    ),
]
# Nonequilibrium PCM formulation
ierr += run(
    options,
    configure,
    input_files=["opa_lda.dal", "H2O.mol", "noneq_pcmsolver.pcm"],
    filters={"out": f_ldaOPA},
)

sys.exit(ierr)
