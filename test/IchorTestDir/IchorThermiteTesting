#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > IchorThermiteTesting.info <<'%EOF%'
   IchorThermiteTesting
   -------------
   Test Purpose:     Test Ichor Integrals agains Thermite Integrals
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > IchorThermiteTesting.mol <<'%EOF%'
BASIS
3-21G
water R(OH) = 0.95Aa , <HOH = 109 deg.
Distance in Aangstroms
   2     0         *
        8.    1
O      0.00000   0.00000   0.00000
        1.    2
H      0.55168   0.77340   0.00000
H      0.55168  -0.77340   0.00000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > IchorThermiteTesting.dal <<'%EOF%'
**GENERAL
.TIME
.NOGCBASIS
**INTEGRALS
.DEBUGICHOR
0
**WAVE FUNCTIONS
.HF
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >IchorThermiteTesting.check
cat >> IchorThermiteTesting.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Integral test
CRIT1=`$GREP "BASIS\(segS1p,segS1p,segS1p,segS1p,1\) SUCCESSFUL" $log | wc -l`
TEST[  1]=`expr  $CRIT1`
CTRL[  1]=1
ERROR[  1]="BASIS(segS1p,segS1p,segS1p,segS1p,1) NOT CORRECT -"

CRIT1=`$GREP "BASIS\(segP1p,segP1p,segP1p,segP1p,1\) SUCCESSFUL" $log | wc -l`
TEST[  2]=`expr  $CRIT1`
CTRL[  2]=1
ERROR[  2]="BASIS(segP1p,segP1p,segP1p,segP1p,1) NOT CORRECT -"

CRIT1=`$GREP "BASIS\(segD1p,segD1p,segD1p,segD1p,1\) SUCCESSFUL" $log | wc -l`
TEST[  3]=`expr  $CRIT1`
CTRL[  3]=1
ERROR[  3]="BASIS(segD1p,segD1p,segD1p,segD1p,1) NOT CORRECT -"

CRIT1=`$GREP "BASIS\(segD1p,segD1p,segD1p,segD1p,1\) SUCCESSFUL" $log | wc -l`
TEST[ 4]=`expr  $CRIT1`
CTRL[ 4]=1
ERROR[ 4]="BASIS(segD1p,segD1p,segD1p,segD1p,1) NOT CORRECT -"

CRIT1=`$GREP "BASIS\(segS  ,segS  ,segS  ,segS  ,1\) SUCCESSFUL" $log | wc -l`
TEST[ 5]=`expr  $CRIT1`
CTRL[ 5]=1
ERROR[ 5]="BASIS(segS  ,segS  ,segS  ,segS  ,1) NOT CORRECT -"

CRIT1=`$GREP "BASIS\(segP  ,segP  ,segP  ,segP  ,1\) SUCCESSFUL" $log | wc -l`
TEST[ 6]=`expr  $CRIT1`
CTRL[ 6]=1
ERROR[ 6]="BASIS(segP  ,segP  ,segP  ,segP  ,1) NOT CORRECT -"

CRIT1=`$GREP "BASIS\(segD  ,segD  ,segD  ,segD  ,1\) SUCCESSFUL" $log | wc -l`
TEST[ 7]=`expr  $CRIT1`
CTRL[ 7]=1
ERROR[ 7]="BASIS(segD  ,segD  ,segD  ,segD  ,1) NOT CORRECT -"

#nPasses = 2
CRIT1=`$GREP "BASIS\(segS1p,segS1p,segS1p,segS1p,1\) SUCCESSFUL" $log | wc -l`
TEST[  8]=`expr  $CRIT1`
CTRL[  8]=1
ERROR[  8]="BASIS(segS1p,segS1p,segS1p,segS1p,1) NOT CORRECT -"

CRIT1=`$GREP "BASIS\(segP1p,segP1p,segP1p,segP1p,1\) SUCCESSFUL" $log | wc -l`
TEST[  9]=`expr  $CRIT1`
CTRL[  9]=1
ERROR[  9]="BASIS(segP1p,segP1p,segP1p,segP1p,1) NOT CORRECT -"

CRIT1=`$GREP "BASIS\(segD1p,segD1p,segD1p,segD1p,1\) SUCCESSFUL" $log | wc -l`
TEST[ 10]=`expr  $CRIT1`
CTRL[ 10]=1
ERROR[ 10]="BASIS(segD1p,segD1p,segD1p,segD1p,1) NOT CORRECT -"

CRIT1=`$GREP "BASIS\(segD1p,segD1p,segD1p,segD1p,1\) SUCCESSFUL" $log | wc -l`
TEST[ 11]=`expr  $CRIT1`
CTRL[ 11]=1
ERROR[ 11]="BASIS(segD1p,segD1p,segD1p,segD1p,1) NOT CORRECT -"

CRIT1=`$GREP "BASIS\(segS  ,segS  ,segS  ,segS  ,1\) SUCCESSFUL" $log | wc -l`
TEST[ 12]=`expr  $CRIT1`
CTRL[ 12]=1
ERROR[ 12]="BASIS(segS  ,segS  ,segS  ,segS  ,1) NOT CORRECT -"

CRIT1=`$GREP "BASIS\(segP  ,segP  ,segP  ,segP  ,1\) SUCCESSFUL" $log | wc -l`
TEST[ 13]=`expr  $CRIT1`
CTRL[ 13]=1
ERROR[ 13]="BASIS(segP  ,segP  ,segP  ,segP  ,1) NOT CORRECT -"

CRIT1=`$GREP "BASIS\(segD  ,segD  ,segD  ,segD  ,1\) SUCCESSFUL" $log | wc -l`
TEST[ 14]=`expr  $CRIT1`
CTRL[ 14]=1
ERROR[ 14]="BASIS(segD  ,segD  ,segD  ,segD  ,1) NOT CORRECT -"

CRIT1=`$GREP "Ichor Integrals tested against Thermite\: SUCCESSFUL" $log | wc -l`
TEST[ 15]=`expr  $CRIT1`
CTRL[ 15]=1
ERROR[ 15]="NOT ALL ICHOR INTEGRALS CORRECT -"

PASSED=1
for((i=1;i<=15;i++))
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then

      if [ $PASSED -eq 1 ]
      then
         echo -e ${ERROR[i]} > newfile.txt
      else
         echo -e ${ERROR[i]} >> newfile.txt
      fi
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   cat newfile.txt -n
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
